package io.jmobile.video.browser.ui;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.graphics.drawable.Animatable2Compat;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseActivity;

public class IntroActivity extends BaseActivity {

    ImageView splashImage;
    Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, 500);
//        splashImage = fv(R.id.image_intro);
//
//        startButton = fv(io.jmobile.video.browser.R.id.button_start);
//        startButton.setVisibility(View.GONE);
//        startButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(IntroActivity.this, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                finish();
//            }
//        });

//        if (Build.VERSION.SDK_INT < 21) {
//            startBounceAnimDrawable();
//        } else {
////            startAnimDrawable();
//            startBounceAnimDrawable();
//        }

    }

    private void startAnimDrawable() {
        if (Build.VERSION.SDK_INT >= 21) {
            AnimatedVectorDrawableCompat animatedVector = AnimatedVectorDrawableCompat.create(this, R.drawable.hdvb_intro);
            animatedVector.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
                @Override
                public void onAnimationEnd(Drawable drawable) {
                    super.onAnimationEnd(drawable);
//                    layoutStart.setVisibility(View.VISIBLE);
                    splashImage.setVisibility(View.GONE);
                    startButton.setVisibility(View.VISIBLE);
                }
            });

            splashImage.setImageDrawable(animatedVector);
            animatedVector.start();
        }
    }

    private void startBounceAnimDrawable() {
        splashImage.clearAnimation();
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.bounce_intro);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashImage.setVisibility(View.GONE);
                startButton.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        splashImage.startAnimation(anim);
    }
}
