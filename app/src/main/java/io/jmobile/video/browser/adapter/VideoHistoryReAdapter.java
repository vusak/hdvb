package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.data.SearchResultItem;

public class VideoHistoryReAdapter extends ReSelectableAdapter<SearchResultItem, VideoHistoryReAdapter.VideoHistoryReHolder> {
    public static final int HEADER = 0;
    public static final int CHILD = 1;

    private Context context;
    private ArrayList<SearchResultItem> items = new ArrayList<>();

    public VideoHistoryReAdapter(Context context, int layoutId, ArrayList<SearchResultItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(VideoHistoryReHolder holder, int position) {
//
//        SearchResultItem item = items.get(position);
//
//        holder.title.setText(item.getTitle());
//        holder.description.setText(item.getDescription());
//        holder.channelTitle.setText(item.getChannelTitle());
//
//        holder.thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getThumbnailDefaultUrl()));
    }

    @Override
    public VideoHistoryReHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoHistoryReHolder holder = new VideoHistoryReHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

//        holder.thumbnail = holder.fv(R.id.image_thumbnail);
//        holder.title = holder.fv(R.id.text_title);
//        holder.description = holder.fv(R.id.text_description);
//        holder.channelTitle = holder.fv(R.id.text_channel_title);


        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    //    public static interface RecommendAdapterListener extends ReOnItemClickListener<RecommendItem> {
////        public void onDeleteButtonClick(int position, RecommendItem item);
//    }

    public static class HistoryItem {
        public int type;
        public String text;
        public List<HistoryItem> invisibleChildren;

        public HistoryItem() {

        }

        public HistoryItem(int type, String text) {
            this.type = type;
            this.text = text;
        }
    }

    public class VideoHistoryReHolder extends ReAbstractViewHolder {
        public TextView headerTitle;
        public HistoryItem refferalItem;

        public VideoHistoryReHolder(View itemView) {
            super(itemView);
//            headerTitle = (TextView) itemView.findViewById(R.id)
        }

//        public void setOnBrowserHolderClickListener(OnItemViewClickListener listener) {
//            browserClickListenr = listener;
//            if (listener != null) {
//                deleteButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        browserClickListenr.onItemViewClick(getPosition(), BrowserHolder.this);
//                    }
//                });
//            }
//        }
    }
}
