package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.common.LogUtil;

public class KeywordItem extends TableObject {
    public static final String TABLE_NAME = "keyword";

    public static final String ROW_ID = "_id";
    public static final String KEYWORD_COUNT = "keyword_count";
    public static final String KEYWORD_AT = "keyword_at";
    public static final String KEYWORD_KEY = "keyword_key";

    public static final Creator<KeywordItem> CREATOR = new Creator<KeywordItem>() {
        @Override
        public KeywordItem createFromParcel(Parcel source) {
            return new KeywordItem(source);
        }

        @Override
        public KeywordItem[] newArray(int size) {
            return new KeywordItem[size];
        }
    };
    private long id;
    private String key;
    private int count = 0;
    private long at;

    public KeywordItem() {
        super();
    }

    public KeywordItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.count = in.readInt();
        this.at = in.readLong();
    }

    public static void createTableKeyword(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(KEYWORD_KEY + " text, ")
                .append(KEYWORD_COUNT + " integer not null, ")
                .append(KEYWORD_AT + " integer not null) ")
                .toString());
    }

    public static KeywordItem getKeywordItem(SQLiteDatabase db, String key) {
        List<KeywordItem> list = getKeywordItems(db, null, KEYWORD_KEY + " = ?", new String[]{key}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<KeywordItem> getKeywordItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<KeywordItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                KeywordItem item = new KeywordItem()
                        .setRowId(c)
                        .setKeywordKey(c)
                        .setKeywordCount(c)
                        .setKeywordAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateKeywordItem(SQLiteDatabase db, KeywordItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putKeywordKey(v)
                    .putKeywordCount(v)
                    .putKeywordAt(v);

            int rowAffected = db.update(TABLE_NAME, v, KEYWORD_KEY + " =? ", new String[]{item.getKeywordKey()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getKeywordKey());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(key);
        out.writeInt(count);
        out.writeLong(at);
    }

    public long getRowId() {
        return this.id;
    }

    public KeywordItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public KeywordItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public KeywordItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getKeywordKey() {
        return this.key;
    }

    public KeywordItem setKeywordKey(Cursor c) {
        this.key = s(c, KEYWORD_KEY);

        return this;
    }

    public KeywordItem setKeywordKey(String key) {
        this.key = key;

        return this;
    }

    public KeywordItem putKeywordKey(ContentValues v) {
        v.put(KEYWORD_KEY, key);

        return this;
    }

    public long getKeywordAt() {
        return this.at;
    }

    public KeywordItem setKeywordAt(Cursor c) {
        this.at = l(c, KEYWORD_AT);

        return this;
    }

    public KeywordItem setKeywordAt(long at) {
        this.at = at;

        return this;
    }

    public KeywordItem putKeywordAt(ContentValues v) {
        v.put(KEYWORD_AT, at);

        return this;
    }


    public int getKeywordCount() {
        return this.count;
    }

    public KeywordItem setKeywordCount(Cursor c) {
        this.count = i(c, KEYWORD_COUNT);

        return this;
    }

    public KeywordItem setKeywordCount(int count) {
        this.count = count;

        return this;
    }

    public KeywordItem putKeywordCount(ContentValues v) {
        v.put(KEYWORD_COUNT, count);

        return this;
    }
}
