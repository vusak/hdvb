package io.jmobile.video.browser.ui.fragment;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.data.BrowserItem;
import io.jmobile.video.browser.ui.BrowserActivity;

public class BrowserItemFragment extends Fragment {
    private static final String POSITON = "position";
    private static final String BROWSER_ITEM = "browser_item";
    private static final String DRAWABLE_RESOURE = "resource";

    private int screenWidth;
    private int screenHeight;
    private int position;
    private Bitmap homeBitmap = null;

    public static Fragment newInstance(BrowserActivity context, BrowserItem item, int pos, Bitmap bmp) {
        Bundle b = new Bundle();
        b.putInt(POSITON, pos);
//        b.putFloat(SCALE, scale);
        b.putParcelable(BROWSER_ITEM, item);
        b.putParcelable(DRAWABLE_RESOURE, bmp);

        return Fragment.instantiate(context, BrowserItemFragment.class.getName(), b);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWidthAndHeight();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        position = this.getArguments().getInt(POSITON);
        homeBitmap = this.getArguments().getParcelable(DRAWABLE_RESOURE);
        ImageView icon;
        TextView title;
        final ImageView thumbnail;
        ImageButton deleteButton;
        final BrowserItem item;
//        float scale = this.getArguments().getFloat(SCALE);
        item = this.getArguments().getParcelable(BROWSER_ITEM);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth / 3 * 2, screenHeight / 15 * 9);
        LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(screenWidth / 3 * 2, WindowManager.LayoutParams.WRAP_CONTENT);
        final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.item_browser, container, false);

//        LinearLayout root = (LinearLayout) linearLayout.findViewById(R.id.layout_root);
        icon = (ImageView) linearLayout.findViewById(R.id.image_browser_icon);

        LinearLayout titleLayout = (LinearLayout) linearLayout.findViewById(R.id.layout_title);
        titleLayout.setLayoutParams(lParams);
        thumbnail = (ImageView) linearLayout.findViewById(R.id.image_browser_thumbnail);
        thumbnail.setLayoutParams(layoutParams);
//        thumbnail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        title = (TextView) linearLayout.findViewById(R.id.text_browser_name);
        title.setText(item.getBrowserTitle());
//        deleteButton = (ImageButton) linearLayout.findViewById(R.id.button_browser_delete);
//        deleteButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.slide_delete);
//                ani.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        linearLayout.setVisibility(View.INVISIBLE);
//                        linearLayout.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                ((BrowserActivity) getActivity()).pagerDelete(item, position);
//                            }
//                        }, 10);
//
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//                linearLayout.startAnimation(ani);
//            }
//        });

//        if (item.getBrowserTitle().equals("HOME")) {
//            icon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_home_white_18));
//            if (homeBitmap == null)
//                thumbnail.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_public_gray_72));
//            else
//                thumbnail.setImageBitmap(homeBitmap);
//            thumbnail.setBackgroundColor(ContextCompat.getColor(getActivity(), homeBitmap == null ? R.color.set_title_background : R.color.transparent));
//        } else {
//            if (item.getBrowserThumbnail() != null) {
//                thumbnail.setImageBitmap(ImageUtil.getImage(item.getBrowserThumbnail()));
//                if (item.getBrowserIcon() != null)
//                    icon.setImageBitmap(ImageUtil.getImage(item.getBrowserIcon()));
//                else
//                    icon.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_public_white_18));
//            } else {
//                thumbnail.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_public_gray_72));
//            }
//            thumbnail.setBackgroundColor(ContextCompat.getColor(getActivity(), item.getBrowserThumbnail() == null ? R.color.set_title_background : R.color.transparent));
//        }

//        thumbnail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((BrowserActivity) getActivity()).selectTab(position);
//                Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.slide_show);
//                ani.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        ((BrowserActivity) getActivity()).selectedTab(position);
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });
//                linearLayout.startAnimation(ani);
//
//            }
//        });

//        //handling click event
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), ImageDetailsActivity.class);
//                intent.putExtra(DRAWABLE_RESOURE, imageArray[postion]);
//                startActivity(intent);
//            }
//        });

//        root.setScaleBoth(scale);
        return linearLayout;
    }

    /**
     * Get device screen width and height
     */
    private void getWidthAndHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenHeight = displaymetrics.heightPixels;
        screenWidth = displaymetrics.widthPixels;
    }
}
