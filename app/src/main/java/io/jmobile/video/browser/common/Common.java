package io.jmobile.video.browser.common;

public class Common {
    // cecil
//    public static final String YOUTUBE_API_KEY = "AIzaSyBWn8mrGgXJUa3dkuN6mT_B6195T-5cuGU";
    public static final String YOUTUBE_API_KEY = "AIzaSyDslsjD69ZJWaRtWde3mAnEkun5MJQH4nE";
    // HEADER_**
    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_NO_CACHE = "no-cache";
    public static final String HEADER_APPLICATION_VERSION = "APPLICATION_VERSION";
    public static final String HEADER_OS = "OS";
    public static final int TIMEOUT = 20000;
    public static final String BASE_URL = "http://jmid.jiran.com/";
    public static final String API_URL_GET_CATEGORY = "api/v/category";
    public static final String API_URL_GET_CHANNEL = "api/v/channel";
    public static final String API_URL_SEND_REPORT = "api/v/report";
    // RESULT_CODE_**
    public static final int RESULT_CODE_ERROR_OK = 0;
    public static final int RESULT_CODE_ERROR_NO_RESPONSE = -1;
    public static final int RESULT_CODE_ERROR_UNKNOWN = -2;
    public static final int RESULT_CODE_ERROR_CANCELED = -3;
    public static final String PARAM_PROGRESS_MESSAGE = "progressMessage";
    public static final String PARAM_SHOW_ERROR_MESSAGE = "showErrorMessage";
    public static final String PARAM_PART = "part";
    public static final String PARAM_CHART = "chart";
    public static final String PARAM_REGION_CODE = "regionCode";
    public static final String PARAM_KEY = "key";
    public static final String PARAM_MAX_RESULTS = "maxResults";
    public static final String PARAM_VIDEO_CATEGORY_ID = "videoCategoryId";
    public static final String PARAM_PAGE_TOKEN = "pageToken";
    public static final String PARAM_Q = "q";
    public static final String PARAM_EVENT_TYPE = "eventType";
    public static final String PARAM_ORDER = "order";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_VER = "ver";
    public static final String PARAM_CHANNEL_ID = "channelId";
    public static final String PARAM_C_IDX = "c_idx";
    public static final String PARAM_M_IDX = "m_idx";
    public static final String PARAM_M_NAME = "m_name";
    public static final String PARAM_M_EMAIL = "m_email";
    public static final String PARAM_COMMENT = "comment";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_KIND = "kind";
    public static final String TAG_ETAG = "etag";
    public static final String TAG_ITEMS = "items";
    public static final String TAG_ID = "id";
    public static final String TAG_SNIPPET = "snippet";
    public static final String TAG_CHANNEL_ID = "channelId";
    public static final String TAG_TITLE = "title";
    public static final String TAG_ASSIGNABLE = "assignable";
    public static final String TAG_NEXT_PAGE_TOKEN = "nextPageToken";
    public static final String TAG_DESCRIPTION = "description";
    public static final String TAG_PUBLISHED_AT = "publishedAt";
    public static final String TAG_THUMBNAILS = "thumbnails";
    public static final String TAG_THUMBNAIL = "thumbnail";
    public static final String TAG_DEFAULT = "default";
    public static final String TAG_MEDIUM = "medium";
    public static final String TAG_HIGH = "high";
    public static final String TAG_URL = "url";
    public static final String TAG_REGION_CODE = "regionCode";
    public static final String TAG_PAGE_INFO = "pageInfo";
    public static final String TAG_VIDEO_ID = "videoId";
    public static final String TAG_CHANNEL_TITLE = "channelTitle";
    public static final String TAG_CHK = "chk";
    public static final String TAG_IDX = "idx";
    public static final String TAG_NAME = "name";
    public static final String TAG_TYPE = "type";
    public static final String TAG_VALUE = "value";
    public static final String TAG_POSITION = "position";
    public static final String TAG_RESULT = "result";
    public static final String TAG_LIST = "list";
    public static final String TAG_ERROR = "error";
    public static final String TAG_STATUS = "status";
    public static final String TAG_LIVE_BROADCAST = "b_livebroadcast";
    public static final String TAG_CH_ID= "ch_idx";
    public static final String TAG_CONTENTS = "contents";
    public static final String TAG_V_ID = "video_id";
    public static final String TAG_REG_DATE = "reg_date";
    public static final String ARG_CATEGORY_LIST = "category_list";
    public static final String ARG_VIDEO_LIST = "video_list";
    public static final String ARG_SEARCH_RESULT_LIST = "search_result_list";
    public static final String ARG_SEARCH_LIVE_LIST = "search_live_list";
    public static final String ARG_SEARCH_LIVE_CATEGORY_ID = "search_live_category_id";
    public static final String ARG_CATEGORY_UPDATE = "category_update";
    public static final String ARG_RESULT = "result";
    public static final String ARG_CHANNEL_LIST = "channel_list";
    public static final String ARG_MESSAGE = "message";
    public static final String ARG_IS_LIVE = "is_live";
    public static final int RESULT_TYPE_ITEM = 0;
    public static final int RESULT_TYPE_LIST = 1;
    public static String YOUTUBE_BASE_URL = "https://www.googleapis.com/youtube/v3/";
    public static String YOUTUBE_URL_SEARCH = "search";
    public static String YOUTUBE_URL_VIDEO_CATEGORIES = "videoCategories";
    public static String YOUTUBE_URL_VIDEOS = "videos";
    public static int YOUTUBE_API_CODE_VIDEO_CATEGORIES = 1000;
    public static int YOUTUBE_API_CODE_VIDEOS = 1001;
    public static int YOUTUBE_API_CODE_SEARCH = 1002;
    public static int YOUTUBE_API_CODE_SEARCH_LIVE = 1003;
    public static int YOUTUBE_API_CODE_SEARCH_CHANNEL = 1004;
    public static int API_CODE_GET_CATEGORY = 1005;
    public static int API_CODE_GET_CHANNEL = 1006;
    public static int API_CODE_SEND_REPORT = 1007;
    public static String INTENT_GO_TO_URL = "go_to_url";
    public static String INTENT_CHANNEL_ID = "channel_id";
    public static String INTENT_CHANNEL_TITLE = "channel_title";
    public static String INTENT_CATEGORY_ID = "category_id";
    public static String INTENT_CATEGORY_TITLE = "category_title";
    public static String INTENTFILTER_BROADCAST_TIMER = "broadcast_timer";

    public static String checkVideoExtension(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp.*")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4.*")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac.*")) {
            fileExt = ".flac";
        } else if (url.matches(".*.ogg.*")) {
            fileExt = ".ogg";
        } else if (url.matches(".*.wma.*")) {
            fileExt = ".wma";
        } else if (url.matches(".*.ape.*")) {
            fileExt = ".ape";
        } else if (url.matches(".*.avi.*")) {
            fileExt = ".avi";
        } else if (url.matches(".*.mp3.*")) {
            fileExt = ".mp3";
        } else if (url.matches(".*.wmv.*")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.wma.*")) {
            fileExt = ".wma";
        } else if (url.matches(".*.mpg.*")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv.*")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv.*")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf.*")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov.*")) {
            fileExt = ".mov";
        } else {
            fileExt = ".mp4";
        }
        return fileExt;
    }

    public static String checkAvailableDownload(String url) {
        String fileExt = "";
        if (url.matches(".*.3gp.*")) {
            fileExt = ".3gp";
        } else if (url.matches(".*.mp4.*")) {
            fileExt = ".mp4";
        } else if (url.matches(".*.flac.*")) {
            fileExt = ".flac";
        } else if (url.matches(".*.wmv.*")) {
            fileExt = ".wmv";
        } else if (url.matches(".*.mpg.*")) {
            fileExt = ".mpg";
        } else if (url.matches(".*.flv.*")) {
            fileExt = ".flv";
        } else if (url.matches(".*.mkv.*")) {
            fileExt = ".mkv";
        } else if (url.matches(".*.swf.*")) {
            fileExt = ".swf";
        } else if (url.matches(".*.mov.*")) {
            fileExt = ".mov";
        } else {
            fileExt = "novideo";
        }
        return fileExt;
    }
}
