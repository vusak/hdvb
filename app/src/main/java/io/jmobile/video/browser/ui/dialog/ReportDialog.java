package io.jmobile.video.browser.ui.dialog;


import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseDialogFragment;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.network.Api;

public class ReportDialog extends BaseDialogFragment {

    public static final String TAG = ReportDialog.class.getSimpleName();
    Context context;

    TextView nameText, emailText, messageText;
    View nameLine, emailLine;
    ImageView nameCheckImage, emailCheckImage;
    ImageButton nameDelButton, emailDelButton;
    EditText nameEdit, emailEdit, messageEdit;
    Button sendButton, cancelButton;

    Api api;

    public static ReportDialog newInstance(String tag, Context context) {
        ReportDialog d = new ReportDialog();
        d.context = context;

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);
        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_report;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.9f;
            params.width = (int) (dm.widthPixels * 0.85f);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_SEND_REPORT) {
            boolean result = m.getData().getBoolean(Common.ARG_RESULT);
            if (result) {
                String message = m.getData().getString(Common.ARG_MESSAGE);
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }

            dismiss();
        }
    }

    private void initView() {
        nameText = fv(R.id.text_name);
        nameCheckImage = fv(R.id.image_check_name);
        nameLine = fv(R.id.view_line_name);
        nameDelButton = fv(R.id.button_delete_name);
        nameDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nameEdit.setText("");
            }
        });
        nameEdit = fv(R.id.edit_name);
        nameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = s.toString().isEmpty();
                nameDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
            }
        });
        nameEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                boolean empty = nameEdit.getText().toString().isEmpty();

                if (hasFocus) {
                    nameText.setTextColor(ContextCompat.getColor(context, R.color.edit_input));
                    nameLine.setBackgroundResource(R.color.edit_input);
                    nameDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
                    nameCheckImage.setVisibility(View.GONE);
                } else {
                    nameText.setTextColor(ContextCompat.getColor(context, empty ? R.color.edit_empty : R.color.edit_check));
                    nameLine.setBackgroundResource(empty ? R.color.edit_empty : R.color.edit_check);
                    nameDelButton.setVisibility(View.GONE);
                    nameCheckImage.setVisibility(empty ? View.GONE : View.VISIBLE);
                }
            }
        });

        nameEdit.postDelayed(new Runnable() {
            @Override
            public void run() {
                Util.showKeyBoard(nameEdit);
            }
        }, 100);

        emailText = fv(R.id.text_email);
        emailCheckImage = fv(R.id.image_check_email);
        emailLine = fv(R.id.view_line_email);
        emailDelButton = fv(R.id.button_delete_email);
        emailDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailEdit.setText("");
            }
        });
        emailEdit = fv(R.id.edit_email);
        emailEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = s.toString().isEmpty();
                emailDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);

            }
        });

        emailEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                boolean empty = emailEdit.getText().toString().isEmpty();

                if (hasFocus) {
                    emailText.setTextColor(ContextCompat.getColor(context, R.color.edit_input));
                    emailLine.setBackgroundResource(R.color.edit_input);
                    emailDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
                    emailCheckImage.setVisibility(View.GONE);
                } else {
                    emailText.setTextColor(ContextCompat.getColor(context, empty ? R.color.edit_empty : R.color.edit_check));
                    emailLine.setBackgroundResource(empty ? R.color.edit_empty : R.color.edit_check);
                    emailDelButton.setVisibility(View.GONE);
                    emailCheckImage.setVisibility(empty ? View.GONE : View.VISIBLE);
                }
            }
        });

        messageText = fv(R.id.text_description);
        messageEdit = fv(R.id.edit_message);
        messageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        messageEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                boolean empty = messageEdit.getText().toString().isEmpty();

                if (hasFocus) {
                    messageText.setTextColor(ContextCompat.getColor(context, R.color.edit_input));
                    messageEdit.setBackgroundResource(R.drawable.edit_feedback_background_input);
                } else {
                    messageText.setTextColor(ContextCompat.getColor(context, empty ? R.color.edit_empty : R.color.edit_check));
                    messageEdit.setBackgroundResource(empty ? R.drawable.edit_feedback_background : R.drawable.edit_feedback_background_check);
                }
            }
        });

        sendButton = fv(R.id.button_positive);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(nameEdit);
                if (!checkContent())
                    return;

                api.sendReport(emailEdit.getText().toString(), nameEdit.getText().toString(), messageEdit.getText().toString());
                if (positiveListener != null)
                    positiveListener.onDialogPositive(ReportDialog.this, tag);
                dismiss();
            }
        });

        cancelButton = fv(R.id.button_negative);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(nameEdit);
                if (negativeListener != null)
                    negativeListener.onDialogNegative(ReportDialog.this, tag);
                dismiss();
            }
        });
    }

    private boolean checkContent() {
        if (TextUtils.isEmpty(emailEdit.getText().toString())) {
            Toast.makeText(getActivity(), r.s(R.string.dlg_edit_hint_email), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (TextUtils.isEmpty(messageEdit.getText().toString())) {
            Toast.makeText(getActivity(), r.s(R.string.dlg_edit_hint_description), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
