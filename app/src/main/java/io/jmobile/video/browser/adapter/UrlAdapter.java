package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;

public class UrlAdapter extends BaseAdapter {

    LayoutInflater inflater;
    private Context context;
    private ArrayList<String> items = new ArrayList<>();
    private String keyword = "";

    public UrlAdapter(Context context, ArrayList<String> items) {
        this.context = context;
        this.items = items;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Holder holder;
        String url = items.get(position);
        int index = url.indexOf("/");
        String host = url;
        if (index > 0)
            host = url.substring(0, index);

        if (convertView == null) {
            holder = new Holder();

            convertView = inflater.inflate(R.layout.item_url, null);
            holder.urlText = (TextView) convertView.findViewById(R.id.text_url);
            holder.hostText = (TextView) convertView.findViewById(R.id.text_host);
            convertView.setTag(holder);
        } else
            holder = (Holder) convertView.getTag();

        index = url.indexOf(keyword);
        SpannableStringBuilder ssb = new SpannableStringBuilder(url);
        ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.red_a100)), index, index + keyword.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.urlText.setText(ssb);
        holder.hostText.setText(host);
        return convertView;
    }

    public void setItems(ArrayList<String> _items, String keyword) {
        this.items = _items;
        this.keyword = keyword;
        notifyDataSetChanged();
    }

    private class Holder {
        TextView urlText;
        TextView hostText;
    }

}
