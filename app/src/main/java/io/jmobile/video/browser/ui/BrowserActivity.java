package io.jmobile.video.browser.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.CarouselPagerAdapter;
import io.jmobile.video.browser.adapter.RecommendAdapter;
import io.jmobile.video.browser.adapter.UrlAdapter;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.base.BaseClickableViewPager;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.BrowserItem;
import io.jmobile.video.browser.data.RecommendItem;
import io.jmobile.video.browser.data.VideoHistoryItem;
import io.jmobile.video.browser.ui.fragment.BrowserFragment;

public class BrowserActivity extends BaseActivity implements View.OnClickListener, BrowserFragment.BrowserFragmentListsner {

    // front layout
    LinearLayout rootLayout;
    ImageButton backButton;
    Button urlButton;
    RecyclerView lv;
    LinearLayoutManager manager;
    ArrayList<RecommendItem> list;
    RecommendAdapter adapter;

    // Url Layout
    LinearLayout urlLayout;
    ImageView searchImage;
    ImageButton urlDelButton;
    EditText urlEdit;
    TextView urlText;
    ListView urlLv;
    UrlAdapter urlAdapter;
    ArrayList<String> urlList, favList;

    // Browser Tab Layout
    LinearLayout newTabLayout;
    Bitmap homeBitmap = null;
    ArrayList<BrowserFragment> browserFragments;
    ArrayList<BrowserItem> browserList;
    BrowserFragment currBrowserFragment;
    CarouselPagerAdapter browserAdapter;
    BaseClickableViewPager pager;
    ImageButton clearAllButton, newButton;
    TextView returnText;
    LinearLayout returnButton;

    // Bottom Buttons
    ImageButton prevButton, nextButton, homeButton, historyButton;
    TextView tabText;


    LinearLayout browserLayout;
    private Handler urlSearchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                if (urlList == null)
                    urlList = new ArrayList<>();
                else
                    urlList.clear();
                String keyword = urlEdit.getText().toString();
                for (String temp : favList) {
                    if (temp.contains(keyword))
                        urlList.add(temp);
                }

                urlAdapter.setItems(urlList, keyword);
            }
        }
    };

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.main));
        }
        setContentView(R.layout.activity_browser);

//        initView();
//
//        Intent intent = getIntent();
//        if (intent != null) {
//            String url = intent.getStringExtra(Common.INTENT_GO_TO_URL);
//            if (!TextUtils.isEmpty(url)) {
//                urlLayout.setVisibility(View.GONE);
//                visibleNewTabLayout(false);
//                visibleBrowser(true);
//
//                Bundle bundle = new Bundle();
//                bundle.putString(Common.INTENT_GO_TO_URL, url);
//                currBrowserFragment.setArguments(bundle);
//            }
//        }
    }

    @Override
    public void onBackPressed() {
        if (urlLayout.getVisibility() == View.VISIBLE) {
            urlLayout.setVisibility(View.GONE);
        } else if (browserLayout.getVisibility() == View.VISIBLE) {
            if (!currBrowserFragment.onPrevButtonClicked())
                visibleBrowser(false);
//            browserLayout.setVisibility(View.GONE);
        } else if (newTabLayout != null && newTabLayout.getVisibility() == View.VISIBLE) {
            visibleNewTabLayout(false);
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_bottom_prev:
                if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE
                        && urlLayout != null && urlLayout.getVisibility() == View.GONE)
                    currBrowserFragment.onPrevButtonClicked();
                break;

            case R.id.btn_bottom_next:
                if (currBrowserFragment != null && browserLayout.getVisibility() == View.VISIBLE
                        && urlLayout != null && urlLayout.getVisibility() == View.GONE)
                    currBrowserFragment.onNextButtonClicked();
                break;

            case R.id.btn_bottom_home:
                if (currBrowserFragment != null)
                    currBrowserFragment.setHomeData();
                visibleBrowser(false);
                if (urlLayout != null && urlLayout.getVisibility() == View.VISIBLE)
                    urlLayout.setVisibility(View.GONE);
                break;

            case R.id.btn_bottom_view:
                Intent intent = new Intent(BrowserActivity.this, HistoryVideoActivity.class);
                startActivity(intent);
                break;

            case R.id.text_bottom_new_tab:
                visibleNewTabLayout(true);
                break;
        }
    }

    @Override
    public void onHomeButtonClick() {

    }

    //    private void initView() {
//        rootLayout = fv(R.id.layout_browser_home);
//        urlLayout = fv(R.id.layout_browser_url);
//        urlLayout.setVisibility(View.GONE);
//
//        backButton = fv(R.id.button_back);
//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        urlButton = fv(R.id.button_url);
//        urlButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (rootLayout != null) {
//                    rootLayout.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            homeBitmap = ImageUtil.getScreenShot(BrowserActivity.this, rootLayout);
//                            db.insertOrUpdateExtraItem(ImageUtil.getImageBytes(homeBitmap));
//                        }
//                    });
//                }
//                urlLayout.setVisibility(View.VISIBLE);
//                Util.showKeyBoard(urlEdit);
//
//                if (favList == null)
//                    favList = new ArrayList<String>();
//                else
//                    favList.clear();
//                ArrayList<FavoriteItem> list = new ArrayList();
//                list.addAll(db.getFavoriteItems());
//                if (list == null || list.size() <= 0)
//                    return;
//
//                for (FavoriteItem item : list) {
//                    String url = item.getFavoriteSite().replace("http://", "");
//                    url = url.replace("https://", "");
//                    favList.add(url);
//                }
//            }
//        });
//
//        lv = fv(R.id.lv);
//        list = new ArrayList<>();
//        manager = new GridLayoutManager(this, 5);
//        lv.setLayoutManager(manager);
//        adapter = new RecommendAdapter(this, R.layout.item_recommend, list, new ReAdapter.ReOnItemClickListener() {
//            @Override
//            public void OnItemClick(int position, Object item) {
//                String url = ((RecommendItem) item).getUrl();
//                if (!url.contains("http://") && !url.contains("https://"))
//                    url = "http://" + url;
//                goToBrowser(url);
//            }
//
//            @Override
//            public void OnItemLongClick(int position, Object item) {
//
//            }
//        });
//        lv.setAdapter(adapter);
//        initRecommendList();
//
//        prevButton = fv(R.id.btn_bottom_prev);
//        prevButton.setOnClickListener(this);
//        nextButton = fv(R.id.btn_bottom_next);
//        nextButton.setOnClickListener(this);
//        homeButton = fv(R.id.btn_bottom_home);
//        homeButton.setOnClickListener(this);
//        historyButton = fv(R.id.btn_bottom_view);
//        historyButton.setOnClickListener(this);
//        tabText = fv(R.id.text_bottom_new_tab);
//        tabText.setOnClickListener(this);
//        urlEdit = fv(R.id.edit_url);
//        urlEdit.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                urlText.setText(s.toString());
//                urlSearchHandler.removeCallbacksAndMessages(null);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                boolean empty = s.toString().isEmpty();
//                searchImage.setVisibility(empty ? View.GONE : View.VISIBLE);
//                urlDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
//                urlText.setVisibility(empty ? View.GONE : View.VISIBLE);
//                urlText.setText(s.toString());
//                urlLv.setVisibility(empty ? View.GONE : View.VISIBLE);
//
//                if (!empty)
//                    urlSearchHandler.sendEmptyMessage(0);
//            }
//        });
//        urlEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_GO) {
//                    Util.hideKeyBoard(BrowserActivity.this);
//                    String url = urlEdit.getText().toString();
//
//                    String SCHEME = "http://";
//                    if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
////                            String googleSearch = "http://www.google.com/m?q=";
//                        sp.setBrowserUrl(getSearchEnging() + url);
//                    } else if (url.matches("http.*")) { // http로 시작
//                        sp.setBrowserUrl(url);
//                    } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
//                            || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
//                            || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
//                            || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
//                            || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
//                        sp.setBrowserUrl(SCHEME + url);
//                    } else {
////                            String googleSearch = "http://www.google.com/m?q=";
//                        sp.setBrowserUrl(getSearchEnging() + url);
//
//                    }
//
////                        btnBack.setVisibility(View.VISIBLE);
//                    goToBrowser(sp.getBrowserUrl());
//                }
//                return false;
//            }
//
//        });
//
//        searchImage = fv(R.id.image_search);
//        searchImage.setVisibility(View.GONE);
//        urlDelButton = fv(R.id.button_delete);
//        urlDelButton.setVisibility(View.GONE);
//        urlDelButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                urlEdit.setText("");
//            }
//        });
//
//        urlText = fv(R.id.text_url);
//        urlText.setVisibility(View.GONE);
//        urlText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Util.hideKeyBoard(BrowserActivity.this);
//                String url = urlText.getText().toString();
//
//                String SCHEME = "http://";
//                if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
////                            String googleSearch = "http://www.google.com/m?q=";
//                    sp.setBrowserUrl(getSearchEnging() + url);
//                } else if (url.matches("http.*")) { // http로 시작
//                    sp.setBrowserUrl(url);
//                } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
//                        || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
//                        || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
//                        || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
//                        || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
//                    sp.setBrowserUrl(SCHEME + url);
//                } else {
////                            String googleSearch = "http://www.google.com/m?q=";
//                    sp.setBrowserUrl(getSearchEnging() + url);
//
//                }
//
////                        btnBack.setVisibility(View.VISIBLE);
//                goToBrowser(sp.getBrowserUrl());
//            }
//        });
//        urlLv = fv(R.id.list_url);
//        urlLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                if (urlList == null || urlList.size() < position)
//                    return;
//                String url = urlList.get(position);
//                if (!url.startsWith("http://") && !url.startsWith("https://"))
//                    url = "http://" + url;
//                urlEdit.setText(url);
//                goToBrowser(url);
//            }
//        });
//        urlList = new ArrayList<>();
//        favList = new ArrayList<>();
//        urlAdapter = new UrlAdapter(this, urlList);
//        urlLv.setAdapter(urlAdapter);
//
//        newTabLayout = fv(R.id.layout_new_tab);
//        newTabLayout.setVisibility(View.GONE);
//
//        pager = fv(R.id.viewpager_new_tab);
//        DisplayMetrics metrics = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        final int pageMargin = ((metrics.widthPixels / 8) * 2);
//        pager.setPageMargin(-pageMargin);
//        pager.setPageTransformer(false, new ViewPager.PageTransformer() {
//            @Override
//            public void transformPage(View page, float position) {
//                int pageWidth = pager.getMeasuredWidth() - pager.getPaddingLeft() - pager.getPaddingRight();
//                int pageHeight = pager.getHeight();
//                int paddingLeft = pager.getPaddingLeft();
//                float transformPos = (float) (page.getLeft() - (pager.getScrollX() + paddingLeft)) / pageWidth;
//
//                final float normalizedposition = Math.abs(Math.abs(transformPos) - 1);
//                page.setAlpha(normalizedposition + 0.5f);
//
//                float upTranslation = -pageHeight / 10f;
//                if (transformPos < -1) {// [-Infinity,-1)
//                    page.setTranslationY(0);
//                } else if (transformPos < 0) { // [-1,0)
//                    float translationY = upTranslation * (transformPos + 1f);
//                    page.setTranslationY(translationY);
//                } else if (transformPos == 0) { // 0
//                    page.setTranslationY(upTranslation);
//                } else if (transformPos <= 1) { // (0,1]
//                    float translationY = upTranslation * (1f - transformPos);
//                    page.setTranslationY(translationY);
//                } else { // (1,+Infinity]
//                    page.setTranslationY(0);
//                }
//            }
//        });
////        pager.setOnItemClickLIstener(new BaseClickableViewPager.OnItemClickListener() {
////            @Override
////            public void onItemClick(int position) {
////
////            }
////        });
//
//        browserFragments = new ArrayList<>();
//
//        clearAllButton = fv(R.id.button_clear_all);
//        clearAllButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (fdf(MessageDialog.TAG) == null) {
//                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
//                    dlg.setMessage(r.s(R.string.dlg_msg_delete_all_tab));
//                    dlg.setNegativeLabel(r.s(R.string.cancel));
//                    dlg.setPositiveLabel(r.s(R.string.close));
//                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
//                        @Override
//                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
//                            db.deleteAllBrowserItems();
//                            refreshBrowserList();
//                            visibleNewTabLayout(false);
//                        }
//                    });
//
//                    sdf(dlg);
//                }
//
//            }
//        });
//
//        newButton = fv(R.id.button_new);
//        newButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BrowserItem item = new BrowserItem();
//                item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
//                item.setBrowserTitle("HOME");
//                item.setBrowserUrl("HOME");
//                item.setBrowserPrivacy(false);
//                item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
//                item.setBrowserAt(System.currentTimeMillis());
//                currBrowserFragment = new BrowserFragment();
//                currBrowserFragment.setListener(BrowserActivity.this);
//                currBrowserFragment.setBrowserItem(item);
//                browserFragments.add(currBrowserFragment);
////                browserFragments.add(currBrowserFragment);
////                browserViewAdapter.addFrag(currBrowserFragment, item.getBrowserId());
////                browserViewAdapter.notifyDataSetChanged();
//                browserList.add(item);
//                sp.setCurrentBrowserIndex(browserList.size() - 1);
//                browserAdapter.notifyDataSetChanged();
//                pager.setCurrentItem(sp.getCurrentBrowserIndex(), true);
//                db.insertOrUpdateBrowserItem(item);
//
//
////                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
////                transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
////                transaction.commit();
////                browserViewpager.setCurrentItem(sp.getCurrentBrowserIndex());
//                visibleNewTabLayout(false);
//            }
//        });
//        returnText = fv(R.id.text_return);
//        returnButton = fv(R.id.button_return);
//        returnButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sp.setCurrentBrowserIndex(pager.getCurrentItem());
//                visibleNewTabLayout(false);
//            }
//        });
//        currBrowserFragment = new BrowserFragment();
//        currBrowserFragment.setListener(BrowserActivity.this);
//        browserLayout = fv(R.id.layout_fragment_browser);
//        browserLayout.setVisibility(View.INVISIBLE);
////        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
////        transaction.add(R.id.layout_fragment_browser, currBrowserFragment);
////        transaction.commit();
////        browserViewpager = fv(R.id.viewpager_browser);
////        browserViewpager.setEnableSwipe(false);
////        browserViewpager.setVisibility(View.INVISIBLE);
////
////        browserViewAdapter = new BrowserViewPagerAdapter(getSupportFragmentManager());
////        browserViewpager.setAdapter(browserViewAdapter);
//
//        browserList = new ArrayList<>();
//        browserList.addAll(db.getBrowserItems());
//
//        browserAdapter = new CarouselPagerAdapter(this, getSupportFragmentManager(), browserList);
//        pager.setAdapter(browserAdapter);
//
//        pager.addOnPageChangeListener(browserAdapter);
//        pager.setCurrentItem(sp.getCurrentBrowserIndex());
//        pager.setOffscreenPageLimit(2);
//
////        browserFragments = new ArrayList<>();
////
//        refreshBrowserList();
//
//    }

    private void initRecommendList() {
        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();

        String[] nameList = getResources().getStringArray(R.array.recommend_name);
        String[] urlList = getResources().getStringArray(R.array.recommend_url);
        TypedArray iconList = getResources().obtainTypedArray(R.array.recommend_icon);

        for (int i = 0; i < nameList.length; i++) {
            RecommendItem item = new RecommendItem();
            item.setIndex(i);
            item.setName(nameList[i]);
            item.setUrl(urlList[i]);
            item.setIconId(iconList.getResourceId(i, -1));
            list.add(item);
        }
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    @SuppressLint("NewApi")
    private void visibleNewTabLayout(boolean show) {
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            if (show)
//                getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black));
//        }

        refreshBrowserList();

//        currBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
////        BrowserItem item = currBrowserFragment.getBrowserItem();
//        if (show) {
//            if (currBrowserFragment != null) {
//                currBrowserFragment.stopWebViewLoading();
//            }
//            currBrowserFragment.clearCurrData();
//            if (browserLayout.getVisibility() != View.VISIBLE && urlLayout.getVisibility() != View.VISIBLE) {
//                homeBitmap = ImageUtil.getScreenShot(this, rootLayout);
//                db.insertOrUpdateExtraItem(ImageUtil.getImageBytes(homeBitmap));
//            }
//            browserAdapter.setHomeBitmap(homeBitmap);
//            pager.setCurrentItem(sp.getCurrentBrowserIndex());
//
//            newTabLayout.setVisibility(View.VISIBLE);
//            newTabLayout.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_up));
//
//        } else {
//            if (item != null && item.getBrowserTitle().equals("HOME"))
//                visibleBrowser(false);
//            else
//                visibleBrowser(true);
//
//
//            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//            transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
//            transaction.commit();
//
//            sp.setBrowserTabCount(browserList.size());
//            tabText.setText("" + browserList.size());
//            returnText.setText("" + browserList.size());
//
//            if (pager.getCurrentItem() == sp.getCurrentBrowserIndex())
//                pager.setCurrentItem(pager.getCurrentItem() == 0 ? 1 : pager.getCurrentItem() - 1);
//            newTabLayout.setVisibility(View.GONE);
//
//        }

    }

    private void goToBrowser(String url) {
        Util.hideKeyBoard(urlEdit);
        urlLayout.setVisibility(View.GONE);
        if (url.matches("(?i).*youtube.*")) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return;
        }

        visibleBrowser(true);
        currBrowserFragment.goUrl(url);
    }

    private void refreshBrowserList() {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        browserList.clear();
        browserFragments.clear();
        browserList.addAll(db.getBrowserItems());
//        browserViewAdapter.clear();
//        browserFragments = new ArrayList<>();

        if (browserList == null || browserList.size() <= 0) {
            BrowserItem item = new BrowserItem();
            item.setBrowserId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
            item.setBrowserTitle("HOME");
            item.setBrowserUrl("HOME");
            item.setBrowserPrivacy(false);
            item.setBrowserOrder((int) db.getNextDBId(BrowserItem.TABLE_NAME));
            item.setBrowserAt(System.currentTimeMillis());
            browserList.add(item);

            currBrowserFragment = new BrowserFragment();
            currBrowserFragment.setListener(this);
//            currBrowserFragment.setBrowserItem(item);
            browserFragments.add(currBrowserFragment);
            sp.setCurrentBrowserIndex(0);

//            transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, item.getBrowserId());
//            transaction.hide(currBrowserFragment);
//            transaction.commit();
//            browserFragments.add(currBrowserFragment);
//            browserViewAdapter.addFrag(currBrowserFragment, item.getBrowserId());

            db.insertOrUpdateBrowserItem(item);
        } else {
            for (BrowserItem item : browserList) {
                BrowserFragment bFragment = new BrowserFragment();
//                bFragment.setBrowserItem(item);
                bFragment.setListener(this);
                browserFragments.add(bFragment);
            }

            int index = sp.getCurrentBrowserIndex();
            if (index < 0 || index >= browserList.size()) {
                sp.setCurrentBrowserIndex(0);
            }

            currBrowserFragment = browserFragments.get(sp.getCurrentBrowserIndex());
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_fragment_browser, currBrowserFragment, browserList.get(sp.getCurrentBrowserIndex()).getBrowserId());
        transaction.commit();
//        browserViewAdapter.notifyDataSetChanged();
        browserAdapter.notifyDataSetChanged();
        pager.setCurrentItem(sp.getCurrentBrowserIndex());
//        browserViewpager.setCurrentItem(sp.getCurrentBrowserIndex());
        sp.setBrowserTabCount(browserList.size());
        tabText.setText("" + browserList.size());
        returnText.setText("" + browserList.size());
    }

    @SuppressLint("NewApi")
    private void visibleBrowser(boolean show) {
        browserLayout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

        if (!show) {
            if (currBrowserFragment != null) {
                currBrowserFragment.stopWebViewLoading();
            }
            currBrowserFragment.clearCurrData();
//            refreshMainList();
        } else {
            if (currBrowserFragment != null) {
                currBrowserFragment.onResume();
            }
        }
    }

    private String getSearchEnging() {
        String engine = "http://www.google.com/m?q=";
        switch (sp.getSearchEngine()) {
            case 0: // Google
                engine = "http://www.google.com/m?q=";
                break;

            case 1: // Naver
                engine = "http://m.search.naver.com/search.naver?query=";
                break;

            case 2: // bing
                engine = "http://www.bing.com/search?q=";
                break;
        }

        return engine;
    }

    @Override
    public void onUrlEditClick(String url) {
        if (urlLayout == null || urlEdit == null)
            return;

        urlLayout.setVisibility(View.VISIBLE);
        if (url != null && !url.isEmpty()) {
            urlEdit.setText(url);
            urlEdit.setSelection(0, url.length());
        }
        Util.showKeyBoard(urlEdit);


    }

    @Override
    public void onNewTabClick() {

    }

    @Override
    public void addVideoHistoryUrl(String site, String url) {
        if (url == null || TextUtils.isEmpty(url) || url.matches("(?i).*youtube.*"))
            return;

        VideoHistoryItem item = new VideoHistoryItem();
        item.setVideoHistoryId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
        item.setVideoHistoryUrl(url);
        item.setVideoHistorySite(site);
        item.setVideoHistoryAt(System.currentTimeMillis());

        new AddVideoHistoryItem().execute(item);
    }

    private class AddVideoHistoryItem extends AsyncTask<VideoHistoryItem, Void, VideoHistoryItem> {
        @Override
        protected VideoHistoryItem doInBackground(VideoHistoryItem... params) {
            VideoHistoryItem item = params[0];
            String path = item.getVideoHistoryUrl();
            if (!TextUtils.isEmpty(path)) {
                long totalSize = -1;
                long time = -1;
                HttpURLConnection conn = null;
                try {
                    URL url = new URL(path);
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setConnectTimeout(1000);
                    conn.setReadTimeout(1000);
                    conn.setRequestMethod("HEAD");
                    totalSize = (long) conn.getContentLength();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (conn != null)
                        conn.disconnect();
                }
                item.setVideoHistoryFileSize(totalSize);
            }

            Bitmap bitmap = null;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                if (Build.VERSION.SDK_INT >= 14)
                    mediaMetadataRetriever.setDataSource(path, new HashMap<String, String>());
                else
                    mediaMetadataRetriever.setDataSource(path);
                bitmap = mediaMetadataRetriever.getFrameAtTime();
                String time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                if (TextUtils.isEmpty(time))
                    item.setVideoHistoryFileTime(0);
                else {
                    long timeInmillisec = Long.parseLong(time);
                    item.setVideoHistoryFileTime(timeInmillisec);
                }
                String title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                if (!TextUtils.isEmpty(title))
                    item.setVideoHistoryTitle(title);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                {
                    if (mediaMetadataRetriever != null)
                        mediaMetadataRetriever.release();
                }
            }
            if (bitmap != null)
                item.setVideoHistoryThumbnail(ImageUtil.getImageBytes(bitmap));

            return item;
        }

        @Override
        protected void onPostExecute(VideoHistoryItem videoHistoryItem) {
            db.insertorUpdateVideoHistoryItem(videoHistoryItem);
        }
    }
}
