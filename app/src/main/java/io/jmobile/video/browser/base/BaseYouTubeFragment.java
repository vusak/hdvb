package io.jmobile.video.browser.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import io.jmobile.video.browser.BaseApplication;
import io.jmobile.video.browser.common.DBController;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.SPController;
import io.jmobile.video.browser.common.Util;


public abstract class BaseYouTubeFragment extends YouTubePlayerSupportFragment {
    protected BaseApplication app;
    protected SPController sp;
    protected DBController db;
    protected BaseApplication.ResourceWrapper r;

    private View v;

    public abstract int getLayoutId();

    public abstract void onCreateView(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app = (BaseApplication) getActivity().getApplication();
        sp = app.getSPController();
        db = app.getDBController();
        r = app.getResourceWrapper();
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(getLayoutId(), container, false);

        onCreateView(savedInstanceState);

        return v;
    }

    protected <T extends BaseYouTubeFragment> T ff(String tag) {
        return Util.fyf(getFragmentManager(), tag);
    }

    protected <T extends BaseDialogFragment> T fdf(String tag) {
        return Util.fdf(getFragmentManager(), tag);
    }

    protected void hdf(String tag) {
        Util.hdf(getFragmentManager(), tag);
    }

    protected void sdf(BaseDialogFragment d) {
        Util.sdf(getFragmentManager(), d);
    }

    protected <T extends View> T fv(int id) {
        try {
            return (T) v.findViewById(id);
        } catch (ClassCastException e) {
            log(e.getMessage());
            return null;
        }
    }

    protected void log(String msg) {
        LogUtil.log(getClass().getSimpleName(), msg);
    }

    protected void log(Throwable tr) {
        LogUtil.log(getClass().getSimpleName(), tr);
    }


    public static interface BaseFragmentCreator<T extends BaseYouTubeFragment> {
        public T create();

        public int getFrameId();
    }
}
