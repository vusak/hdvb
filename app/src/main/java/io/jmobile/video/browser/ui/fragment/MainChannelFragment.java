package io.jmobile.video.browser.ui.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.CategoryAdapter;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.base.BaseFragment;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.data.CategoryItem;
import io.jmobile.video.browser.network.Api;
import io.jmobile.video.browser.ui.ChannelListActivity;
import io.jmobile.video.browser.ui.dialog.ReportDialog;

public class MainChannelFragment extends BaseFragment {

    RecyclerView lv;
    LinearLayoutManager manager;
    ArrayList<CategoryItem> list;
    CategoryAdapter adapter;

    ImageButton reportButton;

    Api api;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main_channel;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (reportButton == null)
            return;

        reportButton.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.move_ltr));
        refreshList();
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void handleApiMessage(Message m) {
//        if (m.what == Common.API_CODE_GET_CATEGORY) {
//            boolean update = m.getData().getBoolean(Common.ARG_CATEGORY_UPDATE);
//            if (update) {
//                if (list == null)
//                    list = new ArrayList<>();
//                else
//                    list.clear();
//
//                list.addAll(db.getCategoryItems());
//                if (list != null && list.size() > 0)
//                    for (CategoryItem item : list) {
//                        if (item.getCategoryThumbnail() == null)
//                            new AddCategoryThumbnail().execute(item);
//                    }
//
//                if (adapter != null)
//                    adapter.notifyDataSetChanged();
//            }
//        } else
        if (m.what == Common.API_CODE_SEND_REPORT) {
            boolean result = m.getData().getBoolean(Common.ARG_RESULT);
            if (result) {
                String message = m.getData().getString(Common.ARG_MESSAGE);
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void refreshList() {
        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();
        ArrayList<CategoryItem> temp = new ArrayList<>();
        temp.addAll(db.getCategoryItems());
//        list.addAll(db.getCategoryItems());
        if (temp != null && temp.size() > 0) {
            for (CategoryItem item : temp) {
                if (item.getCategoryType() != CategoryItem.TYPE_LIVE)
                    list.add(item);
            }
            if (adapter != null)
                adapter.notifyDataSetChanged();

            for (CategoryItem item : temp) {
                if (item.getCategoryThumbnail() == null)
                    new AddCategoryThumbnail().execute(item);
            }
        }
    }

    private void initView() {
        lv = fv(R.id.lv);
        list = new ArrayList<>();

        manager = new GridLayoutManager(getActivity(), 3);
        lv.setLayoutManager(manager);
        adapter = new CategoryAdapter(getContext(), R.layout.item_category, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                CategoryItem categoryItem = (CategoryItem) item;
                Intent intent = new Intent(getActivity(), ChannelListActivity.class);
                intent.putExtra(Common.INTENT_CATEGORY_ID, categoryItem.getCategoryId());
                intent.putExtra(Common.INTENT_CATEGORY_TITLE, categoryItem.getCategoryName());
                startActivity(intent);
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);

//        api.getCategoryList();

        reportButton = fv(R.id.button_report);
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fdf(ReportDialog.TAG) == null) {
                    ReportDialog dlg = ReportDialog.newInstance(ReportDialog.TAG, getActivity());
//                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
//                        @Override
//                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
//                            Util.hideKeyBoard(getActivity());
//                        }
//                    });
//
//                    dlg.setNegativeListener(new BaseDialogFragment.DialogNegativeListener() {
//                        @Override
//                        public void onDialogNegative(BaseDialogFragment dialog, String tag) {
//                            Util.hideKeyBoard(getActivity());
//                        }
//                    });
                    sdf(dlg);
                }
            }
        });

        refreshList();
    }

    class AddCategoryThumbnail extends AsyncTask<CategoryItem, Void, CategoryItem> {
        @Override
        protected CategoryItem doInBackground(CategoryItem... categoryItems) {
            CategoryItem item = categoryItems[0];

            Bitmap bmp = ImageUtil.getImageFromURLNonTask(item.getCategoryThumbnailUrl());
            if (bmp != null)
                item.setCategoryThumbnail(ImageUtil.getImageBytes(bmp));

            return item;
        }

        @Override
        protected void onPostExecute(CategoryItem categoryItem) {
            if (categoryItem != null)
                db.insertOrUpdateCategoryItem(categoryItem);

            if (adapter != null)
                adapter.updateThumbnail(categoryItem);
        }
    }

}
