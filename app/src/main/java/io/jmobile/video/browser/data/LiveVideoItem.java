package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class LiveVideoItem extends TableObject {
    public static final String TABLE_NAME = "live_video";

    public static final String ROW_ID = "_id";
    public static final String LIVE_VIDEO_ID = "live_video_id";
    public static final String LIVE_INDEX = "live_index";
    public static final String LIVE_KIND = "live_kind";
    public static final String LIVE_ETAG = "live_etag";
    public static final String LIVE_CHANNEL_ID = "live_channel_id";
    public static final String LIVE_TITLE = "live_title";
    public static final String LIVE_DESCRIPTION = "live_description";
    public static final String LIVE_PUBLISHED_AT = "live_published_at";
    public static final String LIVE_THUMBNAIL_URL = "live_thumbnail_url";
    public static final String LIVE_THUMBNAIL = "live_thumbnail";
    public static final String LIVE_CHANNEL_TITLE = "live_channel_title";
    public static final String LIVE_TYPE = "live_type";

    public static final Creator<LiveVideoItem> CREATOR = new Creator<LiveVideoItem>() {
        @Override
        public LiveVideoItem createFromParcel(Parcel source) {
            return new LiveVideoItem(source);
        }

        @Override
        public LiveVideoItem[] newArray(int size) {
            return new LiveVideoItem[size];
        }
    };

    private long id;
    private int index;
    private String videoId;
    private String kind;
    private String etag;
    private String channelId;
    private String title;
    private String description;
    private String publishedAt;
    private String thumbnailUrl;
    private byte[] thumbnail;
    private String channelTitle;
    private int type;

    public LiveVideoItem() {
        super();
    }

    public LiveVideoItem(JSONObject o, String channelId, String channelTitle) {
        this.videoId = Util.optString(o, Common.TAG_V_ID);
        this.title = Util.optString(o, Common.TAG_NAME);
        this.thumbnailUrl = Util.optString(o, Common.TAG_THUMBNAIL);
        this.description = Util.optString(o, Common.TAG_DESCRIPTION);
        this.publishedAt = Util.optString(o, Common.TAG_REG_DATE);
        this.type = Integer.valueOf(Util.optString(o, Common.TAG_LIVE_BROADCAST));
        this.kind = Util.optString(o, Common.TAG_POSITION);

        this.channelId = channelId;
        this.channelTitle = channelTitle;
//        this.kind = Util.optString(o, Common.TAG_KIND);
//        this.etag = Util.optString(o, Common.TAG_ETAG);
//        try {
//            JSONObject ob = new JSONObject(Util.optString(o, Common.TAG_ID));
//            videoId = Util.optString(ob, Common.TAG_VIDEO_ID);
//            ob = new JSONObject(Util.optString(o, Common.TAG_SNIPPET));
//            publishedAt = Util.optString(ob, Common.TAG_PUBLISHED_AT);
//            channelId = Util.optString(ob, Common.TAG_CHANNEL_ID);
//            title = Util.optString(ob, Common.TAG_TITLE);
//            description = Util.optString(ob, Common.TAG_DESCRIPTION);
//            this.channelTitle = Util.optString(ob, Common.TAG_CHANNEL_TITLE);
//            JSONObject tob = new JSONObject(Util.optString(ob, Common.TAG_THUMBNAILS));
//            JSONObject uob = new JSONObject(Util.optString(tob, Common.TAG_DEFAULT));
//            this.thumbnailDefaultUrl = Util.optString(uob, Common.TAG_URL);
//            uob = new JSONObject(Util.optString(tob, Common.TAG_MEDIUM));
//            this.thumbnailMediumUrl = Util.optString(uob, Common.TAG_URL);
//            uob = new JSONObject(Util.optString(tob, Common.TAG_HIGH));
//            this.thumbnailHighUrl = Util.optString(uob, Common.TAG_URL);
//        } catch (Exception e) {
//            LogUtil.log(e.getMessage());
//        }
    }

    public LiveVideoItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.index = in.readInt();
        this.kind = in.readString();
        this.etag = in.readString();
        this.videoId = in.readString();
        this.channelId = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.publishedAt = in.readString();
        this.channelTitle = in.readString();
        this.thumbnailUrl = in.readString();
        in.readByteArray(this.thumbnail);
        this.type = in.readInt();
    }

    public static void createTableLiveVideoItems(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(LIVE_VIDEO_ID + " text not null unique, ")
                .append(LIVE_INDEX + " integer, ")
                .append(LIVE_KIND + " text, ")
                .append(LIVE_ETAG + " text, ")
                .append(LIVE_CHANNEL_ID + " text, ")
                .append(LIVE_TITLE + " text, ")
                .append(LIVE_DESCRIPTION + " text, ")
                .append(LIVE_PUBLISHED_AT + " text, ")
                .append(LIVE_THUMBNAIL_URL + " text, ")
                .append(LIVE_THUMBNAIL + " BLOB, ")
                .append(LIVE_CHANNEL_TITLE + " text, ")
                .append(LIVE_TYPE + " integer not null) ")
                .toString());
    }

    public static LiveVideoItem getLiveVideoItem(SQLiteDatabase db, String id) {
        List<LiveVideoItem> list = getLiveVideoItems(db, null, LIVE_VIDEO_ID + " = ? ", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<LiveVideoItem> getLiveVideoItemsByChannel(SQLiteDatabase db, String channelId) {
        List<LiveVideoItem> list = getLiveVideoItems(db, null, LIVE_CHANNEL_ID + " = ? ", new String[] { channelId } , null, null, null, null);

        return list;
    }

    public static List<LiveVideoItem> getLiveVideoItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<LiveVideoItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                LiveVideoItem item = new LiveVideoItem()
                        .setRowId(c)
                        .setVideoId(c)
                        .setIndex(c)
                        .setKind(c)
                        .setEtag(c)
                        .setChannelId(c)
                        .setTitle(c)
                        .setDescription(c)
                        .setPublishedAt(c)
                        .setThumbnailUrl(c)
                        .setThumbnail(c)
                        .setChannelTitle(c)
                        .setType(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateLiveVideoItem(SQLiteDatabase db, LiveVideoItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putVideoId(v)
                    .putIndex(v)
                    .putKind(v)
                    .putEtag(v)
                    .putChannelId(v)
                    .putTitle(v)
                    .putDescription(v)
                    .putPublishedAt(v)
                    .putThumbnailUrl(v)
                    .putThumbnail(v)
                    .putChannelTitle(v)
                    .putType(v);

            int rowAffected = db.update(TABLE_NAME, v, LIVE_VIDEO_ID + " =? ", new String[]{item.getVideoId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllLiveVideoItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(index);
        out.writeString(kind);
        out.writeString(etag);
        out.writeString(videoId);
        out.writeString(channelId);
        out.writeString(title);
        out.writeString(description);
        out.writeString(publishedAt);
        out.writeString(channelTitle);
        out.writeString(thumbnailUrl);
        out.writeByteArray(thumbnail);
        out.writeInt(type);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof LiveVideoItem && index == ((LiveVideoItem) obj).index;
    }

    public long getRowId() {
        return this.id;
    }

    public LiveVideoItem setRowId(long id) {
        this.id = id;
        return this;
    }

    public LiveVideoItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public LiveVideoItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public int getIndex() {
        return this.index;
    }

    public LiveVideoItem setIndex(int index) {
        this.index = index;

        return this;
    }

    public LiveVideoItem setIndex(Cursor c) {
        this.index = i(c, LIVE_INDEX);

        return this;
    }

    public LiveVideoItem putIndex(ContentValues v) {
        v.put(LIVE_INDEX, index);

        return this;
    }

    public String getKind() {
        return this.kind;
    }

    public LiveVideoItem setKind(String kind) {
        this.kind = kind;

        return this;
    }

    public LiveVideoItem setKind(Cursor c) {
        this.kind = s(c, LIVE_KIND);

        return this;
    }

    public LiveVideoItem putKind(ContentValues v) {
        v.put(LIVE_KIND, kind);

        return this;
    }

    public String getEtag() {
        return this.etag;
    }

    public LiveVideoItem setEtag(String etag) {
        this.etag = etag;

        return this;
    }

    public LiveVideoItem setEtag(Cursor c) {
        this.etag = s(c, LIVE_ETAG);

        return this;
    }

    public LiveVideoItem putEtag(ContentValues v) {
        v.put(LIVE_ETAG, etag);

        return this;
    }

    public String getVideoId() {
        return this.videoId;
    }

    public LiveVideoItem setVideoId(String videoId) {
        this.videoId = videoId;

        return this;
    }

    public LiveVideoItem setVideoId(Cursor c) {
        this.videoId = s(c, LIVE_VIDEO_ID);

        return this;
    }

    public LiveVideoItem putVideoId(ContentValues v) {
        v.put(LIVE_VIDEO_ID, videoId);

        return this;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public LiveVideoItem setChannelId(String channelId) {
        this.channelId = channelId;

        return this;
    }

    public LiveVideoItem setChannelId(Cursor c) {
        this.channelId = s(c, LIVE_CHANNEL_ID);

        return this;
    }

    public LiveVideoItem putChannelId(ContentValues v) {
        v.put(LIVE_CHANNEL_ID, channelId);

        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public LiveVideoItem setTitle(String title) {
        this.title = title;

        return this;
    }

    public LiveVideoItem setTitle(Cursor c) {
        this.title = s(c, LIVE_TITLE);

        return this;
    }

    public LiveVideoItem putTitle(ContentValues v) {
        v.put(LIVE_TITLE, title);

        return this;
    }

    public String getDescription() {
        return this.description;
    }

    public LiveVideoItem setDescription(String description) {
        this.description = description;

        return this;
    }

    public LiveVideoItem setDescription(Cursor c) {
        this.description = s(c, LIVE_DESCRIPTION);

        return this;
    }

    public LiveVideoItem putDescription(ContentValues v) {
        v.put(LIVE_DESCRIPTION, description);

        return this;
    }

    public String getPublishedAt() {
        return this.publishedAt;
    }

    public LiveVideoItem setPublishedAt(String at) {
        this.publishedAt = at;

        return this;
    }

    public LiveVideoItem setPublishedAt(Cursor c) {
        publishedAt = s(c, LIVE_PUBLISHED_AT);

        return this;
    }

    public LiveVideoItem putPublishedAt(ContentValues v) {
        v.put(LIVE_PUBLISHED_AT, publishedAt);

        return this;
    }

    public String getChannelTitle() {
        return this.channelTitle;
    }

    public LiveVideoItem setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;

        return this;
    }

    public LiveVideoItem setChannelTitle(Cursor c) {
        this.channelTitle = s(c, LIVE_CHANNEL_TITLE);

        return this;
    }

    public LiveVideoItem putChannelTitle(ContentValues v) {
        v.put(LIVE_CHANNEL_TITLE, channelTitle);

        return this;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public LiveVideoItem setThumbnailUrl(String url) {
        this.thumbnailUrl = url;

        return this;
    }

    public LiveVideoItem setThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, LIVE_THUMBNAIL_URL);

        return this;
    }

    public LiveVideoItem putThumbnailUrl(ContentValues v) {
        v.put(LIVE_THUMBNAIL_URL, thumbnailUrl);

        return this;
    }

    public byte[] getThumbnail() {
        return this.thumbnail;
    }

    public LiveVideoItem setThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public LiveVideoItem setThumbnail(Cursor c) {
        this.thumbnail = blob(c, LIVE_THUMBNAIL);

        return this;
    }

    public LiveVideoItem putThumbnail(ContentValues v) {
        v.put(LIVE_THUMBNAIL, thumbnail);

        return this;
    }

    public int getType() {
        return this.type;
    }

    public LiveVideoItem setType(int type) {
        this.type = type;

        return this;
    }

    public LiveVideoItem setType(Cursor c) {
        this.type = i(c, LIVE_TYPE);

        return this;
    }

    public LiveVideoItem putType(ContentValues v) {
        v.put(LIVE_TYPE, type);

        return this;
    }

}
