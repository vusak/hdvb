package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.data.SearchResultItem;

public class SearchResultAdapter extends ReSelectableAdapter<SearchResultItem, SearchResultAdapter.SearchResultHolder> {

    private Context context;
    private ArrayList<SearchResultItem> items = new ArrayList<>();
    private int index = -1;
    private int checkedIndex = -1;

    public SearchResultAdapter(Context context, int layoutId, ArrayList<SearchResultItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(SearchResultHolder holder, int position) {

        SearchResultItem item = items.get(position);

        holder.title.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        holder.channelTitle.setText(item.getChannelTitle());

        holder.thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getThumbnailHighUrl()));

        holder.root.setBackgroundResource(R.color.white);
        if (holder.root != null && index == position) {
            holder.root.setBackgroundResource(R.color.gray3_100);
        }

        if (holder.root != null && checkedIndex == position)
            holder.root.setBackgroundResource(R.color.gray4_100);

        if (holder.selectedLayout != null)
            holder.selectedLayout.setVisibility(index == position ? View.VISIBLE : View.GONE);

    }

    @Override
    public SearchResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SearchResultHolder holder = new SearchResultHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.title = holder.fv(R.id.text_title);
        holder.description = holder.fv(R.id.text_description);
        holder.channelTitle = holder.fv(R.id.text_channel_title);
        holder.root = holder.fv(R.id.layout_root);
        holder.selectedLayout = holder.fv(R.id.layout_select);


        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setSelectedIndex(int index) {
        this.index = index;
        notifyDataSetChanged();
    }

    public void setCheckedIndex(int index) {
        this.checkedIndex = index;
        notifyDataSetChanged();
    }

//    public static interface RecommendAdapterListener extends ReOnItemClickListener<RecommendItem> {
////        public void onDeleteButtonClick(int position, RecommendItem item);
//    }

    public void removeItem(String videoId) {
        if (items == null)
            return;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getVideoId().equalsIgnoreCase(videoId)) {
                items.remove(i);
                notifyItemRemoved(i);

                return;
            }
        }
    }

    public class SearchResultHolder extends ReAbstractViewHolder {
        ImageView thumbnail;
        TextView title, description, channelTitle;
        LinearLayout root, selectedLayout;

        public SearchResultHolder(View itemView) {
            super(itemView);
        }

//        public void setOnBrowserHolderClickListener(OnItemViewClickListener listener) {
//            browserClickListenr = listener;
//            if (listener != null) {
//                deleteButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        browserClickListenr.onItemViewClick(getPosition(), BrowserHolder.this);
//                    }
//                });
//            }
//        }
    }
}
