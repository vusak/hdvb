package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.DBController;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class VideoCategoryItem extends TableObject {
    public static final String TABLE_NAME = "video_category";

    public static final String ROW_ID = "_id";
    public static final String VIDEO_CATEGORY_INDEX = "video_category_index";
    public static final String VIDEO_CATEGORY_KIND = "video_category_kind";
    public static final String VIDEO_CATEGORY_ID = "video_category_id";
    public static final String VIDEO_CATEGORY_ETAG = "video_category_etag";
    public static final String VIDEO_CATEGORY_CHANNEL_ID = "video_category_channel_id";
    public static final String VIDEO_CATEGORY_TITLE = "video_category_title";
    public static final String VIDEO_CATEGORY_ASSIGNABLE = "video_category_assignable";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_ID = "video_category_live_video_id";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_URL_DEFAULT = "video_category_live_video_url_default";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_URL_MEDIUM = "video_category_live_video_url_medium";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_URL_HIGH = "video_category_live_video_url_high";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_THUMBNAIL = "video_category_live_video_thumbnail";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_TITLE = "video_category_live_video_title";
    public static final String VIDEO_CATEGORY_LIVE_VIDEO_DESCRIPTION = "video_category_live_video_description";

    public static final Creator<VideoCategoryItem> CREATOR = new Creator<VideoCategoryItem>() {
        @Override
        public VideoCategoryItem createFromParcel(Parcel source) {
            return new VideoCategoryItem(source);
        }

        @Override
        public VideoCategoryItem[] newArray(int size) {
            return new VideoCategoryItem[size];
        }
    };

    private long rowId;
    private int index;
    private String kind;
    private String id;
    private String etag;
    private String channelId;
    private String title;
    private boolean assignable;
    private String liveVideoId;
    private byte[] thumbnail;
    private String liveTItle;
    private String liveDescription;
    private String liveUrlDefault;
    private String liveUrlMedium;
    private String liveUrlHigh;

    public VideoCategoryItem() {
        super();
    }

    public VideoCategoryItem(DBController db, JSONObject o) {
        // category item
        this.id = Util.optString(o, Common.TAG_VALUE);
        this.etag = Util.optString(o, Common.TAG_TYPE);
        this.kind = Util.optString(o, Common.TAG_TYPE);
        this.channelId = Util.optString(o, Common.TAG_CH_ID);
        this.title = Util.optString(o, Common.TAG_NAME);

        // video item
        LiveVideoItem liveItem = null;
        try {
            JSONArray arr = o.getJSONArray(Common.TAG_CONTENTS);

            if (arr != null && arr.length() > 0) {
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject json_c = arr.getJSONObject(i);

                    LiveVideoItem item = new LiveVideoItem(json_c, channelId, title);
                    LiveVideoItem dbItem = db.getLiveVideoItem(item.getVideoId());

                    if (item != null) {
                        if (dbItem != null
                                && dbItem.getThumbnail() != null
                                && dbItem.getThumbnailUrl().equals(item.getThumbnailUrl()))
                            item.setThumbnail(dbItem.getThumbnail());
                        if (i == 0)
                            liveItem = item;
                        db.insertOrUpdateLiveVideoItem(item);
                    }
                }
            }
        }
        catch (Exception e) {
            LogUtil.log(e.getMessage());
        }

        VideoCategoryItem dbItem = db.getVideoCategoryItem(id);
        if (dbItem != null) {
            this.index = dbItem.getVideoCategoryIndex();
            if (TextUtils.isEmpty(dbItem.getVideoCategoryLiveVideoId())) {
                if (liveItem != null) {
                    this.liveVideoId = liveItem.getVideoId();
                    this.liveTItle = liveItem.getTitle();
                    this.liveDescription = liveItem.getDescription();
                    this.liveUrlDefault = liveItem.getThumbnailUrl();
                    this.liveUrlMedium = liveItem.getThumbnailUrl();
                    this.liveUrlHigh = liveItem.getThumbnailUrl();
                }
            }
            else {
                this.liveVideoId = dbItem.getVideoCategoryLiveVideoId();
                this.liveTItle = dbItem.getVideoCategoryLiveTitle();
                this.liveDescription = dbItem.getVideoCategoryLiveDescription();
                this.liveUrlDefault = dbItem.getVideoCategoryLiveUrlDefault();
                this.liveUrlMedium = dbItem.getVideoCategoryLiveUrlMedium();
                this.liveUrlHigh = dbItem.getVideoCategoryLiveUrlHigh();
                this.thumbnail = dbItem.getVideoCategoryLiveThumbnail();
            }
        }
        else {
            if (liveItem != null) {
                this.liveVideoId = liveItem.getVideoId();
                this.liveTItle = liveItem.getTitle();
                this.liveDescription = liveItem.getDescription();
                this.liveUrlDefault = liveItem.getThumbnailUrl();
                this.liveUrlMedium = liveItem.getThumbnailUrl();
                this.liveUrlHigh = liveItem.getThumbnailUrl();
            }
        }
    }

    public VideoCategoryItem(Parcel in) {
        super(in);

        this.rowId = in.readLong();
        this.index = in.readInt();
        this.kind = in.readString();
        this.id = in.readString();
        this.etag = in.readString();
        this.channelId = in.readString();
        this.title = in.readString();
        this.assignable = in.readInt() == 1;
        this.liveVideoId = in.readString();
        in.readByteArray(this.thumbnail);
        this.liveTItle = in.readString();
        this.liveDescription = in.readString();
        this.liveUrlDefault = in.readString();
        this.liveUrlMedium = in.readString();
        this.liveUrlHigh = in.readString();
    }

    public static void createTableBrowserItems(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(VIDEO_CATEGORY_ID + " text not null unique, ")
                .append(VIDEO_CATEGORY_KIND + " text, ")
                .append(VIDEO_CATEGORY_ETAG + " text, ")
                .append(VIDEO_CATEGORY_CHANNEL_ID + " text, ")
                .append(VIDEO_CATEGORY_INDEX + " integer not null, ")
                .append(VIDEO_CATEGORY_TITLE + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_ID + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_TITLE + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_DESCRIPTION + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_URL_DEFAULT + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_URL_MEDIUM + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_URL_HIGH + " text, ")
                .append(VIDEO_CATEGORY_LIVE_VIDEO_THUMBNAIL + " BLOB, ")
                .append(VIDEO_CATEGORY_ASSIGNABLE + " integer not null) ")
                .toString());
    }

    public static VideoCategoryItem getVideoCategoryItem(SQLiteDatabase db, String id) {
        List<VideoCategoryItem> list = getVideoCategoryItems(db, null, VIDEO_CATEGORY_ID + " = ? ", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<VideoCategoryItem> getVideoCategoryItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<VideoCategoryItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VideoCategoryItem item = new VideoCategoryItem()
                        .setRowId(c)
                        .setVideoCategoryId(c)
                        .setVideoCategoryIndex(c)
                        .setVideoCategoryKind(c)
                        .setVideoCategoryEtag(c)
                        .setVideoCategoryChannelId(c)
                        .setVideoCategoryTitle(c)
                        .setVideoCategoryAssignable(c)
                        .setVideoCategoryLiveVideoId(c)
                        .setVideoCategoryLiveTitle(c)
                        .setVideoCategoryLiveDescription(c)
                        .setVideoCategoryLiveUrlDefault(c)
                        .setVideoCategoryLiveUrlMedium(c)
                        .setVideoCategoryLiveUrlHigh(c)
                        .setVideoCategoryLiveThumbnail(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateBrowserItem(SQLiteDatabase db, VideoCategoryItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putVideoCategoryId(v)
                    .putVideoCategoryIndex(v)
                    .putVideoCategoryKind(v)
                    .putVideoCategoryEtag(v)
                    .putVideoCategoryChannelId(v)
                    .putVideoCategoryTitle(v)
                    .putVideoCategoryAssignable(v)
                    .putVideoCategoryLiveVideoId(v)
                    .putVideoCategoryLiveTitle(v)
                    .putVideoCategoryLiveDescription(v)
                    .putVideoCategoryLiveUrlDefault(v)
                    .putVideoCategoryLiveUrlMedium(v)
                    .putVideoCategoryLiveUrlHigh(v)
                    .putVideoCategoryLiveThumbnail(v);

            int rowAffected = db.update(TABLE_NAME, v, VIDEO_CATEGORY_ID + " =? ", new String[]{item.getVideoCategoryId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getVideoCategoryTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean updateVideoCategoryOrder(SQLiteDatabase db, VideoCategoryItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putVideoCategoryIndex(v);

            db.update(TABLE_NAME, v, VIDEO_CATEGORY_ID + " = ?", new String[]{item.getVideoCategoryId()});

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteVideoCategoryItem(SQLiteDatabase db, VideoCategoryItem item) {
        try {
            db.delete(TABLE_NAME, VIDEO_CATEGORY_ID + " = ? ", new String[]{item.getVideoCategoryId()});
            return true;
        }
        catch (Exception e) {
            LogUtil.log(e.getMessage());
        }

        return false;
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(rowId);
        out.writeInt(index);
        out.writeString(kind);
        out.writeString(id);
        out.writeString(etag);
        out.writeString(channelId);
        out.writeString(title);
        out.writeInt(assignable ? 1 : 0);
        out.writeString(liveVideoId);
        out.writeByteArray(thumbnail);
        out.writeString(liveTItle);
        out.writeString(liveDescription);
        out.writeString(liveUrlDefault);
        out.writeString(liveUrlMedium);
        out.writeString(liveUrlHigh);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof VideoCategoryItem && index == ((VideoCategoryItem) obj).index;
    }

    public long getRowId() {
        return this.rowId;
    }

    public VideoCategoryItem setRowId(Cursor c) {
        this.rowId = l(c, ROW_ID);

        return this;
    }

    public VideoCategoryItem setRowId(long id) {
        this.rowId = id;

        return this;
    }

    public VideoCategoryItem putRowId(ContentValues v) {
        v.put(ROW_ID, rowId);

        return this;
    }

    public int getVideoCategoryIndex() {
        return this.index;
    }

    public VideoCategoryItem setVideoCategoryIndex(Cursor c) {
        this.index = i(c, VIDEO_CATEGORY_INDEX);

        return this;
    }

    public VideoCategoryItem setVideoCategoryIndex(int index) {
        this.index = index;

        return this;
    }

    public VideoCategoryItem putVideoCategoryIndex(ContentValues v) {
        v.put(VIDEO_CATEGORY_INDEX, index);

        return this;
    }

    public String getVideoCategoryKind() {
        return this.kind;
    }

    public VideoCategoryItem setVideoCategoryKind(Cursor c) {
        this.kind = s(c, VIDEO_CATEGORY_KIND);

        return this;
    }

    public VideoCategoryItem setVideoCategoryKind(String kind) {
        this.kind = kind;

        return this;
    }

    public VideoCategoryItem putVideoCategoryKind(ContentValues v) {
        v.put(VIDEO_CATEGORY_KIND, kind);

        return this;
    }

    public String getVideoCategoryId() {
        return this.id;
    }

    public VideoCategoryItem setVideoCategoryId(Cursor c) {
        this.id = s(c, VIDEO_CATEGORY_ID);

        return this;
    }

    public VideoCategoryItem setVideoCategoryId(String id) {
        this.id = id;
        return this;
    }

    public VideoCategoryItem putVideoCategoryId(ContentValues v) {
        v.put(VIDEO_CATEGORY_ID, id);

        return this;
    }

    public String getVideoCategoryEtag() {
        return this.etag;
    }

    public VideoCategoryItem setVideoCategoryEtag(Cursor c) {
        this.etag = s(c, VIDEO_CATEGORY_ETAG);

        return this;
    }

    public VideoCategoryItem setVideoCategoryEtag(String etag) {
        this.etag = etag;

        return this;
    }

    public VideoCategoryItem putVideoCategoryEtag(ContentValues v) {
        v.put(VIDEO_CATEGORY_ETAG, etag);

        return this;
    }

    public String getVideoCategoryChannelId() {
        return this.channelId;
    }

    public VideoCategoryItem setVideoCategoryChannelId(Cursor c) {
        this.channelId = s(c, VIDEO_CATEGORY_CHANNEL_ID);

        return this;
    }

    public VideoCategoryItem setVideoCategoryChannelId(String channelId) {
        this.channelId = channelId;

        return this;
    }

    public VideoCategoryItem putVideoCategoryChannelId(ContentValues v) {
        v.put(VIDEO_CATEGORY_CHANNEL_ID, channelId);

        return this;
    }

    public String getVideoCategoryTitle() {
        return this.title;
    }

    public VideoCategoryItem setVideoCategoryTitle(Cursor c) {
        this.title = s(c, VIDEO_CATEGORY_TITLE);

        return this;
    }

    public VideoCategoryItem setVideoCategoryTitle(String title) {
        this.title = title;

        return this;
    }

    public VideoCategoryItem putVideoCategoryTitle(ContentValues v) {
        v.put(VIDEO_CATEGORY_TITLE, title);

        return this;
    }

    public boolean isVideoCategoryAssignable() {
        return this.assignable;
    }

    public VideoCategoryItem setVideoCategoryAssignable(Cursor c) {
        this.assignable = i(c, VIDEO_CATEGORY_ASSIGNABLE) == 1;

        return this;
    }

    public VideoCategoryItem setVideoCategoryAssignable(boolean assignable) {
        this.assignable = assignable;

        return this;
    }

    public VideoCategoryItem putVideoCategoryAssignable(ContentValues v) {
        v.put(VIDEO_CATEGORY_ASSIGNABLE, assignable ? 1 : 0);

        return this;
    }

    public String getVideoCategoryLiveVideoId() {
        return this.liveVideoId;
    }

    public VideoCategoryItem setVideoCategoryLiveVideoId(Cursor c) {
        this.liveVideoId = s(c, VIDEO_CATEGORY_LIVE_VIDEO_ID);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveVideoId(String liveVideoId) {
        this.liveVideoId = liveVideoId;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveVideoId(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_ID, liveVideoId);

        return this;
    }

    public byte[] getVideoCategoryLiveThumbnail() {
        return this.thumbnail;
    }

    public VideoCategoryItem setVideoCategoryLiveThumbnail(Cursor c) {
        this.thumbnail = blob(c, VIDEO_CATEGORY_LIVE_VIDEO_THUMBNAIL);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveThumbnail(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_THUMBNAIL, thumbnail);

        return this;
    }

    public String getVideoCategoryLiveTitle() {
        return this.liveTItle;
    }

    public VideoCategoryItem setVideoCategoryLiveTitle(Cursor c) {
        this.liveTItle = s(c, VIDEO_CATEGORY_LIVE_VIDEO_TITLE);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveTitle(String liveTitle) {
        this.liveTItle = liveTitle;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveTitle(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_TITLE, liveTItle);

        return this;
    }

    public String getVideoCategoryLiveDescription() {
        return this.liveDescription;
    }

    public VideoCategoryItem setVideoCategoryLiveDescription(Cursor c) {
        this.liveDescription = s(c, VIDEO_CATEGORY_LIVE_VIDEO_DESCRIPTION);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveDescription(String liveDescription) {
        this.liveDescription = liveDescription;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveDescription(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_DESCRIPTION, liveDescription);

        return this;
    }

    public String getVideoCategoryLiveUrlDefault() {
        return this.liveUrlDefault;
    }

    public VideoCategoryItem setVideoCategoryLiveUrlDefault(Cursor c) {
        this.liveUrlDefault = s(c, VIDEO_CATEGORY_LIVE_VIDEO_URL_DEFAULT);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveUrlDefault(String url) {
        this.liveUrlDefault = url;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveUrlDefault(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_URL_DEFAULT, liveUrlDefault);

        return this;
    }

    public String getVideoCategoryLiveUrlMedium() {
        return this.liveUrlMedium;
    }

    public VideoCategoryItem setVideoCategoryLiveUrlMedium(Cursor c) {
        this.liveUrlMedium = s(c, VIDEO_CATEGORY_LIVE_VIDEO_URL_MEDIUM);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveUrlMedium(String url) {
        this.liveUrlMedium = url;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveUrlMedium(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_URL_MEDIUM, liveUrlMedium);

        return this;
    }

    public String getVideoCategoryLiveUrlHigh() {
        return this.liveUrlHigh;
    }

    public VideoCategoryItem setVideoCategoryLiveUrlHigh(Cursor c) {
        this.liveUrlHigh = s(c, VIDEO_CATEGORY_LIVE_VIDEO_URL_HIGH);

        return this;
    }

    public VideoCategoryItem setVideoCategoryLiveUrlHigh(String url) {
        this.liveUrlHigh = url;

        return this;
    }

    public VideoCategoryItem putVideoCategoryLiveUrlHigh(ContentValues v) {
        v.put(VIDEO_CATEGORY_LIVE_VIDEO_URL_HIGH, liveUrlHigh);

        return this;
    }
}
