package io.jmobile.video.browser.ui.dialog;


import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.adapter.SearchResultAdapter;
import io.jmobile.video.browser.base.BaseDialogFragment;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.SearchResultItem;
import io.jmobile.video.browser.data.VideoCategoryItem;
import io.jmobile.video.browser.network.Api;

public class LiveListDialog extends BaseDialogFragment {

    public static final String TAG = LiveListDialog.class.getSimpleName();
    VideoCategoryItem item;
    Context context;

    TextView title;
    RecyclerView listView;
    LinearLayoutManager manager;
    ArrayList<SearchResultItem> list;
    SearchResultAdapter adapter;

    Button positiveButton, negativeButton;
    Api api;

    SearchResultItem selectedItem;

    public static LiveListDialog newInstance(String tag, Context context, VideoCategoryItem item) {
        LiveListDialog d = new LiveListDialog();
        d.item = item;
        d.context = context;

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);
        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_live_list;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        api.getSearchLiveVideos(item.getVideoCategoryId(), Common.RESULT_TYPE_LIST);
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.9f;
            params.width = (int) (dm.widthPixels * 0.85f);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.YOUTUBE_API_CODE_SEARCH_LIVE) {
            ArrayList<SearchResultItem> temp = m.getData().getParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST);
            if (temp != null && temp.size() > 0) {
                list.clear();
                list.addAll(temp);
                adapter.notifyDataSetChanged();
            }
        }
    }

    private void initView() {
        title = fv(R.id.text_live_category);
        title.setText(item.getVideoCategoryTitle());
        listView = fv(R.id.lv);
        manager = new GridLayoutManager(context, 1);
        listView.setLayoutManager(manager);
        list = new ArrayList<>();
        adapter = new SearchResultAdapter(context, R.layout.item_live_search_list, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                selectedItem = list.get(position);
                adapter.setSelectedIndex(position);
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        listView.setAdapter(adapter);

        negativeButton = fv(R.id.button_negative);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        positiveButton = fv(R.id.button_positive);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setVideoCategoryLiveTitle(selectedItem.getTitle());
                item.setVideoCategoryLiveVideoId(selectedItem.getVideoId());
                item.setVideoCategoryLiveDescription(selectedItem.getDescription());
                item.setVideoCategoryLiveUrlDefault(selectedItem.getThumbnailDefaultUrl());
                item.setVideoCategoryLiveUrlMedium(selectedItem.getThumbnailMediumUrl());
                item.setVideoCategoryLiveUrlHigh(selectedItem.getThumbnailHighUrl());

                db.insertOrUpdateVideoCategoryItem(item);
                if (positiveListener != null)
                    positiveListener.onDialogPositive(LiveListDialog.this, tag);
                dismiss();
            }
        });
    }


}
