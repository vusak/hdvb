package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.data.ChannelItem;

public class ChannelAdapter extends ReSelectableAdapter<ChannelItem, ChannelAdapter.ChannelHolder> {

    private Context context;
    private ArrayList<ChannelItem> items = new ArrayList<>();

    public ChannelAdapter(Context context, int layoutId, ArrayList<ChannelItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ChannelHolder holder, int position) {

        ChannelItem item = items.get(position);

        holder.title.setText(item.getChannelTitle());

        if (TextUtils.isEmpty(item.getChannelThumbnailDUrl())) // || item.getChannelTHumbnail() == null)
            holder.thumbnail.setImageResource(R.drawable.ic_youtube_color_48);
        else
//            holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getChannelTHumbnail()));
            holder.thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getChannelThumbnailDUrl()));

    }

    @Override
    public ChannelHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ChannelHolder holder = new ChannelHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.title = holder.fv(R.id.text_title);


        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }


    public class ChannelHolder extends ReAbstractViewHolder {
        ImageView thumbnail;
        TextView title;

        public ChannelHolder(View itemView) {
            super(itemView);
        }

    }

    public void updateThumbnail(ChannelItem _item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getChannelId().equalsIgnoreCase(_item.getChannelId())) {
                notifyItemChanged(i);
                break;
            }
        }
    }
}
