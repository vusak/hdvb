package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class ChannelItem extends TableObject {
    public static final String TABLE_NAME = "channel";

    public static final String ROW_ID = "_id";
    public static final String CHANNEL_ID = "channel_id";
    public static final String CHANNEL_INDEX = "channel_index";
    public static final String CHANNEL_KIND = "channel_kind";
    public static final String CHANNEL_ETAG = "channel_etag";
    public static final String CHANNEL_CATEGORY_ID = "channel_category_id";
    public static final String CHANNEL_TITLE = "channel_title";
    public static final String CHANNEL_DESCRIPTION = "channel_description";
    public static final String CHANNEL_PUBLISHED_AT = "channel_published_at";
    public static final String CHANNEL_THUMBNAIL_D_URL = "channel_thumbnail_d_url";
    public static final String CHANNEL_THUMBNAIL_M_URL = "channel_thumbnail_m_url";
    public static final String CHANNEL_THUMBNAIL_H_URL = "channel_thumbnail_h_url";
    public static final String CHANNEL_THUMBNAIL = "channel_thumbnail";
    public static final String CHANNEL_TYPE = "channel_type";
    public static final String CHANNLE_VALUE = "channel_value";

    public static final int TYPE_CHANNEL_ID = 1;
    public static final int TYPE_SEARCH_KEYWORD = 2;
    public static final int TYPE_URL = 3;

    public static final Creator<ChannelItem> CREATOR = new Creator<ChannelItem>() {
        @Override
        public ChannelItem createFromParcel(Parcel source) {
            return new ChannelItem(source);
        }

        @Override
        public ChannelItem[] newArray(int size) {
            return new ChannelItem[size];
        }
    };

    private long id;
    private int index;
    private String categoryId;
    private String kind;
    private String channelId;
    private String etag;
    private String title;
    private String description;
    private String publishedAt;
    private String thumbnailUrlDefault;
    private String thumbnailUrlMedium;
    private String thumbnailUrlHigh;
    private byte[] thumbnail;
    private int type = TYPE_CHANNEL_ID;
    private String value;

    public ChannelItem() {
        super();
    }

    public ChannelItem(JSONObject o, String categoryId) {
        this.channelId = Util.optString(o, Common.TAG_IDX);
        this.categoryId = categoryId;
        this.title = Util.optString(o, Common.TAG_NAME);
        this.thumbnailUrlDefault = Util.optString(o, Common.TAG_THUMBNAIL);
        this.type = Integer.valueOf(Util.optString(o, Common.TAG_TYPE));
        this.index = Integer.valueOf(Util.optString(o, Common.TAG_POSITION));
        this.value = Util.optString(o, Common.TAG_VALUE);
    }

    public ChannelItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.index = in.readInt();
        this.categoryId = in.readString();
        this.kind = in.readString();
        this.channelId = in.readString();
        this.etag = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.publishedAt = in.readString();
        this.thumbnailUrlDefault = in.readString();
        this.thumbnailUrlMedium = in.readString();
        this.thumbnailUrlHigh = in.readString();
        this.type = in.readInt();
        this.value = in.readString();
        in.readByteArray(this.thumbnail);
    }

    public static void createTableChannel(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(CHANNEL_ID + " text not null unique, ")
                .append(CHANNEL_INDEX + " integer, ")
                .append(CHANNEL_CATEGORY_ID + " text, ")
                .append(CHANNEL_TYPE + " integer, ")
                .append(CHANNLE_VALUE + " text, ")
                .append(CHANNEL_KIND + " text, ")
                .append(CHANNEL_ETAG + " text, ")
                .append(CHANNEL_TITLE + " text, ")
                .append(CHANNEL_DESCRIPTION + " text, ")
                .append(CHANNEL_PUBLISHED_AT + " text, ")
                .append(CHANNEL_THUMBNAIL_D_URL + " text, ")
                .append(CHANNEL_THUMBNAIL_M_URL + " text, ")
                .append(CHANNEL_THUMBNAIL_H_URL + " text, ")
                .append(CHANNEL_THUMBNAIL + " BLOB) ")
                .toString());
    }

    public static ChannelItem getChannelItem(SQLiteDatabase db, String id) {
        List<ChannelItem> list = getChannelItems(db, null, CHANNEL_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<ChannelItem> getChannelItemsByCategory(SQLiteDatabase db, String categoryId) {
        List<ChannelItem> list = getChannelItems(db, null, CHANNEL_CATEGORY_ID + " = ?", new String[]{categoryId}, null, null, null, null);

        return list;
    }

    public static List<ChannelItem> getChannelItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<ChannelItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                ChannelItem item = new ChannelItem()
                        .setRowId(c)
                        .setChannelId(c)
                        .setChannelIndex(c)
                        .setChannelCategoryId(c)
                        .setChannelEtag(c)
                        .setChannelKind(c)
                        .setChannelTitle(c)
                        .setChannelType(c)
                        .setChannelValue(c)
                        .setChannelDescription(c)
                        .setChannelPublishedAt(c)
                        .setChannelThumbnailDUrl(c)
                        .setChannelThumbnailMUrl(c)
                        .setChannelThumbnailHUrl(c)
                        .setChannelThumbnail(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateChannelItem(SQLiteDatabase db, ChannelItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putChannelId(v)
                    .putChannelIndex(v)
                    .putChannelCategoryId(v)
                    .putChannelKind(v)
                    .putChannelEtag(v)
                    .putChannelTitle(v)
                    .putChannelType(v)
                    .putChannelValue(v)
                    .putChannelDescription(v)
                    .putChannelThumbnailDUrl(v)
                    .putChannelThumbnailMUrl(v)
                    .putChannelThumbnailHUrl(v)
                    .putChannelPublishedAt(v)
                    .putChannelThumbnail(v);

            int rowAffected = db.update(TABLE_NAME, v, CHANNEL_ID + " =? ", new String[]{item.getChannelId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getChannelTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllChannelItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(index);
        out.writeString(categoryId);
        out.writeString(kind);
        out.writeString(channelId);
        out.writeString(etag);
        out.writeString(title);
        out.writeString(description);
        out.writeString(publishedAt);
        out.writeString(thumbnailUrlDefault);
        out.writeString(thumbnailUrlMedium);
        out.writeString(thumbnailUrlHigh);
        out.writeByteArray(thumbnail);
        out.writeInt(type);
        out.writeString(value);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ChannelItem && channelId == ((ChannelItem) obj).channelId;
    }

    public long getRowId() {
        return this.id;
    }

    public ChannelItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public ChannelItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public ChannelItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public ChannelItem setChannelId(Cursor c) {
        this.channelId = s(c, CHANNEL_ID);

        return this;
    }

    public ChannelItem setChannelId(String channelId) {
        this.channelId = channelId;

        return this;
    }

    public ChannelItem putChannelId(ContentValues v) {
        v.put(CHANNEL_ID, this.channelId);

        return this;
    }

    public int getChannelIndex() {
        return this.index;
    }

    public ChannelItem setChannelIndex(Cursor c) {
        this.index = i(c, CHANNEL_INDEX);

        return this;
    }

    public ChannelItem setChannelIndex(int index) {
        this.index = index;

        return this;
    }

    public ChannelItem putChannelIndex(ContentValues v) {
        v.put(CHANNEL_INDEX, this.index);

        return this;
    }

    public String getChannelCategoryId() {
        return this.categoryId;
    }

    public ChannelItem setChannelCategoryId(Cursor c) {
        this.categoryId = s(c, CHANNEL_CATEGORY_ID);

        return this;
    }

    public ChannelItem setChannelCategoryId(String categoryId) {
        this.categoryId = categoryId;

        return this;
    }

    public ChannelItem putChannelCategoryId(ContentValues v) {
        v.put(CHANNEL_CATEGORY_ID, this.categoryId);

        return this;
    }

    public String getCahnnelKind() {
        return this.kind;
    }

    public ChannelItem setChannelKind(String kind) {
        this.kind = kind;

        return this;
    }

    public ChannelItem setChannelKind(Cursor c) {
        this.kind = s(c, CHANNEL_KIND);

        return this;
    }

    public ChannelItem putChannelKind(ContentValues v) {
        v.put(CHANNEL_KIND, this.kind);

        return this;
    }

    public String getChannelEtag() {
        return this.etag;
    }

    public ChannelItem setChannelEtag(Cursor c) {
        this.etag = s(c, CHANNEL_ETAG);

        return this;
    }

    public ChannelItem setChannelEtag(String etag) {
        this.etag = etag;

        return this;
    }

    public ChannelItem putChannelEtag(ContentValues v) {
        v.put(CHANNEL_ETAG, this.etag);

        return this;
    }

    public String getChannelTitle() {
        return this.title;
    }

    public ChannelItem setChannelTitle(Cursor c) {
        this.title = s(c, CHANNEL_TITLE);

        return this;
    }

    public ChannelItem setChannelTitle(String title) {
        this.title = title;

        return this;
    }

    public ChannelItem putChannelTitle(ContentValues v) {
        v.put(CHANNEL_TITLE, this.title);

        return this;
    }

    public String getChannelDescription() {
        return this.description;
    }

    public ChannelItem setChannelDescription(Cursor c) {
        this.description = s(c, CHANNEL_DESCRIPTION);

        return this;
    }

    public ChannelItem setChannelDescription(String description) {
        this.description = description;

        return this;
    }

    public ChannelItem putChannelDescription(ContentValues v) {
        v.put(CHANNEL_DESCRIPTION, this.description);

        return this;
    }

    public String getChannelPublishedAt() {
        return this.publishedAt;
    }

    public ChannelItem setChannelPublishedAt(Cursor c) {
        this.publishedAt = s(c, CHANNEL_PUBLISHED_AT);

        return this;
    }

    public ChannelItem setChannelPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;

        return this;
    }

    public ChannelItem putChannelPublishedAt(ContentValues v) {
        v.put(CHANNEL_PUBLISHED_AT, this.publishedAt);

        return this;
    }

    public String getChannelThumbnailDUrl() {
        return this.thumbnailUrlDefault;
    }

    public ChannelItem setChannelThumbnailDUrl(Cursor c) {
        this.thumbnailUrlDefault = s(c, CHANNEL_THUMBNAIL_D_URL);

        return this;
    }

    public ChannelItem setChannelThumbnailDUrl(String defaultUrl) {
        this.thumbnailUrlDefault = defaultUrl;

        return this;
    }

    public ChannelItem putChannelThumbnailDUrl(ContentValues v) {
        v.put(CHANNEL_THUMBNAIL_D_URL, this.thumbnailUrlDefault);

        return this;
    }

    public String getChannelThumbnailMUrl() {
        return this.thumbnailUrlMedium;
    }

    public ChannelItem setChannelThumbnailMUrl(Cursor c) {
        this.thumbnailUrlMedium = s(c, CHANNEL_THUMBNAIL_M_URL);

        return this;
    }

    public ChannelItem setChannelThumbnailMUrl(String mediumUrl) {
        this.thumbnailUrlMedium = mediumUrl;

        return this;
    }

    public ChannelItem putChannelThumbnailMUrl(ContentValues v) {
        v.put(CHANNEL_THUMBNAIL_M_URL, this.thumbnailUrlMedium);

        return this;
    }

    public String getChannelThumbnailHUrl() {
        return this.thumbnailUrlHigh;
    }

    public ChannelItem setChannelThumbnailHUrl(Cursor c) {
        this.thumbnailUrlHigh = s(c, CHANNEL_THUMBNAIL_H_URL);

        return this;
    }

    public ChannelItem setChannelThumbnailHUrl(String highUrl) {
        this.thumbnailUrlHigh = highUrl;

        return this;
    }

    public ChannelItem putChannelThumbnailHUrl(ContentValues v) {
        v.put(CHANNEL_THUMBNAIL_H_URL, this.thumbnailUrlHigh);

        return this;
    }

    public byte[] getChannelTHumbnail() {
        return this.thumbnail;
    }

    public ChannelItem setChannelThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public ChannelItem setChannelThumbnail(Cursor c) {
        this.thumbnail = blob(c, CHANNEL_THUMBNAIL);

        return this;
    }

    public ChannelItem putChannelThumbnail(ContentValues v) {
        v.put(CHANNEL_THUMBNAIL, this.thumbnail);

        return this;
    }

    public int getChannelType() {
        return this.type;
    }

    public ChannelItem setChannelType(Cursor c) {
        this.type = i(c, CHANNEL_TYPE);

        return this;
    }

    public ChannelItem setChannelType(int type) {
        this.type = type;

        return this;
    }

    public ChannelItem putChannelType(ContentValues v) {
        v.put(CHANNEL_TYPE, type);

        return this;
    }

    public String getChannelValue() {
        return this.value;
    }

    public ChannelItem setChannelValue(Cursor c) {
        this.value = s(c, CHANNLE_VALUE);

        return this;
    }

    public ChannelItem setChannelValue(String value) {
        this.value = value;

        return this;
    }

    public ChannelItem putChannelValue(ContentValues v) {
        v.put(CHANNLE_VALUE, value);

        return this;
    }
}
