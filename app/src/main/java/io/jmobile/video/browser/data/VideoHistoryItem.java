package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.common.LogUtil;

public class VideoHistoryItem extends TableObject {
    public static final String TABLE_NAME = "video_history";

    public static final String ROW_ID = "_id";
    public static final String VIDEO_HISTORY_ID = "video_history_id";
    public static final String VIDEO_HISTORY_URL = "video_history_url";
    public static final String VIDEO_HISTORY_SITE = "video_history_site";
    public static final String VIDEO_HISTORY_TITLE = "video_history_title";
    public static final String VIDEO_HISTORY_AT = "video_history_at";
    public static final String VIDEO_HISTORY_FILE_PATH = "video_history_file_path";
    public static final String VIDEO_HISTORY_FILE_TIME = "video_history_file_time";
    public static final String VIDEO_HISTORY_FILE_SIZE = "video_history_file_size";
    public static final String VIDEO_HISTORY_THUMBNAIL_PATH = "video_history_thumbnail_path";
    public static final String VIDEO_HISTORY_THUMBNAIL = "video_history_thumbnail";

    public static final Creator<VideoHistoryItem> CREATOR = new Creator<VideoHistoryItem>() {
        @Override
        public VideoHistoryItem createFromParcel(Parcel source) {
            return new VideoHistoryItem(source);
        }

        @Override
        public VideoHistoryItem[] newArray(int size) {
            return new VideoHistoryItem[size];
        }
    };
    private long id;
    private String historyId;
    private String url;
    private String site;
    private String title;
    private String filePath;
    private long fileSize;
    private long fileTime;
    private String thumbnailPath;
    private byte[] thumbnail;
    private long historyAt;
    private boolean isChecked = false;

    public VideoHistoryItem() {
        super();
    }

    public VideoHistoryItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.historyId = in.readString();
        this.url = in.readString();
        this.site = in.readString();
        this.title = in.readString();
        this.filePath = in.readString();
        this.fileSize = in.readLong();
        this.fileTime = in.readLong();
        this.thumbnailPath = in.readString();
        in.readByteArray(this.thumbnail);
        this.historyAt = in.readLong();
    }

    public static void createTableVideoHistory(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(VIDEO_HISTORY_ID + " text not null unique, ")
                .append(VIDEO_HISTORY_URL + " text not null, ")
                .append(VIDEO_HISTORY_SITE + " text, ")
                .append(VIDEO_HISTORY_TITLE + " text, ")
                .append(VIDEO_HISTORY_FILE_PATH + " text, ")
                .append(VIDEO_HISTORY_FILE_TIME + " integer, ")
                .append(VIDEO_HISTORY_FILE_SIZE + " integer, ")
                .append(VIDEO_HISTORY_THUMBNAIL_PATH + " text, ")
                .append(VIDEO_HISTORY_THUMBNAIL + " BLOB, ")
                .append(VIDEO_HISTORY_AT + " integer not null) ")
                .toString());
    }

    public static VideoHistoryItem getVideoHistoryItem(SQLiteDatabase db, String id) {
        List<VideoHistoryItem> list = getVideoHistoryItems(db, null, VIDEO_HISTORY_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<VideoHistoryItem> getVideoHistoryItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<VideoHistoryItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                VideoHistoryItem item = new VideoHistoryItem()
                        .setRowId(c)
                        .setVideoHistoryId(c)
                        .setVideoHistoryUrl(c)
                        .setVideoHistorySite(c)
                        .setVideoHistoryTitle(c)
                        .setVideoHistoryFilePath(c)
                        .setVideoHistoryFileSize(c)
                        .setVideoHistoryFileTime(c)
                        .setVideoHistoryThumbnailPath(c)
                        .setVideoHistoryThumbnail(c)
                        .setVideoHistoryAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateVideoHistoryItem(SQLiteDatabase db, VideoHistoryItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putVideoHistoryId(v)
                    .putVideoHistoryUrl(v)
                    .putVideoHistorySite(v)
                    .putVideoHistoryTitle(v)
                    .putVideoHistoryFilePath(v)
                    .putVideoHistoryFileSize(v)
                    .putVideoHistoryFileTime(v)
                    .putVideoHistoryThumbnailPath(v)
                    .putVideoHistoryThumbnail(v)
                    .putVideoHistoryAt(v);

            int rowAffected = db.update(TABLE_NAME, v, VIDEO_HISTORY_ID + " =? ", new String[]{item.getVideoHistoryId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getVideoHistoryTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

//    public static boolean deleteHistory(SQLiteDatabase db, int past) {
//        long current = System.currentTimeMillis();
//        long compare = 0;
//
//        switch (past) {
//            case ClearHistoryDialog.CLEAR_PAST_HOUR:
//                compare = 1000 * 60 * 60;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_DAY:
//                compare = 1000 * 60 * 60 * 24;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_WEEK:
//                compare = 1000 * 60 * 60 * 24 * 7;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_MONTH:
//                compare = 1000 * 60 * 60 * 24 * 7 * 4;
//                break;
//
//            case ClearHistoryDialog.CLEAR_PAST_BEGINNING:
//                return deleteAllHistoryItems(db);
//        }
//
//        compare = current - compare;
//        try {
//            db.delete(TABLE_NAME, HISTORY_AT + " > " + compare, null);
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }

    public static boolean deleteVideoHistoryItem(SQLiteDatabase db, VideoHistoryItem item) {
        try {
            db.delete(TABLE_NAME, VIDEO_HISTORY_ID + " =? ", new String[]{item.getVideoHistoryId()});
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean deleteAllVideoHistoryItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeString(historyId);
        out.writeString(url);
        out.writeString(site);
        out.writeString(title);
        out.writeString(filePath);
        out.writeLong(fileSize);
        out.writeLong(fileTime);
        out.writeString(thumbnailPath);
        out.writeByteArray(thumbnail);
        out.writeLong(historyAt);
    }

    public long getRowId() {
        return this.id;
    }

    public VideoHistoryItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public VideoHistoryItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public VideoHistoryItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getVideoHistoryId() {
        return this.historyId;
    }

    public VideoHistoryItem setVideoHistoryId(String historyId) {
        this.historyId = historyId;

        return this;
    }

    public VideoHistoryItem setVideoHistoryId(Cursor c) {
        this.historyId = s(c, VIDEO_HISTORY_ID);

        return this;
    }

    public VideoHistoryItem putVideoHistoryId(ContentValues v) {
        v.put(VIDEO_HISTORY_ID, historyId);

        return this;
    }

    public String getVideoHistoryUrl() {
        return this.url;
    }

    public VideoHistoryItem setVideoHistoryUrl(String url) {
        this.url = url;

        return this;
    }

    public VideoHistoryItem setVideoHistoryUrl(Cursor c) {
        this.url = s(c, VIDEO_HISTORY_URL);

        return this;
    }

    public VideoHistoryItem putVideoHistoryUrl(ContentValues v) {
        v.put(VIDEO_HISTORY_URL, this.url);

        return this;
    }

    public String getVideoHistorySite() {
        return this.site;
    }

    public VideoHistoryItem setVideoHistorySite(String site) {
        this.site = site;

        return this;
    }

    public VideoHistoryItem setVideoHistorySite(Cursor c) {
        this.site = s(c, VIDEO_HISTORY_SITE);

        return this;
    }

    public VideoHistoryItem putVideoHistorySite(ContentValues v) {
        v.put(VIDEO_HISTORY_SITE, this.site);

        return this;
    }


    public String getVideoHistoryTitle() {
        return this.title;
    }

    public VideoHistoryItem setVideoHistoryTitle(String title) {
        this.title = title;

        return this;
    }

    public VideoHistoryItem setVideoHistoryTitle(Cursor c) {
        this.title = s(c, VIDEO_HISTORY_TITLE);

        return this;
    }

    public VideoHistoryItem putVideoHistoryTitle(ContentValues v) {
        v.put(VIDEO_HISTORY_TITLE, this.title);

        return this;
    }

    public String getVideoHistoryFilePath() {
        return this.filePath;
    }

    public VideoHistoryItem setVideoHistoryFilePath(String filePath) {
        this.filePath = filePath;

        return this;
    }

    public VideoHistoryItem setVideoHistoryFilePath(Cursor c) {
        this.filePath = s(c, VIDEO_HISTORY_FILE_PATH);

        return this;
    }

    public VideoHistoryItem putVideoHistoryFilePath(ContentValues v) {
        v.put(VIDEO_HISTORY_FILE_PATH, this.filePath);

        return this;
    }

    public long getVideoHistoryFileSize() {
        return this.fileSize;
    }

    public VideoHistoryItem setVideoHistoryFileSize(long size) {
        this.fileSize = size;

        return this;
    }

    public VideoHistoryItem setVideoHistoryFileSize(Cursor c) {
        this.fileSize = l(c, VIDEO_HISTORY_FILE_SIZE);

        return this;
    }

    public VideoHistoryItem putVideoHistoryFileSize(ContentValues v) {
        v.put(VIDEO_HISTORY_FILE_SIZE, this.fileSize);

        return this;
    }

    public long getVideoHistoryFileTime() {
        return this.fileTime;
    }

    public VideoHistoryItem setVideoHistoryFileTime(long time) {
        this.fileTime = time;

        return this;
    }

    public VideoHistoryItem setVideoHistoryFileTime(Cursor c) {
        this.fileTime = l(c, VIDEO_HISTORY_FILE_TIME);

        return this;
    }

    public VideoHistoryItem putVideoHistoryFileTime(ContentValues v) {
        v.put(VIDEO_HISTORY_FILE_TIME, this.fileTime);

        return this;
    }

    public String getVideoHistoryThumbnailPath() {
        return this.thumbnailPath;
    }

    public VideoHistoryItem setVideoHistoryThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;

        return this;
    }

    public VideoHistoryItem setVideoHistoryThumbnailPath(Cursor c) {
        this.thumbnailPath = s(c, VIDEO_HISTORY_THUMBNAIL_PATH);

        return this;
    }

    public VideoHistoryItem putVideoHistoryThumbnailPath(ContentValues v) {
        v.put(VIDEO_HISTORY_THUMBNAIL_PATH, this.thumbnailPath);

        return this;
    }

    public byte[] getVideoHistoryThumnail() {
        return this.thumbnail;
    }

    public VideoHistoryItem setVideoHistoryThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public VideoHistoryItem setVideoHistoryThumbnail(Cursor c) {
        this.thumbnail = blob(c, VIDEO_HISTORY_THUMBNAIL);

        return this;
    }

    public VideoHistoryItem putVideoHistoryThumbnail(ContentValues v) {
        v.put(VIDEO_HISTORY_THUMBNAIL, thumbnail);

        return this;
    }

    public long getVideoHistoryAt() {
        return this.historyAt;
    }

    public VideoHistoryItem setVideoHistoryAt(long historyAt) {
        this.historyAt = historyAt;

        return this;
    }

    public VideoHistoryItem setVideoHistoryAt(Cursor c) {
        this.historyAt = l(c, VIDEO_HISTORY_AT);

        return this;
    }

    public VideoHistoryItem putVideoHistoryAt(ContentValues v) {
        v.put(VIDEO_HISTORY_AT, this.historyAt);

        return this;
    }

    public boolean isChecked() {
        return this.isChecked;
    }

    public VideoHistoryItem setChecked(boolean isChecked) {
        this.isChecked = isChecked;

        return this;
    }

    public void setChecked() {
        this.isChecked = !this.isChecked;
    }
}
