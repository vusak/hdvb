package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.data.RecommendItem;

public class RecommendAdapter extends ReSelectableAdapter<RecommendItem, RecommendAdapter.RecommendHolder> {

    private Context context;
    private ArrayList<RecommendItem> items = new ArrayList<>();

    public RecommendAdapter(Context context, int layoutId, ArrayList<RecommendItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(RecommendHolder holder, int position) {

        RecommendItem item = items.get(position);

        holder.name.setText(item.getName());
        holder.icon.setImageResource(item.getIconId());
//        holder.layout.setBackgroundColor(ContextCompat.getColor(context, isCheckMode() && item.isSelected() ? R.color.bookmarks_list_background_sel : R.color.bookmarks_list_background));

//            holder.title.setText(item.getBrowserTitle());
//            if (item.getBrowserTitle().equals("HOME")) {
//                holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_home_n));
//                holder.thumbnail.setImageBitmap(homeBitmap);
//            } else if (item.getBrowserThumbnail() != null) {
//                holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getBrowserThumbnail()));
//                if (item.getBrowserIcon() != null)
//                    holder.icon.setImageBitmap(ImageUtil.getImage(item.getBrowserIcon()));
//                else
//                    holder.icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_globe_n));
//            }
//
//            if (holder.layout != null) {
//                holder.layout.setBackgroundColor(ContextCompat.getColor(context, position == selectedIndex ? R.color.colorAccent : R.color.transparent));
//            }


    }

    @Override
    public RecommendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecommendHolder holder = new RecommendHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.icon = holder.fv(R.id.image_icon);
        holder.name = holder.fv(R.id.text_name);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }


//    public static interface RecommendAdapterListener extends ReOnItemClickListener<RecommendItem> {
////        public void onDeleteButtonClick(int position, RecommendItem item);
//    }

    public class RecommendHolder extends ReAbstractViewHolder {
        ImageView icon;
        TextView name;

        public RecommendHolder(View itemView) {
            super(itemView);
        }

//        public void setOnBrowserHolderClickListener(OnItemViewClickListener listener) {
//            browserClickListenr = listener;
//            if (listener != null) {
//                deleteButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        browserClickListenr.onItemViewClick(getPosition(), BrowserHolder.this);
//                    }
//                });
//            }
//        }
    }
}
