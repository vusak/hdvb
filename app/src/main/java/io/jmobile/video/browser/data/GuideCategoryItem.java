package io.jmobile.video.browser.data;

import android.os.Parcel;

import org.json.JSONObject;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class GuideCategoryItem extends TableObject {
    public static final Creator<GuideCategoryItem> CREATOR = new Creator<GuideCategoryItem>() {
        @Override
        public GuideCategoryItem createFromParcel(Parcel source) {
            return new GuideCategoryItem(source);
        }

        @Override
        public GuideCategoryItem[] newArray(int size) {
            return new GuideCategoryItem[size];
        }
    };
    private int index;
    private String kind;
    private String id;
    private String etag;
    private String channelId;
    private String title;
    private String thumbnailUrl;
    private byte[] thumbnail;

    public GuideCategoryItem() {
        super();
    }

    public GuideCategoryItem(JSONObject o) {
        this.id = Util.optString(o, Common.TAG_ID);
        this.etag = Util.optString(o, Common.TAG_ETAG);
        this.kind = Util.optString(o, Common.TAG_KIND);
        try {
            JSONObject ob = new JSONObject(Util.optString(o, Common.TAG_SNIPPET));
            this.channelId = Util.optString(ob, Common.TAG_CHANNEL_ID);
            this.title = Util.optString(ob, Common.TAG_TITLE);
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    public GuideCategoryItem(Parcel in) {
        super(in);

        this.index = in.readInt();
        this.kind = in.readString();
        this.id = in.readString();
        this.etag = in.readString();
        this.channelId = in.readString();
        this.title = in.readString();
        this.thumbnailUrl = in.readString();
        in.readByteArray(this.thumbnail);
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(index);
        out.writeString(kind);
        out.writeString(id);
        out.writeString(etag);
        out.writeString(channelId);
        out.writeString(title);
        out.writeString(thumbnailUrl);
        out.writeByteArray(thumbnail);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof GuideCategoryItem && index == ((GuideCategoryItem) obj).index;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtag() {
        return this.etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public void setThumbnailUrl(String url) {
        this.thumbnailUrl = url;
    }

    public byte[] getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;
    }
}
