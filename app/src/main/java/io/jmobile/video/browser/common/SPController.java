package io.jmobile.video.browser.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

import io.jmobile.video.browser.ui.fragment.PlaylistFragment;

public class SPController {

    private static final String LIVE_SELECTED_INDEX = "live_selected_index";
    private static final int DEFAULT_LIVE_SELECTED_INDEX = 0;

    private static final String BROWSER_URL = "browser_url";
    private static final String DEFAULT_BROWSER_URL = "";

    private static final String SET_SEARCH_ENGINE = "set_search_engine";
    private static final int DEFAULT_SEARCH_ENGINE = 0;

    private static final String CURRENT_BROWSER_INDEX = "current_browser_index";
    private static final int DEFAULT_CURRENT_BROWSER_INDEX = -1;

    private static final String BROWSER_TAB_COUNT = "browser_tab_count";
    private static final int DEFAULT_BROWSER_TAB_COUNT = 1;

    private static final String SETTING_TIMER_HOUR = "setting_timer_hour";
    private static final int DEFAULT_SETTING_TIMER_HOUR = -1;

    private static final String SETTING_TIMER_MINUTE = "setting_timer_minute";
    private static final int DEFAULT_SETTING_TIMER_MINUTE = -1;

    private static final String PLAYER_SHUFFLE_MODE = "player_shuffle_mode";
    private static final int DEFAULT_PLAYER_SHUFFLE_MODE = PlaylistFragment.SHUFFLE_OFF;

    private static final String PLAYER_REPEAT_MODE = "player_repeat_mode";
    private static final int DEFAULT_PLAYER_REPEAT_MODE = PlaylistFragment.REPEAT_NONE;

    private static final String FIRST_START = "first_start";
    private static final boolean DEFAULT_FIRST_START = true;

    private static final String CHECK_CATEGORY_UPDATE_VERSION = "check_category_update_version";
    private static final String DEFAULT_CHECK_CATEGORY_UPDATE_VERSION = "-1";

    protected SharedPreferences sp;

    public SPController(Context context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public int getLiveSelectedIndex() {
        return sp.getInt(LIVE_SELECTED_INDEX, DEFAULT_LIVE_SELECTED_INDEX);
    }

    public void setLiveSelectedIndex(int val) {
        put(LIVE_SELECTED_INDEX, val);
    }

    public String getBrowserUrl() {
        return sp.getString(BROWSER_URL, DEFAULT_BROWSER_URL);
    }

    public void setBrowserUrl(String val) {
        put(BROWSER_URL, val);
    }

    public int getSearchEngine() {
        return sp.getInt(SET_SEARCH_ENGINE, DEFAULT_SEARCH_ENGINE);
    }

    public void setSearchEngine(int val) {
        put(SET_SEARCH_ENGINE, val);
    }

    public int getCurrentBrowserIndex() {
        return sp.getInt(CURRENT_BROWSER_INDEX, DEFAULT_CURRENT_BROWSER_INDEX);
    }

    public void setCurrentBrowserIndex(int val) {
        put(CURRENT_BROWSER_INDEX, val);
    }

    public int getBrowserTabCount() {
        return sp.getInt(BROWSER_TAB_COUNT, DEFAULT_BROWSER_TAB_COUNT);
    }

    public void setBrowserTabCount(int val) {
        put(BROWSER_TAB_COUNT, val);
    }

    public int getSettingTimerHour() {
        return sp.getInt(SETTING_TIMER_HOUR, DEFAULT_SETTING_TIMER_HOUR);
    }

    public void setSettingTimerHour(int val) {
        put(SETTING_TIMER_HOUR, val);
    }

    public int getSettingTimerMinute() {
        return sp.getInt(SETTING_TIMER_MINUTE, DEFAULT_SETTING_TIMER_MINUTE);
    }

    public void setSettingTimerMinute(int val) {
        put(SETTING_TIMER_MINUTE, val);
    }

    public void setSettingTimerRelease() {
        setSettingTimerHour(-1);
        setSettingTimerMinute(-1);
    }

    public int getPlayerShuffleMode() {
        return sp.getInt(PLAYER_SHUFFLE_MODE, DEFAULT_PLAYER_SHUFFLE_MODE);
    }

    public void setPlayerShuffleMode(int val) {
        put(PLAYER_SHUFFLE_MODE, val);
    }

    public int getPlayerRepeatMode() {
        return sp.getInt(PLAYER_REPEAT_MODE, DEFAULT_PLAYER_REPEAT_MODE);
    }

    public void setPlayerRepeatMode(int val) {
        put(PLAYER_REPEAT_MODE, val);
    }

    public boolean isFirstStart() {
        return sp.getBoolean(FIRST_START, DEFAULT_FIRST_START);
    }

    public void setFirstStart(boolean val) {
        put(FIRST_START, val);
    }

    public String getCheckCategoryUpdateVersion() {
        return sp.getString(CHECK_CATEGORY_UPDATE_VERSION, DEFAULT_CHECK_CATEGORY_UPDATE_VERSION);
    }

    public void setCheckCategoryUpdateVersion(String val) {
        put(CHECK_CATEGORY_UPDATE_VERSION, val);
    }

    public void clearSP() {
        sp.edit().clear().commit();
    }

    protected void put(String key, boolean value) {
        sp.edit().putBoolean(key, value).commit();
    }

    protected void put(String key, int value) {
        sp.edit().putInt(key, value).commit();
    }

    protected void put(String key, float value) {
        sp.edit().putFloat(key, value).commit();
    }

    protected void put(String key, long value) {
        sp.edit().putLong(key, value).commit();
    }

    protected void put(String key, String value) {
        sp.edit().putString(key, value).commit();
    }

    protected void put(String key, Set<String> value) {
        sp.edit().putStringSet(key, value).commit();
    }
}
