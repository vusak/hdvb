package io.jmobile.video.browser.ui.dialog;


import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.Calendar;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseDialogFragment;
import io.jmobile.video.browser.common.Util;

public class TimerDialog extends BaseDialogFragment {

    public static final String TAG = TimerDialog.class.getSimpleName();
    Context context;
    TimePicker timePicker;

    Button negativeButton, positiveButton;

    public static TimerDialog newInstance(String tag, Context context) {
        TimerDialog d = new TimerDialog();
        d.context = context;

        d.createArguments(tag);
        return d;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false, false);

    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_timer;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    @SuppressWarnings("static-access")
    @Override
    public void onResume() {
        super.onResume();

        if (!Util.isTablet(getActivity())) {
            Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
            DisplayMetrics dm = new DisplayMetrics();
            display.getMetrics(dm);
            WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
            float rate = 0.9f;
            params.width = (int) (dm.widthPixels * 0.85f);
            getDialog().getWindow().setAttributes(params);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }

    private void initView() {
        timePicker = fv(R.id.timepicker);
        negativeButton = fv(R.id.button_negative);
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.offAlarm(context);
                sp.setSettingTimerRelease();
                if (negativeListener != null)
                    negativeListener.onDialogNegative(TimerDialog.this, tag);
                dismiss();
            }
        });
        positiveButton = fv(R.id.button_positive);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp.setSettingTimerHour(timePicker.getCurrentHour());
                sp.setSettingTimerMinute(timePicker.getCurrentMinute());
                if (positiveListener != null)
                    positiveListener.onDialogPositive(TimerDialog.this, tag);
                dismiss();
            }
        });

        Calendar c = Calendar.getInstance();


        timePicker.setCurrentHour(sp.getSettingTimerHour() == -1 ? c.get(Calendar.HOUR_OF_DAY) : sp.getSettingTimerHour());
        timePicker.setCurrentMinute(sp.getSettingTimerMinute() == -1 ? c.get(Calendar.MINUTE) : sp.getSettingTimerMinute());
    }


}
