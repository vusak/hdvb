package io.jmobile.video.browser.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.VideoHistoryItem;

public class VideoHistoryAdapter extends BaseExpandableListAdapter {

    private Context context;
    private Map<String, List<VideoHistoryItem>> map;
    private List<String> groupNameList;
    private LayoutInflater li;

    public VideoHistoryAdapter(Activity context, Map<String, List<VideoHistoryItem>> map, List<String> groupNameList) {

        this.context = context;
        this.map = map;
        this.groupNameList = groupNameList;
        li = context.getLayoutInflater();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        String name = groupNameList.get(groupPosition);
        return map.get(name).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.item_video_history_list, parent, false);

        long current = System.currentTimeMillis();

        VideoHistoryItem item = (VideoHistoryItem) getChild(groupPosition, childPosition);
        TextView url = (TextView) convertView.findViewById(R.id.text_url);
        url.setText(item.getVideoHistoryUrl());

        TextView title = (TextView) convertView.findViewById(R.id.text_title);
        title.setText(item.getVideoHistorySite());

        TextView date = (TextView) convertView.findViewById(R.id.text_date);
        date.setText(Util.getTimeString(item.getVideoHistoryAt()));

        TextView time = (TextView) convertView.findViewById(R.id.text_time);
        time.setText(Util.convertToStringTime(item.getVideoHistoryFileTime()));

        ImageView icon = (ImageView) convertView.findViewById(R.id.image_icon);
        if (item.getVideoHistoryThumnail() != null) {
            icon.setImageBitmap(ImageUtil.getImage(item.getVideoHistoryThumnail()));
        }
//        else
//            icon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_globe_n));

//        View line = (View) convertView.findViewById(R.id.line_history);
//        line.setVisibility(isLastChild ? View.INVISIBLE : View.VISIBLE);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        String name = groupNameList.get(groupPosition);
        return map.get(name).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupNameList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groupNameList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = li.inflate(R.layout.layout_group_history, parent, false);

        TextView date = (TextView) convertView.findViewById(R.id.text_group);
        String name = (String) getGroup(groupPosition);
        if (Util.isToday(name))
            name = context.getString(R.string.today) + " " + name;
        if (Util.isYesterday(name))
            name = context.getString(R.string.yesterday) + " " + name;
        date.setText(name);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
