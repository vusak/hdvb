package io.jmobile.video.browser.data;

import android.os.Parcel;

public class RecommendItem extends TableObject {
    public static final Creator<RecommendItem> CREATOR = new Creator<RecommendItem>() {
        @Override
        public RecommendItem createFromParcel(Parcel source) {
            return new RecommendItem(source);
        }

        @Override
        public RecommendItem[] newArray(int size) {
            return new RecommendItem[size];
        }
    };
    private int index;
    private String url;
    private String name;
    private int iconId;

    public RecommendItem() {
        super();
    }

    public RecommendItem(Parcel in) {
        super(in);

        this.index = in.readInt();
        this.url = in.readString();
        this.name = in.readString();
        this.iconId = in.readInt();
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(index);
        out.writeString(url);
        out.writeString(name);
        out.writeInt(iconId);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RecommendItem && index == ((RecommendItem) obj).index;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconId() {
        return this.iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
