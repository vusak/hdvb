package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.data.CategoryItem;

public class CategoryAdapter extends ReSelectableAdapter<CategoryItem, CategoryAdapter.CategoryHolder> {

    private Context context;
    private ArrayList<CategoryItem> items = new ArrayList<>();

    private int[] bgColors = {
            R.drawable.button_bg_channel_black,
            R.drawable.button_bg_channel_red,
            R.drawable.button_bg_channel_green,
            R.drawable.button_bg_channel_yellow,
            R.drawable.button_bg_channel_purple,
            R.drawable.button_bg_channel_skyblue,
            R.drawable.button_bg_channel_gray,
            R.drawable.button_bg_channel_blue,
            R.drawable.button_bg_channel_orange
    };

    public CategoryAdapter(Context context, int layoutId, ArrayList<CategoryItem> items, ReOnItemClickListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        int standard = bgColors.length;
        int index = position;
        if (index >= standard)
            index = index % standard;
        CategoryItem item = items.get(position);

        holder.root.setBackgroundResource(bgColors[index]);
        holder.title.setText(item.getCategoryName());
        if (item.getCategoryThumbnail() == null)
            holder.thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getCategoryThumbnailUrl()));
        else
            holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getCategoryThumbnail()));

    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final CategoryHolder holder = new CategoryHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.root = holder.fv(R.id.layout_root);
        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.title = holder.fv(R.id.text_title);


        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder h) {
                if (listener != null && items.size() > position) {
                    holder.thumbnail.startAnimation(AnimationUtils.loadAnimation(context, R.anim.scale_alpha));
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }


    public class CategoryHolder extends ReAbstractViewHolder {
        LinearLayout root;
        ImageView thumbnail;
        TextView title;

        public CategoryHolder(View itemView) {
            super(itemView);
        }

    }

    public void updateThumbnail(CategoryItem _item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getCategoryId().equalsIgnoreCase(_item.getCategoryId())) {
                notifyItemChanged(i);
                break;
            }
        }
    }
}
