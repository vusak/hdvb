package io.jmobile.video.browser.data;

import android.os.Parcel;

import org.json.JSONObject;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.Util;

public class VideoItem extends TableObject {
    public static final Creator<VideoItem> CREATOR = new Creator<VideoItem>() {
        @Override
        public VideoItem createFromParcel(Parcel source) {
            return new VideoItem(source);
        }

        @Override
        public VideoItem[] newArray(int size) {
            return new VideoItem[size];
        }
    };
    private int index;
    private String kind;
    private String etag;
    private String id;
    private String channelId;
    private String title;
    private String description;

    public VideoItem() {
        super();
    }

    public VideoItem(JSONObject o) {
        this.id = Util.optString(o, Common.TAG_ID);
        this.kind = Util.optString(o, Common.TAG_KIND);
        this.etag = Util.optString(o, Common.TAG_ETAG);

    }

    public VideoItem(Parcel in) {
        super(in);

        this.index = in.readInt();
        this.id = in.readString();
        this.kind = in.readString();
        this.etag = in.readString();
        this.channelId = in.readString();
        this.title = in.readString();
        this.description = in.readString();
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(index);
        out.writeString(id);
        out.writeString(kind);
        out.writeString(etag);
        out.writeString(channelId);
        out.writeString(title);
        out.writeString(description);

    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof VideoItem && index == ((VideoItem) obj).index;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtag() {
        return this.etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
