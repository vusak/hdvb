package io.jmobile.video.browser.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.graphics.drawable.Animatable2Compat;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.base.BaseDialogFragment;
import io.jmobile.video.browser.base.ExpandLayout;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.MessageDialog;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.CategoryItem;
import io.jmobile.video.browser.network.Api;
import io.jmobile.video.browser.ui.dialog.TimerDialog;
import io.jmobile.video.browser.ui.fragment.MainChannelFragment;
import io.jmobile.video.browser.ui.fragment.MainPlayFragment;
import io.jmobile.video.browser.ui.fragment.MainPlayListFragment;
import io.jmobile.video.browser.ui.fragment.MainSearchFragment;

public class MainActivity extends BaseActivity implements MainSearchFragment.MainSearchFragmentListener {

    LinearLayout mainTopLayout;
    TabLayout tabLayout;
    ViewPager viewPager;
    ImageButton searchButton, menuButton;

    MainPlayFragment playFragment;
    MainChannelFragment channelFragment;
    MainSearchFragment searchFragment;
    MainPlayListFragment playListFragment;

    LinearLayout loadingLayout;
    ImageView loadingImage;
    AnimatedVectorDrawableCompat animatedVector;
    Animation anim;

    Api api;

    ExpandLayout menuLayout;
    View menuCloseView;
    Button timerButton, settingButton;
    private onKeyBackPressedListener mOnkeyBackPressedListener;

    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (playFragment != null && tabLayout != null && tabLayout.getSelectedTabPosition() == 0) {
                playFragment.pauseVideo();
                if (LogUtil.DEBUG_MODE)
                    Toast.makeText(MainActivity.this, "MainActivity Times up!!!", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
//        sp.setCheckCategoryUpdateVersion("-1");

//        if (playFragment != null)
//            playFragment.startLoading();
        startLoading();
        api.getCategoryList();
    }

    public void setOnkeyBackPressedListener(onKeyBackPressedListener listener) {
        mOnkeyBackPressedListener = listener;
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.API_CODE_GET_CATEGORY) {
            boolean update = m.getData().getBoolean(Common.ARG_CATEGORY_UPDATE);

            if (update) {
                if (channelFragment != null)
                    channelFragment.refreshList();

                CategoryItem item = db.getLiveCategoryItem();
                if (item != null)
                    api.getChannelList(item.getCategoryId(), true);
            }
            else {
                endLoading();
//                if (playFragment != null)
//                    playFragment.endLoading();
            }
        }
        else if (m.what == Common.API_CODE_GET_CHANNEL) {
            boolean isLive = m.getData().getBoolean(Common.ARG_IS_LIVE);
            if (isLive && playFragment != null)
                playFragment.refreshList(false);

            endLoading();
//            if (playFragment != null)
//                playFragment.endLoading();
        }
        super.handleApiMessage(m);
    }

    @Override
    public void onBackPressed() {
        if (mOnkeyBackPressedListener != null) {
            mOnkeyBackPressedListener.onBack();
        } else {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_exit));
                    dlg.setNegativeLabel(r.s(R.string.cancel));
                    dlg.setPositiveLabel(r.s(R.string.exit));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            MainActivity.super.onBackPressed();
                        }
                    });
                sdf(dlg);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    private void initView() {
        mainTopLayout = fv(R.id.layout_main_top);
        searchButton = fv(R.id.button_main_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        menuButton = fv(R.id.button_main_menu);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
//                menuCloseView.setVisibility(View.VISIBLE);
//                menuLayout.expand(true);
            }
        });
        menuLayout = fv(R.id.layout_expand_menu);
        menuCloseView = fv(R.id.view_close_menu);
        menuCloseView.setVisibility(View.GONE);
        menuCloseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCloseView.setVisibility(View.GONE);
                menuLayout.collapse(true);
            }
        });

        timerButton = fv(R.id.button_timer);
        timerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCloseView.setVisibility(View.GONE);
                menuLayout.collapse(true);

                if (fdf(TimerDialog.TAG) == null) {
                    TimerDialog dlg = TimerDialog.newInstance(TimerDialog.TAG, MainActivity.this);
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {

                        }
                    });
                    sdf(dlg);
                }
            }
        });

        settingButton = fv(R.id.button_setting);
        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuCloseView.setVisibility(View.GONE);
                menuLayout.collapse(true);
            }
        });
        viewPager = fv(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);
        tabLayout = fv(R.id.tabs_main);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView text = new TextView(MainActivity.this);
                text.setTextSize(Util.convertDpToPixel(2.5f, MainActivity.this));
                text.setGravity(Gravity.CENTER);
                switch (tabLayout.getSelectedTabPosition()) {
                    case 0:
                        text.setText(r.s(R.string.main_tab_play));
                        text.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_live_tv_p_1_24, 0, 0);
                        break;

                    case 1:
                        text.setText(r.s(R.string.main_tab_chanel));
                        text.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subscriptions_p_1_24, 0, 0);
                        break;

                    case 2:
                        text.setText(r.s(R.string.main_tab_search));
                        text.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_explore_p_1_24, 0, 0);
                        break;

                    case 3:
                        text.setText(r.s(R.string.main_tab_list));
                        text.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_format_list_numbered_p_1_24, 0, 0);
                        break;
                }
                text.setTextColor(r.c(MainActivity.this, R.color.p1_a100));
                tab.setCustomView(text);
                tab.setIcon(null);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tabLayout.getSelectedTabPosition()) {
                    case 0:
                        tab.setIcon(r.d(MainActivity.this, R.drawable.ic_live_tv_border_gray_24));

                        break;

                    case 1:
                        tab.setIcon(r.d(MainActivity.this, R.drawable.ic_subscriptions_border_gray_24));
                        break;

                    case 2:
                        tab.setIcon(r.d(MainActivity.this, R.drawable.ic_explore_border_gray_24));
                        break;

                    case 3:
                        tab.setIcon(r.d(MainActivity.this, R.drawable.ic_format_list_numbered_border_24));
                        break;
                }
                tab.setCustomView(null);
                tab.setText("");
                mainTopLayout.setVisibility(View.VISIBLE);
                Util.hideKeyBoard(MainActivity.this);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        TextView text = new TextView(MainActivity.this);
        text.setTextSize(Util.convertDpToPixel(2.5f, MainActivity.this));
        text.setGravity(Gravity.CENTER);
        text.setText(r.s(R.string.main_tab_play));
        text.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_live_tv_p_1_24, 0, 0);
        text.setTextColor(r.c(MainActivity.this, R.color.p1_a100));
        tabLayout.getTabAt(0).setCustomView(text);
        tabLayout.getTabAt(0).setIcon(null);
//        tabLayout.getTabAt(0).setIcon(r.d(this, R.drawable.tab_main_live));
//        tabLayout.getTabAt(0).setText(r.s(R.string.main_tab_play));
        tabLayout.getTabAt(1).setIcon(r.d(this, R.drawable.tab_main_channel));
        tabLayout.getTabAt(2).setIcon(r.d(this, R.drawable.tab_main_explore));
//        tabLayout.setTabMode(TabLayout.MODE_FIXED);
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
//        tabLayout.getTabAt(3).setIcon(r.d(this, R.drawable.tab_main_list));

        loadingLayout = fv(R.id.layout_loading);
        loadingLayout.setVisibility(View.GONE);
        loadingImage = fv(R.id.image_loading);
    }

    @Override
    public void onHideMainTopLayout(boolean hide) {
        mainTopLayout.setVisibility(hide ? View.GONE : View.VISIBLE);
    }

    private void setupViewPager(ViewPager pager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        playFragment = new MainPlayFragment();
        adapter.addFrag(playFragment, r.s(R.string.main_tab_play));
        channelFragment = new MainChannelFragment();
        adapter.addFrag(channelFragment, r.s(R.string.main_tab_chanel));
        searchFragment = new MainSearchFragment();
        searchFragment.setListener(this);
        adapter.addFrag(searchFragment, r.s(R.string.main_tab_search));
//        playListFragment = new MainPlayListFragment();
//        adapter.addFrag(playListFragment, r.s(R.string.main_tab_list));
        viewPager.setAdapter(adapter);
    }

    public interface onKeyBackPressedListener {
        public void onBack();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }
    }

    private void startLoading() {
        if (loadingLayout == null)
            return ;

        if (Build.VERSION.SDK_INT < 21) {
            startBounceAnimDrawable();
        } else {
            startAnimDrawable();
        }
    }

    private void endLoading() {
        if (loadingLayout == null)
            return ;

        if (loadingLayout.getVisibility() == View.VISIBLE) {
            loadingLayout.setVisibility(View.GONE);
            loadingImage.clearAnimation();
            if (animatedVector != null && animatedVector.isRunning())
                animatedVector.clearAnimationCallbacks();
        }
    }

    private void startAnimDrawable() {
        loadingLayout.setVisibility(View.VISIBLE);
        animatedVector = AnimatedVectorDrawableCompat.create(this, R.drawable.loading);
        animatedVector.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                startAnimDrawable();
            }
        });

        loadingImage.setImageDrawable(animatedVector);
        animatedVector.start();
    }

    private void startBounceAnimDrawable() {
        loadingLayout.setVisibility(View.VISIBLE);
        loadingImage.clearAnimation();
        anim = AnimationUtils.loadAnimation(this, R.anim.bounce_intro);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingImage.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        loadingImage.startAnimation(anim);
    }
}
