package io.jmobile.video.browser.ui.fragment;

import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.adapter.RecommendAdapter;
import io.jmobile.video.browser.adapter.UrlAdapter;
import io.jmobile.video.browser.base.BaseFragment;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.FavoriteItem;
import io.jmobile.video.browser.data.RecommendItem;
import io.jmobile.video.browser.network.Api;
import io.jmobile.video.browser.ui.MainActivity;

public class MainSearchFragment extends BaseFragment {

//    EditText keywordEdit;
//    ImageButton searchButton;
//
//    RecyclerView lv;
//    LinearLayoutManager manager;
//    SearchResultAdapter adapter;
//    ArrayList<SearchResultItem> list;
//
//    FrameLayout youtubeLayout;
    ////////////////////////////////

    // front layout
    Button urlButton;
    RecyclerView lv;
    LinearLayoutManager manager;
    ArrayList<RecommendItem> list;
    RecommendAdapter adapter;

    // url layout
    LinearLayout urlLayout;
    ImageView searchImage;
    ImageButton urlDelButton;
    EditText urlEdit;
    TextView urlText;
    ListView urlLv;
    UrlAdapter urlAdapter;
    ArrayList<String> urlList, favList;

    // Browser layout
    LinearLayout browserLayout;
    BrowserFragment currBrowserFragment;

    Api api;
    MainSearchFragmentListener listener;

    private Handler urlSearchHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                if (urlList == null)
                    urlList = new ArrayList<>();
                else
                    urlList.clear();
                String keyword = urlEdit.getText().toString();
                for (String temp : favList) {
                    if (temp.contains(keyword))
                        urlList.add(temp);
                }

                urlAdapter.setItems(urlList, keyword);
            }
        }
    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main_browsing;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).setOnkeyBackPressedListener(new MainActivity.onKeyBackPressedListener() {
            @Override
            public void onBack() {
                if (urlLayout.getVisibility() == View.VISIBLE) {
                    urlLayout.setVisibility(View.GONE);
                    if (browserLayout.getVisibility() != View.VISIBLE && listener != null)
                        listener.onHideMainTopLayout(false);
                } else if (browserLayout.getVisibility() == View.VISIBLE) {
                    if (!currBrowserFragment.onPrevButtonClicked())
                        visibleBrowser(false);
                } else {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.setOnkeyBackPressedListener(null);
                    mainActivity.onBackPressed();
                }
            }
        });
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);
        initView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        if (urlLayout == null)
            return;

        if (isVisibleToUser) {
            if (listener != null
                    && (urlLayout.getVisibility() == View.VISIBLE
                    || browserLayout.getVisibility() == View.VISIBLE))
                listener.onHideMainTopLayout(true);

        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    public void setListener(MainSearchFragmentListener listener) {
        this.listener = listener;
    }

    private void initView() {
        urlLayout = fv(R.id.layout_browser_url);
        urlLayout.setVisibility(View.GONE);

        urlButton = fv(R.id.button_url);
        urlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlLayout.setVisibility(View.VISIBLE);
                if (listener != null)
                    listener.onHideMainTopLayout(true);
                Util.showKeyBoard(urlEdit);

                if (favList == null)
                    favList = new ArrayList<String>();
                else
                    favList.clear();

                ArrayList<FavoriteItem> list = new ArrayList<FavoriteItem>();
                list.addAll(db.getFavoriteItems());
                if (list == null || list.size() < 0)
                    return;

                for (FavoriteItem item : list) {
                    String url = item.getFavoriteSite().replace("http://", "");
                    url = url.replace("https://", "");
                    favList.add(url);
                }
            }
        });

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 5);
        lv.setLayoutManager(manager);
        adapter = new RecommendAdapter(getActivity(), R.layout.item_recommend, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                String url = ((RecommendItem) item).getUrl();
                if (!url.contains("http://") && !url.contains("https://"))
                    url = "http://" + url;
                goToBrowser(url);
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);
        initRecommendList();

        urlEdit = fv(R.id.edit_url);
        urlEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                urlText.setText(s.toString());
                urlSearchHandler.removeCallbacksAndMessages(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean empty = s.toString().isEmpty();
                searchImage.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlDelButton.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlText.setVisibility(empty ? View.GONE : View.VISIBLE);
                urlText.setText(s.toString());
                urlLv.setVisibility(empty ? View.GONE : View.VISIBLE);

                if (!empty)
                    urlSearchHandler.sendEmptyMessage(0);
            }
        });

        urlEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    Util.hideKeyBoard(getActivity());
                    String url = urlEdit.getText().toString();

                    String SCHEME = "http://";
                    if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
//                            String googleSearch = "http://www.google.com/m?q=";
                        sp.setBrowserUrl(getSearchEnging() + url);
                    } else if (url.matches("http.*")) { // http로 시작
                        sp.setBrowserUrl(url);
                    } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
                            || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
                            || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
                            || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
                            || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
                        sp.setBrowserUrl(SCHEME + url);
                    } else {
//                            String googleSearch = "http://www.google.com/m?q=";
                        sp.setBrowserUrl(getSearchEnging() + url);

                    }

//                        btnBack.setVisibility(View.VISIBLE);
                    goToBrowser(sp.getBrowserUrl());
                }
                return false;
            }

        });

        searchImage = fv(R.id.image_search);
        searchImage.setVisibility(View.GONE);
        urlDelButton = fv(R.id.button_delete);
        urlDelButton.setVisibility(View.GONE);
        urlDelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlEdit.setText("");
            }
        });

        urlText = fv(R.id.text_url);
        urlText.setVisibility(View.GONE);
        urlText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.hideKeyBoard(getActivity());
                String url = urlText.getText().toString();

                String SCHEME = "http://";
                if (url.matches("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*") || url.matches(".* .*")) {  // 영어, 한글, 뛰어쓰기
//                            String googleSearch = "http://www.google.com/m?q=";
                    sp.setBrowserUrl(getSearchEnging() + url);
                } else if (url.matches("http.*")) { // http로 시작
                    sp.setBrowserUrl(url);
                } else if (url.matches(".*.com") || url.matches(".*.net") || url.matches(".*.kr") || url.matches(".*.co.kr")
                        || url.matches(".*.co") || url.matches(".*.io") || url.matches(".*.tech") || url.matches(".*.org")
                        || url.matches(".*.or.kr") || url.matches(".*.cn") || url.matches(".*.in") || url.matches(".*.us") || url.matches(".*.me")
                        || url.matches(".*.jp") || url.matches(".*.fr") || url.matches(".*.id") || url.matches(".*.biz") || url.matches(".*.tv")
                        || url.matches(".*.eu") || url.matches(".*.in") || url.matches(".*.pk")) { // .com .net 등
                    sp.setBrowserUrl(SCHEME + url);
                } else {
//                            String googleSearch = "http://www.google.com/m?q=";
                    sp.setBrowserUrl(getSearchEnging() + url);

                }

//                        btnBack.setVisibility(View.VISIBLE);
                goToBrowser(sp.getBrowserUrl());
            }
        });

        urlLv = fv(R.id.list_url);
        urlLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (urlList == null || urlList.size() < position)
                    return;
                String url = urlList.get(position);
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                urlEdit.setText(url);
                goToBrowser(url);
            }
        });
        urlList = new ArrayList<>();
        favList = new ArrayList<>();
        urlAdapter = new UrlAdapter(getActivity(), urlList);
        urlLv.setAdapter(urlAdapter);

        browserLayout = fv(R.id.layout_fragment_browser);
        browserLayout.setVisibility(View.INVISIBLE);
        currBrowserFragment = new BrowserFragment();
        currBrowserFragment.setListener(new BrowserFragment.BrowserFragmentListsner() {
            @Override
            public void onUrlEditClick(String url) {
                urlLayout.setVisibility(View.VISIBLE);
                if (listener != null)
                    listener.onHideMainTopLayout(true);
                Util.showKeyBoard(urlEdit);

                if (favList == null)
                    favList = new ArrayList<String>();
                else
                    favList.clear();

                ArrayList<FavoriteItem> list = new ArrayList<FavoriteItem>();
                list.addAll(db.getFavoriteItems());
                if (list == null || list.size() < 0)
                    return;

                for (FavoriteItem item : list) {
                    String u = item.getFavoriteSite().replace("http://", "");
                    u = u.replace("https://", "");
                    favList.add(u);
                }

                urlEdit.setText(url);
            }

            @Override
            public void addVideoHistoryUrl(String site, String url) {

            }

            @Override
            public void onNewTabClick() {

            }

            @Override
            public void onHomeButtonClick() {
                visibleBrowser(false);
            }
        });
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.layout_fragment_browser, currBrowserFragment);
        transaction.commit();
    }

    private void initRecommendList() {
        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();

        String[] nameList = getResources().getStringArray(R.array.recommend_name);
        String[] urlList = getResources().getStringArray(R.array.recommend_url);
        TypedArray iconList = getResources().obtainTypedArray(R.array.recommend_icon);

        for (int i = 0; i < nameList.length; i++) {
            RecommendItem item = new RecommendItem();
            item.setIndex(i);
            item.setName(nameList[i]);
            item.setUrl(urlList[i]);
            item.setIconId(iconList.getResourceId(i, -1));
            list.add(item);
        }
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    private void goToBrowser(String url) {
        Util.hideKeyBoard(urlEdit);
        urlLayout.setVisibility(View.GONE);
//        if (url.matches("(?i).*youtube.*")) {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//            return;
//        }

        visibleBrowser(true);
        currBrowserFragment.goUrl(url);
    }

    private void visibleBrowser(boolean show) {
        if (listener != null)
            listener.onHideMainTopLayout(show);
        browserLayout.setVisibility(show ? View.VISIBLE : View.INVISIBLE);

        if (!show) {
            if (currBrowserFragment != null) {
                currBrowserFragment.stopWebViewLoading();
            }
            currBrowserFragment.clearCurrData();
        } else {
            if (currBrowserFragment != null) {
                currBrowserFragment.onResume();
            }
        }
    }

    private String getSearchEnging() {
        String engine = "http://www.google.com/m?q=";
        switch (sp.getSearchEngine()) {
            case 0: // Google
                engine = "http://www.google.com/m?q=";
                break;

            case 1: // Naver
                engine = "http://m.search.naver.com/search.naver?query=";
                break;

            case 2: // bing
                engine = "http://www.bing.com/search?q=";
                break;
        }

        return engine;
    }

    public static interface MainSearchFragmentListener {
        public void onHideMainTopLayout(boolean hide);
    }
//    private void initView() {
//        keywordEdit = fv(R.id.edit_keyword);
//        searchButton = fv(R.id.button_search);
//        searchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!TextUtils.isEmpty(keywordEdit.getText().toString()))
//                    api.getSearchResultList(keywordEdit.getText().toString(), "");
//
//                Util.hideKeyBoard(getActivity());
//            }
//        });
//
//        lv = fv(R.id.lv);
//        list = new ArrayList<>();
//        manager = new GridLayoutManager(getActivity(), 1);
//        lv.setLayoutManager(manager);
//        adapter = new SearchResultAdapter(getActivity(), R.layout.item_search_result, list, new ReAdapter.ReOnItemClickListener() {
//            @Override
//            public void OnItemClick(int position, Object item) {
////                Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(), Common.YOUTUBE_API_KEY, ((SearchResultItem) item).getVideoId());
//                Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(), Common.YOUTUBE_API_KEY, ((SearchResultItem) item).getVideoId(), 0, true, true);
//
////                Intent intent = YouTubeStandalonePlayer.createPlaylistIntent(getActivity(), Common.YOUTUBE_API_KEY, "PLrEnWoR732-Cb5ZXewsGR19TveCtDJmIs");
//                startActivity(intent);
//            }
//
//            @Override
//            public void OnItemLongClick(int position, Object item) {
//
//            }
//        });
//        lv.setAdapter(adapter);
//
//    }
//
//    @Override
//    public void handleApiMessage(Message m) {
//        if (m.what == Common.YOUTUBE_API_CODE_SEARCH) {
//            ArrayList<SearchResultItem> temp = m.getData().getParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST);
//            if (temp != null && temp.size() > 0) {
//                list.clear();
//                list.addAll(temp);
//                adapter.notifyDataSetChanged();
//            }
//        }
//    }


}
