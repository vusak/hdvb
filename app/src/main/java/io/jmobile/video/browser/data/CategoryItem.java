package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class CategoryItem extends TableObject {
    public static final String TABLE_NAME = "category";

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_LIVE = 1;

    public static final String ROW_ID = "_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_THUMBNAIL_URL = "category_thumbnail_url";
    public static final String CATEGORY_THUMBNAIL = "category_thumbnail";
    public static final String CATEGORY_POSITION = "category_position";
    public static final String CATEGORY_TYPE = "category_type";

    public static final Creator<CategoryItem> CREATOR = new Creator<CategoryItem>() {
        @Override
        public CategoryItem createFromParcel(Parcel source) {
            return new CategoryItem(source);
        }

        @Override
        public CategoryItem[] newArray(int size) {
            return new CategoryItem[size];
        }
    };

    private long id;
    private int position;
    private String categoryId;
    private String name;
    private String thumbnailUrl;
    private byte[] thumbnail;
    private int type;

    public CategoryItem() {
        super();
    }

    public CategoryItem(JSONObject o) {
        this.categoryId = Util.optString(o, Common.TAG_IDX);
        this.name = Util.optString(o, Common.TAG_NAME);
        this.thumbnailUrl = Util.optString(o, Common.TAG_THUMBNAIL);
        this.position = Integer.valueOf(Util.optString(o, Common.TAG_POSITION));
        this.type = Integer.valueOf(Util.optString(o, Common.TAG_LIVE_BROADCAST));
    }

//    public CategoryItem(JSONObject o) {
//        this.id = Util.optString(o, Common.TAG_ID);
//        this.etag = Util.optString(o, Common.TAG_ETAG);
//        this.kind = Util.optString(o, Common.TAG_KIND);
//        try {
//            JSONObject ob = new JSONObject(Util.optString(o, Common.TAG_SNIPPET));
//            this.channelId = Util.optString(ob, Common.TAG_CHANNEL_ID);
//            this.title = Util.optString(ob, Common.TAG_TITLE);
//        } catch (Exception e) {
//            LogUtil.log(e.getMessage());
//        }
//    }

    public CategoryItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.categoryId = in.readString();
        this.position = in.readInt();
        this.name = in.readString();
        this.thumbnailUrl = in.readString();
        in.readByteArray(this.thumbnail);
        this.type = in.readInt();
    }

    public static void createTableCategory(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(CATEGORY_ID + " text not null unique, ")
                .append(CATEGORY_POSITION + " integer, ")
                .append(CATEGORY_NAME + " text, ")
                .append(CATEGORY_THUMBNAIL_URL + " text, ")
                .append(CATEGORY_TYPE + " integer, ")
                .append(CATEGORY_THUMBNAIL + " BLOB) ")
                .toString());
    }

    public static void updateAddColumnVer3(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD " + CATEGORY_TYPE + " integer");
    }

    public static CategoryItem getCategoryItem(SQLiteDatabase db, String id) {
        List<CategoryItem> list = getCategoryItems(db, null, CATEGORY_ID + " = ?", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static CategoryItem getLiveCategoryItem(SQLiteDatabase db) {
        List<CategoryItem> list = getCategoryItems(db, null, CATEGORY_TYPE + " = ?", new String[]{ String.valueOf(TYPE_LIVE)}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<CategoryItem> getCategoryItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<CategoryItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                CategoryItem item = new CategoryItem()
                        .setRowId(c)
                        .setCategoryId(c)
                        .setCategoryPosition(c)
                        .setCategoryType(c)
                        .setCategoryName(c)
                        .setCategoryThumbnailUrl(c)
                        .setCategoryThumbnail(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateCategoryItem(SQLiteDatabase db, CategoryItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putCategoryId(v)
                    .putCategoryPosition(v)
                    .putCategoryName(v)
                    .putCategoryType(v)
                    .putCategoryThumbnailUrl(v)
                    .putCategoryThumbnail(v);

            int rowAffected = db.update(TABLE_NAME, v, CATEGORY_ID + " =? ", new String[]{item.getCategoryId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getCategoryName());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAppCategoryItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(position);
        out.writeString(categoryId);
        out.writeString(name);
        out.writeString(thumbnailUrl);
        out.writeByteArray(thumbnail);
        out.writeInt(type);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof CategoryItem && categoryId == ((CategoryItem) obj).categoryId;
    }

    public long getRowId() {
        return this.id;
    }

    public CategoryItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public CategoryItem setRowId(long id) {
        this.id = id;

        return this;
    }

    public CategoryItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public CategoryItem setCategoryId(Cursor c) {
        this.categoryId = s(c, CATEGORY_ID);

        return this;
    }

    public CategoryItem setCategoryId(String id) {
        this.categoryId = id;

        return this;
    }

    public CategoryItem putCategoryId(ContentValues v) {
        v.put(CATEGORY_ID, this.categoryId);

        return this;
    }

    public int getCategoryPosition() {
        return this.position;
    }

    public CategoryItem setCategoryPosition(Cursor c) {
        this.position = i(c, CATEGORY_POSITION);

        return this;
    }

    public CategoryItem setCategoryPosition(int position) {
        this.position = position;

        return this;
    }

    public CategoryItem putCategoryPosition(ContentValues v) {
        v.put(CATEGORY_POSITION, this.position);

        return this;
    }

    public String getCategoryName() {
        return this.name;
    }

    public CategoryItem setCategoryName(Cursor c) {
        this.name = s(c, CATEGORY_NAME);

        return this;
    }

    public CategoryItem setCategoryName(String name) {
        this.name = name;

        return this;
    }

    public CategoryItem putCategoryName(ContentValues v) {
        v.put(CATEGORY_NAME, this.name);

        return this;
    }

    public String getCategoryThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public CategoryItem setCategoryThumbnailUrl(Cursor c) {
        this.thumbnailUrl = s(c, CATEGORY_THUMBNAIL_URL);

        return this;
    }

    public CategoryItem setCategoryThumbnailUrl(String url) {
        this.thumbnailUrl = url;

        return this;
    }

    public CategoryItem putCategoryThumbnailUrl(ContentValues v) {
        v.put(CATEGORY_THUMBNAIL_URL, this.thumbnailUrl);

        return this;
    }

    public byte[] getCategoryThumbnail() {
        return this.thumbnail;
    }

    public CategoryItem setCategoryThumbnail(Cursor c) {
        this.thumbnail = blob(c, CATEGORY_THUMBNAIL);

        return this;
    }

    public CategoryItem setCategoryThumbnail(byte[] thumbnail) {
        this.thumbnail = thumbnail;

        return this;
    }

    public CategoryItem putCategoryThumbnail(ContentValues v) {
        v.put(CATEGORY_THUMBNAIL, this.thumbnail);

        return this;
    }

    public int getCategoryType() {
        return this.type;
    }

    public CategoryItem setCategoryType(int type) {
        this.type = type;

        return this;
    }

    public CategoryItem setCategoryType(Cursor c) {
        this.type = i(c, CATEGORY_TYPE);

        return this;
    }

    public CategoryItem putCategoryType(ContentValues v) {
        v.put(CATEGORY_TYPE, this.type);

        return this;
    }
}
