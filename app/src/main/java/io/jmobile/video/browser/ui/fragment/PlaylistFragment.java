package io.jmobile.video.browser.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.adapter.SearchResultAdapter;
import io.jmobile.video.browser.base.BaseFragment;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.SearchResultItem;

public class PlaylistFragment extends BaseFragment {

    public static final int REPEAT_NONE = 0;
    public static final int REPEAT_ONCE = 1;
    public static final int REPEAT_LOOP = 2;
    public static final int SHUFFLE_OFF = 0;
    public static final int SHUFFLE_ON = 1;
    RecyclerView lv;
    ArrayList<SearchResultItem> list;
    ArrayList<String> videoIds;
    LinearLayoutManager manager;
    SearchResultAdapter adapter;
    int selectedIndex;
    PlaylistFragmentListener listener;
    TextView mainText, subText;
    ImageButton miniButton, repeatButton, shuffleButton;
    int modeRepeat = REPEAT_NONE;
    int modeShuffle = SHUFFLE_OFF;
    String curVideoId = "";
    int curIndex = 0;
    private Paint p = new Paint();

    @Override
    public int getLayoutId() {
        return R.layout.fragment_playlist;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    private void initView() {
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new SearchResultAdapter(getActivity(), R.layout.item_channel_play_list, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                SearchResultItem videoItem = (SearchResultItem) item;
                if (videoItem == null)
                    return;

                curIndex = position;
                if (listener != null)
                    listener.onChangedVideo(videoItem.getVideoId());
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        mainText = fv(R.id.text_main);
        subText = fv(R.id.text_sub);

        miniButton = fv(R.id.button_mini);
        miniButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onMiniButtonClick();
            }
        });

        repeatButton = fv(R.id.button_loop);
        repeatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (modeRepeat) {
                    case REPEAT_NONE:
                        modeRepeat = REPEAT_LOOP;
                        break;

                    case REPEAT_ONCE:
                        modeRepeat = REPEAT_NONE;
                        break;

                    case REPEAT_LOOP:
                        modeRepeat = REPEAT_ONCE;
                        break;
                }

                sp.setPlayerRepeatMode(modeRepeat);
                setRepeatMode();
            }

        });
        modeRepeat = sp.getPlayerRepeatMode();
        setRepeatMode();

        shuffleButton = fv(R.id.button_shuffle);
        shuffleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modeShuffle = (modeShuffle == SHUFFLE_OFF) ? SHUFFLE_ON : SHUFFLE_OFF;
                sp.setPlayerShuffleMode(modeShuffle);
                setShuffleMode();
            }
        });
        modeShuffle = sp.getPlayerShuffleMode();
    }

    public void refreshList(ArrayList<SearchResultItem> items) {
        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();

        list.addAll(items);
        adapter.notifyDataSetChanged();

        if (videoIds == null)
            videoIds = new ArrayList<>();
        else
            videoIds.clear();

        for (SearchResultItem item : items)
            videoIds.add(item.getVideoId());

        setShuffleMode();
//        adapter.setSelectedIndex(index);
//        selectedIndex = index;
    }

    public void loaded(String id) {
        if (TextUtils.isEmpty(id))
            return;

        for (int i = 0; i < list.size(); i++) {
            curVideoId = id;
            SearchResultItem item = list.get(i);
            if (id.equals(item.getVideoId())) {
                mainText.setText(item.getTitle());
                subText.setText(item.getDescription());
                adapter.setSelectedIndex(i);
                item.setHistoryAt(System.currentTimeMillis());
                db.insertOrUpdateHistoryVideoItem(item);
                break;
            }
        }
    }

    public String getNextVideoId() {
        if (modeRepeat == REPEAT_NONE)
            return "";

        curIndex += 1;
        if (curIndex >= videoIds.size()) {
            if (modeRepeat == REPEAT_ONCE)
                return "";
            else
                curIndex = 0;
        }

        return videoIds.get(curIndex);
    }

    public void setListener(PlaylistFragmentListener listener) {
        this.listener = listener;
    }

    private void setRepeatMode() {
        int id = R.drawable.ic_repeat_white_off_24;
        switch (modeRepeat) {
            case REPEAT_NONE:
                id = R.drawable.ic_repeat_white_off_24;
                break;

            case REPEAT_ONCE:
                id = R.drawable.ic_repeat_one_white_on_24;
                break;

            case REPEAT_LOOP:
                id = R.drawable.ic_repeat_white_on_24;
                break;
        }
        repeatButton.setImageResource(id);
    }

    private void setShuffleMode() {
        boolean isOff = modeShuffle == SHUFFLE_OFF;
        shuffleButton.setImageResource(isOff ? R.drawable.ic_shuffle_white_off_24 : R.drawable.ic_shuffle_white_on_24);
        if (isOff) {
            videoIds.clear();
            for (SearchResultItem item : list)
                videoIds.add(item.getVideoId());
        } else
            Collections.shuffle(videoIds);
    }

    private void moveItem(int oldPos, int newPos) {
        SearchResultItem item = list.get(oldPos);
        list.remove(oldPos);
        list.add(newPos, item);
        adapter.notifyItemMoved(oldPos, newPos);

        videoIds.clear();

        for (SearchResultItem temp : list)
            videoIds.add(temp.getVideoId());
    }

    private void deleteItem(int position) {
        String id = list.get(position).getVideoId();
        if (id.equalsIgnoreCase(curVideoId)) {
            adapter.notifyDataSetChanged();
            Toast.makeText(getActivity(), "재생중입니다.", Toast.LENGTH_SHORT).show();
        } else {
            adapter.removeItem(id);
            videoIds.remove(id);
        }
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                deleteItem(position);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    Bitmap icon;
                    if (dX > 0) {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.p2_a100));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_amazon_color_48);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(10, getActivity()));
                        c.drawText(r.s(R.string.delete), icon_dest.right + width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    } else {
                        p.setColor(ContextCompat.getColor(getActivity(), R.color.p2_a100));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_amazon_color_48);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);

                        p.setColor(Color.WHITE);
                        p.setTextAlign(Paint.Align.CENTER);
                        p.setTextSize(Util.convertDpToPixel(10, getActivity()));
                        c.drawText(r.s(R.string.delete), icon_dest.left - width, icon_dest.centerY() + Util.convertDpToPixel(6, getActivity()), p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        return simpleCallback;
    }

    public static interface PlaylistFragmentListener {
        public void onMiniButtonClick();

        public void onChangedPlaylist(ArrayList<String> playlist);

        public boolean isPlaying();

        public void onChangedVideo(String id);
    }
}
