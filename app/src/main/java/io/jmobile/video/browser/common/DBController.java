package io.jmobile.video.browser.common;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

import io.jmobile.video.browser.data.BrowserItem;
import io.jmobile.video.browser.data.CategoryItem;
import io.jmobile.video.browser.data.ChannelItem;
import io.jmobile.video.browser.data.ExtraItem;
import io.jmobile.video.browser.data.FavoriteItem;
import io.jmobile.video.browser.data.KeywordItem;
import io.jmobile.video.browser.data.LiveVideoItem;
import io.jmobile.video.browser.data.SearchResultItem;
import io.jmobile.video.browser.data.VideoCategoryItem;
import io.jmobile.video.browser.data.VideoHistoryItem;

public class DBController {
    public static final String DB_ID = "_id";

    private static final String DB_NAME = "hdvb.db";
    private static final int DB_VERSION = 3;
    private static DBHelper helper;
    private static SQLiteDatabase db;
    private Context context;

    public DBController(Context context) {
        this.context = context;

        if (helper == null) {
            helper = new DBHelper(context.getApplicationContext());
            db = helper.getWritableDatabase();
        }
    }

    public synchronized long getNextDBId(String tableName) {
        long id = -1;

        Cursor cursor = db.rawQuery("select seq from SQLITE_SEQUENCE where name = ?", new String[]{tableName});
        if (cursor.moveToFirst())
            id = cursor.getLong(cursor.getColumnIndex("seq"));
        cursor.close();

        return id + 1;
    }

    /**
     * Favorite Items
     **/
    public synchronized FavoriteItem getFavoriteItem(String url) {
        return FavoriteItem.getFavoriteItem(db, url);
    }

    public synchronized List<FavoriteItem> getFavoriteItems() {
        return FavoriteItem.getFavoriteItems(db, null, null, null, null, null, FavoriteItem.FAVORITE_COUNT + " DESC", null);
    }

    public synchronized boolean insertOrUpdateFavoriteItem(FavoriteItem item) {
        return FavoriteItem.insertOrUpdateFavoriteItem(db, item);
    }

    public synchronized boolean deleteFavoriteItem(FavoriteItem item) {
        return FavoriteItem.deleteFavoriteItem(db, item);
    }

    public synchronized boolean deleteAllFavoriteItem() {
        return FavoriteItem.deleteAllFavoriteItems(db);
    }

    public synchronized BrowserItem getBrowserItem(String id) {
        return BrowserItem.getBrowserItem(db, id);
    }

    public synchronized List<BrowserItem> getBrowserItems() {
        return BrowserItem.getBrowserItems(db, null, null, null, null, null, BrowserItem.BROWSER_ORDER + " ASC", null);
    }

    public synchronized boolean insertOrUpdateBrowserItem(BrowserItem item) {
        return BrowserItem.insertOrUpdateBrowserItem(db, item);
    }

    public synchronized boolean updateBrowser(BrowserItem item) {
        return BrowserItem.updateBrowser(db, item);
    }

    public synchronized boolean deleteAllBrowserItems() {
        return BrowserItem.deleteAllBrowserItems(db);
    }

    public synchronized VideoHistoryItem getVideoHistoryItem(String id) {
        return VideoHistoryItem.getVideoHistoryItem(db, id);
    }

    public synchronized List<VideoHistoryItem> getVideoHistoryItems() {
        return VideoHistoryItem.getVideoHistoryItems(db, null, null, null, null, null, VideoHistoryItem.VIDEO_HISTORY_AT + " DESC", null);
    }

    public synchronized boolean insertorUpdateVideoHistoryItem(VideoHistoryItem item) {
        return VideoHistoryItem.insertOrUpdateVideoHistoryItem(db, item);
    }

    public synchronized boolean deleteVideoHistoryItem(VideoHistoryItem item) {
        return VideoHistoryItem.deleteVideoHistoryItem(db, item);
    }

    public synchronized boolean deleteAllVideoHistoryItems() {
        return VideoHistoryItem.deleteAllVideoHistoryItems(db);
    }

    public synchronized boolean insertOrUpdateExtraItem(ExtraItem item) {
        return ExtraItem.insertOrUpdateExtraItem(db, item);
    }

    public synchronized boolean insertOrUpdateExtraItem(byte[] home) {
        return ExtraItem.insertOrUpdateExtraItem(db, home);
    }

    public synchronized byte[] getExtraHomeImage() {
        return ExtraItem.getExtraHomeImage(db);
    }

    public synchronized VideoCategoryItem getVideoCategoryItem(String id) {
        return VideoCategoryItem.getVideoCategoryItem(db, id);
    }

    public synchronized List<VideoCategoryItem> getVideoCategoryItems() {
        return VideoCategoryItem.getVideoCategoryItems(db, null, null, null, null, null, VideoCategoryItem.VIDEO_CATEGORY_INDEX + " DESC", null);
    }

    public synchronized boolean insertOrUpdateVideoCategoryItem(VideoCategoryItem item) {
        return VideoCategoryItem.insertOrUpdateBrowserItem(db, item);
    }

    public synchronized boolean updateVideoCategoryOrder(VideoCategoryItem item) {
        return VideoCategoryItem.updateVideoCategoryOrder(db, item);
    }

    public synchronized boolean deleteVideoCategoryItem(VideoCategoryItem item) {
        return VideoCategoryItem.deleteVideoCategoryItem(db, item);
    }

    public synchronized void deleteAllData() {

    }

    public synchronized KeywordItem getKeywordItem(String id) {
        return KeywordItem.getKeywordItem(db, id);
    }

    public synchronized List<KeywordItem> getKeywordItems() {
        return KeywordItem.getKeywordItems(db, null, null, null, null, null, KeywordItem.ROW_ID + " DESC", null);
    }

    public synchronized boolean insertOrUpdateKeywordItem(KeywordItem item) {
        return KeywordItem.insertOrUpdateKeywordItem(db, item);
    }

    public synchronized SearchResultItem getHistoryItem(String id) {
        return SearchResultItem.getHistoryYoutubeVideoItem(db, id);
    }

    public synchronized List<SearchResultItem> getHistoryVideoItems() {
        return SearchResultItem.getHistoryYoutubeVideoItems(db, null, null, null, null, null, SearchResultItem.ROW_ID + " DESC", null);
    }

    public synchronized boolean insertOrUpdateHistoryVideoItem(SearchResultItem item) {
        return SearchResultItem.insertOrUpdateHistoryYoutubeVideoItem(db, item);
    }

    public synchronized boolean deleteAllHistoryVideoItems() {
        return SearchResultItem.deleteAllHistoryVideoItems(db);
    }

    public synchronized ChannelItem getChannelItem(String id) {
        return ChannelItem.getChannelItem(db, id);
    }

    public synchronized List<ChannelItem> getChannelItems() {
        return ChannelItem.getChannelItems(db, null, null, null, null, null, ChannelItem.ROW_ID + " DESC", null);
    }

    public synchronized List<ChannelItem> getChannelItemsByCategory(String categoryId) {
        return ChannelItem.getChannelItemsByCategory(db, categoryId);
    }

    public synchronized boolean insertOrUpdateChannelItem(ChannelItem item) {
        return ChannelItem.insertOrUpdateChannelItem(db, item);
    }

    public synchronized boolean deleteAllChannelItems() {
        return ChannelItem.deleteAllChannelItems(db);
    }

    public synchronized CategoryItem getCategoryItem(String id) {
        return CategoryItem.getCategoryItem(db, id);
    }

    public synchronized CategoryItem getLiveCategoryItem() {
        return CategoryItem.getLiveCategoryItem(db);
    }

    public synchronized List<CategoryItem> getCategoryItems() {
        return CategoryItem.getCategoryItems(db, null, null, null, null, null, null, null);
    }

    public synchronized boolean insertOrUpdateCategoryItem(CategoryItem item) {
        return CategoryItem.insertOrUpdateCategoryItem(db, item);
    }

    public synchronized boolean deleteAllCategoryItem() {
        return CategoryItem.deleteAppCategoryItems(db);
    }

    public synchronized List<LiveVideoItem> getLiveVideoItems() {
        return LiveVideoItem.getLiveVideoItems(db, null, null, null, null, null, null, null);
    }

    public synchronized  List<LiveVideoItem> getLiveVideoItemsByChannel(String channelId) {
        return LiveVideoItem.getLiveVideoItemsByChannel(db, channelId);
    }

    public synchronized boolean insertOrUpdateLiveVideoItem(LiveVideoItem item) {
        return LiveVideoItem.insertOrUpdateLiveVideoItem(db, item);
    }

    public synchronized boolean deleteAllLiveVideoItems() {
        return LiveVideoItem.deleteAllLiveVideoItems(db);
    }

    public synchronized LiveVideoItem getLiveVideoItem(String id) {
        return LiveVideoItem.getLiveVideoItem(db, id);
    }

    private static class DBHelper extends SQLiteOpenHelper {
        DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // create tables
            createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            switch (oldVersion) {
                case 1:
                case 2:
                    updateVer3(db);
            }
        }

        private void updateVer3(SQLiteDatabase db) {
            CategoryItem.updateAddColumnVer3(db);
            LiveVideoItem.createTableLiveVideoItems(db);
        }

        private void dropAllTables(SQLiteDatabase db) {

        }

        private void createAllTables(SQLiteDatabase db) {
            BrowserItem.createTableBrowserItems(db);
            FavoriteItem.createTableFavorite(db);
            ExtraItem.createTableExtraItems(db);
            VideoHistoryItem.createTableVideoHistory(db);
            VideoCategoryItem.createTableBrowserItems(db);
            KeywordItem.createTableKeyword(db);
            SearchResultItem.createTableHistoryYoutubeVideoItems(db);
            ChannelItem.createTableChannel(db);
            CategoryItem.createTableCategory(db);
            LiveVideoItem.createTableLiveVideoItems(db);
        }
    }
}
