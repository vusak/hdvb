package io.jmobile.video.browser.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.VideoHistoryAdapter;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.VideoHistoryItem;

public class HistoryVideoActivity extends BaseActivity {
    ImageButton backButton;
    TextView titleText;

    TabLayout tabLayout;
    LinearLayout listLayout;
    RecyclerView listLv;
    LinearLayoutManager manager;


    ExpandableListView lv;
    ArrayList<VideoHistoryItem> list;
    Map<String, List<VideoHistoryItem>> map;
    ArrayList<String> groupNameList;
    VideoHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_history_video);

        initView();
        refreshList();
    }

    private void initView() {
        backButton = fv(R.id.btn_top_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        titleText = fv(R.id.text_top_title);
        titleText.setText("Watch Video");

        tabLayout = fv(R.id.tabs_video_history);
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.video_history_tab_today)));
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.video_history_tab_weekly)));
        tabLayout.addTab(tabLayout.newTab().setText(r.s(R.string.video_history_tab_Total)));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        lv = fv(R.id.lv);
        lv.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        lv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String group = groupNameList.get(groupPosition);
                String url = map.get(group).get(childPosition).getVideoHistoryUrl();

                Intent intent = new Intent(HistoryVideoActivity.this, BrowserActivity.class);
                intent.putExtra(Common.INTENT_GO_TO_URL, url);
                startActivity(intent);
                finish();
                return false;
            }
        });

        list = new ArrayList<>();
        map = new HashMap<>();
        groupNameList = new ArrayList<>();

        adapter = new VideoHistoryAdapter(this, map, groupNameList);
        lv.setAdapter(adapter);
    }

    public void refreshList() {
        if (list == null)
            return;

        list.clear();
        list.addAll(db.getVideoHistoryItems());

        groupNameList.clear();
        map.clear();

        for (VideoHistoryItem item : list) {
            long temp = item.getVideoHistoryAt();
            String date = Util.getDateString(temp);
            if (groupNameList.indexOf(date) < 0) {
                groupNameList.add(date);
                map.put(date, new ArrayList<VideoHistoryItem>());
            }
            map.get(date).add(item);
        }

        adapter.notifyDataSetChanged();

        boolean empty = list.size() <= 0;
        lv.setVisibility(empty ? View.GONE : View.VISIBLE);

        for (int i = 0; i < groupNameList.size(); i++)
            lv.expandGroup(i);

    }
}
