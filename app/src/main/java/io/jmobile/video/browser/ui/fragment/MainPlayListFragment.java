package io.jmobile.video.browser.ui.fragment;

import android.os.Bundle;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseFragment;

public class MainPlayListFragment extends BaseFragment {


    @Override
    public int getLayoutId() {
        return R.layout.fragment_main_list;
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();
    }

    private void initView() {

    }

}
