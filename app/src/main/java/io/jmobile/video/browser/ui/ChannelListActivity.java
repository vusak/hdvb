package io.jmobile.video.browser.ui;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.graphics.drawable.Animatable2Compat;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pedrovgs.DraggableListener;
import com.github.pedrovgs.DraggablePanel;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.ChannelAdapter;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.data.ChannelItem;
import io.jmobile.video.browser.data.LiveVideoItem;
import io.jmobile.video.browser.data.SearchResultItem;
import io.jmobile.video.browser.network.Api;
import io.jmobile.video.browser.ui.fragment.PlaylistFragment;

public class ChannelListActivity extends BaseActivity implements View.OnClickListener,
        YouTubePlayer.PlayerStateChangeListener,
        YouTubePlayer.OnFullscreenListener,
        PlaylistFragment.PlaylistFragmentListener {

    ImageButton backButton;
    TextView titleText;

    LinearLayout loadingLayout;
    ImageView loadingImage;
    RecyclerView listView;
    LinearLayoutManager manager;
    ArrayList<ChannelItem> channelList;
    ArrayList<SearchResultItem> list;
    ChannelAdapter adapter;

    Api api;
    String categoryId, categoryTitle;

    YouTubePlayer player;
    YouTubePlayerSupportFragment youTubePlayerSupportFragment;
    DraggablePanel draggablePanel;
    PlaylistFragment playlistFragment;

    boolean isFullScreen = false;
    AnimatedVectorDrawableCompat animatedVector;
    Animation anim;
    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogUtil.DEBUG_MODE)
                Toast.makeText(ChannelListActivity.this, "ChannelListActivity Times up!!!", Toast.LENGTH_SHORT).show();
            if (player != null && player.isPlaying()) {
                player.pause();

            }
        }
    };

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_list);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        if (getIntent() != null) {
            categoryId = getIntent().getStringExtra(Common.INTENT_CATEGORY_ID);
            categoryTitle = getIntent().getStringExtra(Common.INTENT_CATEGORY_TITLE);
        }

        initView();
        if (Build.VERSION.SDK_INT < 21) {
            startBounceAnimDrawable(false);
        } else {
            startAnimDrawable(false);
        }
        api.getChannelList(categoryId, false);
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.YOUTUBE_API_CODE_SEARCH_CHANNEL) {
            ArrayList<SearchResultItem> temp = m.getData().getParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST);
            if (temp != null && temp.size() > 0) {
                list.clear();
                list.addAll(temp);

            }
        } else if (m.what == Common.API_CODE_GET_CHANNEL) {
            boolean success = m.getData().getBoolean(Common.ARG_RESULT);
            if (success) {
                if (channelList == null)
                    channelList = new ArrayList<>();
                channelList.clear();

                ArrayList<ChannelItem> items = m.getData().getParcelableArrayList(Common.ARG_CHANNEL_LIST);
                channelList.addAll(items);
                if (adapter != null)
                    adapter.notifyDataSetChanged();

                if (loadingLayout.getVisibility() == View.VISIBLE) {
                    loadingLayout.setVisibility(View.GONE);
                    loadingImage.clearAnimation();
                    if (animatedVector != null && animatedVector.isRunning())
                        animatedVector.clearAnimationCallbacks();
                }

//                for (int i = 0; i < channelList.size(); i++) {
//                    try {
//                        ChannelItem temp = new AddChannelThumbnail().execute(channelList.get(i)).get();
//                        channelList.get(i).setChannelThumbnail(temp.getChannelTHumbnail());
////                        adapter.notifyItemChanged(i);
//                    }
//                    catch (Exception e) {
//                        LogUtil.log(e.getMessage());
//                    }
//                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (draggablePanel != null && draggablePanel.getVisibility() == View.VISIBLE && draggablePanel.isMaximized()) {
            draggablePanel.minimize();
            player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        } else if (player != null && isFullScreen) {
            player.setFullscreen(false);
        } else
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    @Override
    public void onVideoEnded() {
        String id = playlistFragment.getNextVideoId();
        if (!TextUtils.isEmpty(id))
            player.loadVideo(id);
    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onLoading() {
        if (player != null && !player.isPlaying())
            player.play();
    }

    @Override
    public void onLoaded(String s) {
        if (draggablePanel.getVisibility() == View.VISIBLE)
            player.play();

        if (playlistFragment != null)
            playlistFragment.loaded(s);
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onFullscreen(boolean b) {
        isFullScreen = b;
    }

    // PlaylistFragmentListener
    @Override
    public void onMiniButtonClick() {
        if (!draggablePanel.isMinimized())
            draggablePanel.minimize();
    }

    @Override
    public void onChangedPlaylist(ArrayList<String> playlist) {

    }

    @Override
    public void onChangedVideo(String id) {
        if (player != null) {
            player.loadVideo(id);
        }
    }

    @Override
    public boolean isPlaying() {
        return false;
    }

    @Override
    public void onClick(View v) {
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleText = fv(R.id.text_title);
        titleText.setText(categoryTitle);

        loadingLayout = fv(R.id.layout_loading);
        loadingLayout.setVisibility(View.GONE);
        loadingImage = fv(R.id.image_loading);

        listView = fv(R.id.lv);
        list = new ArrayList<>();
        channelList = new ArrayList<>();
        manager = new GridLayoutManager(this, 2);
        listView.setLayoutManager(manager);
        adapter = new ChannelAdapter(this, R.layout.item_channel, channelList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                adapter.notifyDataSetChanged();
                ChannelItem cItem = (ChannelItem) item;
                api.getSearchChannelVideos(cItem.getChannelValue(), "");
                list.clear();
                if (Build.VERSION.SDK_INT < 21) {
                    startBounceAnimDrawable(false);
                } else {
                    startAnimDrawable(false);
                }
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        listView.setAdapter(adapter);

        draggablePanel = fv(R.id.draggable_panel);
        youTubePlayerSupportFragment = YouTubePlayerSupportFragment.newInstance();
        youTubePlayerSupportFragment.initialize(Common.YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                player = youTubePlayer;
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                player.setPlayerStateChangeListener(ChannelListActivity.this);
                player.setOnFullscreenListener(ChannelListActivity.this);
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
//                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE|YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

        draggablePanel.setFragmentManager(getSupportFragmentManager());
        draggablePanel.setTopFragment(youTubePlayerSupportFragment);
        playlistFragment = new PlaylistFragment();
        playlistFragment.setListener(this);
        draggablePanel.setBottomFragment(playlistFragment);
        draggablePanel.initializeView();
        draggablePanel.setVisibility(View.GONE);
        hookDraggablePanelListeners();
    }

    private void startAnimDrawable(final boolean simple) {
        loadingLayout.setVisibility(View.VISIBLE);
        animatedVector = AnimatedVectorDrawableCompat.create(this, R.drawable.loading);
        animatedVector.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {

            @Override
            public void onAnimationStart(Drawable drawable) {
                super.onAnimationStart(drawable);
            }

            @Override
            public void onAnimationEnd(Drawable drawable) {
//                super.onAnimationEnd(drawable);
                if (simple) {
                    startAnimDrawable(true);
                }
                else {
                    if (list != null && list.size() > 0) {
                        adapter.notifyDataSetChanged();

//                if (playlistFragment != null)
//                    playlistFragment.refreshList(list);
                        if (playlistFragment != null)
                            playlistFragment.refreshList(list);

                        if (player != null) {
                            player.loadVideo(list.get(0).getVideoId());
                            player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
                        }

                        if (draggablePanel.getVisibility() == View.GONE)
                            draggablePanel.setVisibility(View.VISIBLE);
                        else
                            draggablePanel.maximize();
                        loadingLayout.setVisibility(View.GONE);

                    } else
                        startAnimDrawable(false);
                }
            }
        });
        loadingImage.setImageDrawable(animatedVector);
        animatedVector.start();
    }

    private void startBounceAnimDrawable(final boolean simple) {
        loadingLayout.setVisibility(View.VISIBLE);
        loadingImage.clearAnimation();
        anim = AnimationUtils.loadAnimation(this, R.anim.bounce_intro);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (simple) {
                    loadingImage.startAnimation(anim);
                }
                else {
                    if (list != null && list.size() > 0) {
                        adapter.notifyDataSetChanged();

//                if (playlistFragment != null)
//                    playlistFragment.refreshList(list);
                        if (playlistFragment != null)
                            playlistFragment.refreshList(list);

                        if (player != null) {
                            player.loadVideo(list.get(0).getVideoId());
                            player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
                        }

                        if (draggablePanel.getVisibility() == View.GONE)
                            draggablePanel.setVisibility(View.VISIBLE);
                        else
                            draggablePanel.maximize();
                        loadingLayout.setVisibility(View.GONE);
                    } else
                        loadingImage.startAnimation(anim);
                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        loadingImage.startAnimation(anim);
    }

    private void hookDraggablePanelListeners() {
        draggablePanel.setDraggableListener(new DraggableListener() {
            @Override
            public void onMaximized() {
                playVideo();
            }

            @Override
            public void onMinimized() {
                //Empty
            }

            @Override
            public void onClosedToLeft() {
                pauseVideo();
            }

            @Override
            public void onClosedToRight() {
                pauseVideo();
            }
        });
    }

    /**
     * Pause the video reproduced in the YouTubePlayer.
     */
    private void pauseVideo() {
        if (player.isPlaying()) {
            player.pause();
        }
    }

    /**
     * Resume the video reproduced in the YouTubePlayer.
     */
    private void playVideo() {
        if (!player.isPlaying()) {
            player.play();
        }
    }

    class AddChannelThumbnail extends AsyncTask<ChannelItem, Void, ChannelItem> {
        @Override
        protected ChannelItem doInBackground(ChannelItem... items) {
            ChannelItem item = items[0];

            Bitmap bmp = ImageUtil.getImageFromURLNonTask(item.getChannelThumbnailDUrl());
            if (bmp != null)
                item.setChannelThumbnail(ImageUtil.getImageBytes(bmp));

            return item;
        }

        @Override
        protected void onPostExecute(final ChannelItem item) {
            if (adapter != null) {
                adapter.updateThumbnail(item);
            }

        }
    }
}
