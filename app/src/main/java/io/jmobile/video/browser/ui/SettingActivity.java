package io.jmobile.video.browser.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.base.BaseDialogFragment;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.ui.dialog.TimerDialog;

public class SettingActivity extends BaseActivity {

    ImageButton backButton;

    LinearLayout timerLayout, moreLayout, requestLayout;
    TextView timerText, versionText;

    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogUtil.DEBUG_MODE)
                Toast.makeText(SettingActivity.this, "SettingActivity Times up!!!", Toast.LENGTH_SHORT).show();
            if (timerText != null)
                timerText.setText("");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initView();
        setTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        timerLayout = fv(R.id.layout_setting_timer);
        timerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fdf(TimerDialog.TAG) == null) {
                    TimerDialog dlg = TimerDialog.newInstance(TimerDialog.TAG, SettingActivity.this);
                    dlg.setNegativeListener(new BaseDialogFragment.DialogNegativeListener() {
                        @Override
                        public void onDialogNegative(BaseDialogFragment dialog, String tag) {
                            setTimer();
                        }
                    });
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {

                            setTimer();

                            Calendar cal = Calendar.getInstance();
                            cal.set(Calendar.HOUR_OF_DAY, sp.getSettingTimerHour());
                            cal.set(Calendar.MINUTE, sp.getSettingTimerMinute());
                            cal.set(Calendar.SECOND, 0);

                            if (cal.getTimeInMillis() <= System.currentTimeMillis()) {
                                cal.add(Calendar.DATE, 1);
                            }

                            Util.onAlarm(SettingActivity.this, cal.getTimeInMillis());
                        }
                    });
                    sdf(dlg);
                }
            }
        });

        timerText = fv(R.id.text_setting_timer);
        versionText = fv(R.id.text_setting_version);
        versionName();

        moreLayout = fv(R.id.layout_setting_more);
        moreLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moreInfo();
            }
        });

        requestLayout = fv(R.id.layout_setting_request);
        requestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });
    }

    private void setTimer() {
        int hour = sp.getSettingTimerHour();
        int min = sp.getSettingTimerMinute();

        if (hour == -1 || min == -1)
            timerText.setText("");
        else {
            timerText.setText(Util.getTimerString(hour, min) + " 끄기");

        }
    }

    private void versionName() {
        String version;
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pi.versionName;
            versionText.setText(r.s(R.string.setting_version_prefix) + " " + version);
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    private void moreInfo() {
        try {
            Uri uri = Uri.parse("https://play.google.com/store/apps/dev?id=6800756349887572109");
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(i);
        } catch (Exception e) {
        }
    }

    private void sendFeedback() {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "dooyonce13@gmail.com"));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Suggest to HDV Browser"); //제목
            startActivity(intent);
        } catch (Exception e) {
        }
    }

}
