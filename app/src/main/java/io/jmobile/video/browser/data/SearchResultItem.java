package io.jmobile.video.browser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class SearchResultItem extends TableObject {
    public static final String TABLE_NAME = "history_youtube_video";

    public static final String ROW_ID = "_id";
    public static final String HISTORY_VIDEO_ID = "history_video_id";
    public static final String HISTORY_INDEX = "history_index";
    public static final String HISTORY_KIND = "history_kind";
    public static final String HISTORY_ETAG = "history_etag";
    public static final String HISTORY_CHANNEL_ID = "history_channel_id";
    public static final String HISTORY_TITLE = "history_title";
    public static final String HISTORY_DESCRIPTION = "history_description";
    public static final String HISTORY_PUBLISHED_AT = "history_published_at";
    public static final String HISTORY_THUMBNAIL_DEFAULT = "history_thumbnail_default";
    public static final String HISTORY_THUMBNAIL_MEDIUM = "history_thumbnail_medium";
    public static final String HISTORY_THUMBNAIL_HIGH = "history_thumbnail_high";
    public static final String HISTORY_CHANNEL_TITLE = "history_channel_title";
    public static final String HISTORY_AT = "history_at";

    public static final Creator<SearchResultItem> CREATOR = new Creator<SearchResultItem>() {
        @Override
        public SearchResultItem createFromParcel(Parcel source) {
            return new SearchResultItem(source);
        }

        @Override
        public SearchResultItem[] newArray(int size) {
            return new SearchResultItem[size];
        }
    };

    private long id;
    private int index;
    private String videoId;
    private String kind;
    private String etag;
    private String channelId;
    private String title;
    private String description;
    private String publishedAt;
    private String thumbnailDefaultUrl;
    private String thumbnailMediumUrl;
    private String thumbnailHighUrl;
    private String channelTitle;
    private long historyAt;

    public SearchResultItem() {
        super();
    }

    public SearchResultItem(JSONObject o) {
        this.kind = Util.optString(o, Common.TAG_KIND);
        this.etag = Util.optString(o, Common.TAG_ETAG);
        try {
            JSONObject ob = new JSONObject(Util.optString(o, Common.TAG_ID));
            videoId = Util.optString(ob, Common.TAG_VIDEO_ID);
            ob = new JSONObject(Util.optString(o, Common.TAG_SNIPPET));
            publishedAt = Util.optString(ob, Common.TAG_PUBLISHED_AT);
            channelId = Util.optString(ob, Common.TAG_CHANNEL_ID);
            title = Util.optString(ob, Common.TAG_TITLE);
            description = Util.optString(ob, Common.TAG_DESCRIPTION);
            this.channelTitle = Util.optString(ob, Common.TAG_CHANNEL_TITLE);
            JSONObject tob = new JSONObject(Util.optString(ob, Common.TAG_THUMBNAILS));
            JSONObject uob = new JSONObject(Util.optString(tob, Common.TAG_DEFAULT));
            this.thumbnailDefaultUrl = Util.optString(uob, Common.TAG_URL);
            uob = new JSONObject(Util.optString(tob, Common.TAG_MEDIUM));
            this.thumbnailMediumUrl = Util.optString(uob, Common.TAG_URL);
            uob = new JSONObject(Util.optString(tob, Common.TAG_HIGH));
            this.thumbnailHighUrl = Util.optString(uob, Common.TAG_URL);
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    public SearchResultItem(Parcel in) {
        super(in);

        this.id = in.readLong();
        this.index = in.readInt();
        this.kind = in.readString();
        this.etag = in.readString();
        this.videoId = in.readString();
        this.channelId = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.publishedAt = in.readString();
        this.channelTitle = in.readString();
        this.thumbnailDefaultUrl = in.readString();
        this.thumbnailMediumUrl = in.readString();
        this.thumbnailHighUrl = in.readString();
        this.historyAt = in.readLong();
    }

    public static void createTableHistoryYoutubeVideoItems(SQLiteDatabase db) {
        db.execSQL(new StringBuilder()
                .append("create table " + TABLE_NAME + " (")
                .append(ROW_ID + " integer primary key autoincrement, ")
                .append(HISTORY_VIDEO_ID + " text not null unique, ")
                .append(HISTORY_INDEX + " integer, ")
                .append(HISTORY_KIND + " text, ")
                .append(HISTORY_ETAG + " text, ")
                .append(HISTORY_CHANNEL_ID + " text, ")
                .append(HISTORY_TITLE + " text, ")
                .append(HISTORY_DESCRIPTION + " text, ")
                .append(HISTORY_PUBLISHED_AT + " text, ")
                .append(HISTORY_THUMBNAIL_DEFAULT + " text, ")
                .append(HISTORY_THUMBNAIL_MEDIUM + " text, ")
                .append(HISTORY_THUMBNAIL_HIGH + " text, ")
                .append(HISTORY_CHANNEL_TITLE + " text, ")
                .append(HISTORY_AT + " integer not null) ")
                .toString());
    }

    public static SearchResultItem getHistoryYoutubeVideoItem(SQLiteDatabase db, String id) {
        List<SearchResultItem> list = getHistoryYoutubeVideoItems(db, null, HISTORY_VIDEO_ID + " = ? ", new String[]{id}, null, null, null, "1");

        return list.size() <= 0 ? null : list.get(0);
    }

    public static List<SearchResultItem> getHistoryYoutubeVideoItems(SQLiteDatabase db, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) {
        List<SearchResultItem> list = new ArrayList<>();

        try {
            Cursor c = db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
            c.moveToFirst();

            while (!c.isAfterLast()) {
                SearchResultItem item = new SearchResultItem()
                        .setRowId(c)
                        .setVideoId(c)
                        .setIndex(c)
                        .setKind(c)
                        .setEtag(c)
                        .setChannelId(c)
                        .setTitle(c)
                        .setDescription(c)
                        .setPublishedAt(c)
                        .setThumbnailDefaultUrl(c)
                        .setThumbnailMediumUrl(c)
                        .setThumbnailHighUrl(c)
                        .setChannelTitle(c)
                        .setHistoryAt(c);

                list.add(item);
                c.moveToNext();
            }
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    public static boolean insertOrUpdateHistoryYoutubeVideoItem(SQLiteDatabase db, SearchResultItem item) {
        try {
            ContentValues v = new ContentValues();
            item.putVideoId(v)
                    .putIndex(v)
                    .putKind(v)
                    .putEtag(v)
                    .putChannelId(v)
                    .putTitle(v)
                    .putDescription(v)
                    .putPublishedAt(v)
                    .putThumbnailDefaultUrl(v)
                    .putThumbnailMediumUrl(v)
                    .putThumbnailHighUrl(v)
                    .putChannelTitle(v)
                    .putHistoryAt(v);

            int rowAffected = db.update(TABLE_NAME, v, HISTORY_VIDEO_ID + " =? ", new String[]{item.getVideoId()});
            if (rowAffected == 0)
                db.insert(TABLE_NAME, null, v);

            return true;
        } catch (Exception e) {
            LogUtil.log("DB Insert Error !!!! " + item.getTitle());
            LogUtil.log(e.getMessage());
        }

        return false;
    }

    public static boolean deleteAllHistoryVideoItems(SQLiteDatabase db) {
        try {
            db.delete(TABLE_NAME, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeLong(id);
        out.writeInt(index);
        out.writeString(kind);
        out.writeString(etag);
        out.writeString(videoId);
        out.writeString(channelId);
        out.writeString(title);
        out.writeString(description);
        out.writeString(publishedAt);
        out.writeString(channelTitle);
        out.writeString(thumbnailDefaultUrl);
        out.writeString(thumbnailMediumUrl);
        out.writeString(thumbnailHighUrl);
        out.writeLong(historyAt);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SearchResultItem && index == ((SearchResultItem) obj).index;
    }

    public long getRowId() {
        return this.id;
    }

    public SearchResultItem setRowId(long id) {
        this.id = id;
        return this;
    }

    public SearchResultItem setRowId(Cursor c) {
        this.id = l(c, ROW_ID);

        return this;
    }

    public SearchResultItem putRowId(ContentValues v) {
        v.put(ROW_ID, id);

        return this;
    }

    public int getIndex() {
        return this.index;
    }

    public SearchResultItem setIndex(int index) {
        this.index = index;

        return this;
    }

    public SearchResultItem setIndex(Cursor c) {
        this.index = i(c, HISTORY_INDEX);

        return this;
    }

    public SearchResultItem putIndex(ContentValues v) {
        v.put(HISTORY_INDEX, index);

        return this;
    }

    public String getKind() {
        return this.kind;
    }

    public SearchResultItem setKind(String kind) {
        this.kind = kind;

        return this;
    }

    public SearchResultItem setKind(Cursor c) {
        this.kind = s(c, HISTORY_KIND);

        return this;
    }

    public SearchResultItem putKind(ContentValues v) {
        v.put(HISTORY_KIND, kind);

        return this;
    }

    public String getEtag() {
        return this.etag;
    }

    public SearchResultItem setEtag(String etag) {
        this.etag = etag;

        return this;
    }

    public SearchResultItem setEtag(Cursor c) {
        this.etag = s(c, HISTORY_ETAG);

        return this;
    }

    public SearchResultItem putEtag(ContentValues v) {
        v.put(HISTORY_ETAG, etag);

        return this;
    }

    public String getVideoId() {
        return this.videoId;
    }

    public SearchResultItem setVideoId(String videoId) {
        this.videoId = videoId;

        return this;
    }

    public SearchResultItem setVideoId(Cursor c) {
        this.videoId = s(c, HISTORY_VIDEO_ID);

        return this;
    }

    public SearchResultItem putVideoId(ContentValues v) {
        v.put(HISTORY_VIDEO_ID, videoId);

        return this;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public SearchResultItem setChannelId(String channelId) {
        this.channelId = channelId;

        return this;
    }

    public SearchResultItem setChannelId(Cursor c) {
        this.channelId = s(c, HISTORY_CHANNEL_ID);

        return this;
    }

    public SearchResultItem putChannelId(ContentValues v) {
        v.put(HISTORY_CHANNEL_ID, channelId);

        return this;
    }

    public String getTitle() {
        return this.title;
    }

    public SearchResultItem setTitle(String title) {
        this.title = title;

        return this;
    }

    public SearchResultItem setTitle(Cursor c) {
        this.title = s(c, HISTORY_TITLE);

        return this;
    }

    public SearchResultItem putTitle(ContentValues v) {
        v.put(HISTORY_TITLE, title);

        return this;
    }

    public String getDescription() {
        return this.description;
    }

    public SearchResultItem setDescription(String description) {
        this.description = description;

        return this;
    }

    public SearchResultItem setDescription(Cursor c) {
        this.description = s(c, HISTORY_DESCRIPTION);

        return this;
    }

    public SearchResultItem putDescription(ContentValues v) {
        v.put(HISTORY_DESCRIPTION, description);

        return this;
    }

    public String getPublishedAt() {
        return this.publishedAt;
    }

    public SearchResultItem setPublishedAt(String at) {
        this.publishedAt = at;

        return this;
    }

    public SearchResultItem setPublishedAt(Cursor c) {
        publishedAt = s(c, HISTORY_PUBLISHED_AT);

        return this;
    }

    public SearchResultItem putPublishedAt(ContentValues v) {
        v.put(HISTORY_PUBLISHED_AT, publishedAt);

        return this;
    }

    public String getChannelTitle() {
        return this.channelTitle;
    }

    public SearchResultItem setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;

        return this;
    }

    public SearchResultItem setChannelTitle(Cursor c) {
        this.channelTitle = s(c, HISTORY_CHANNEL_TITLE);

        return this;
    }

    public SearchResultItem putChannelTitle(ContentValues v) {
        v.put(HISTORY_CHANNEL_TITLE, channelTitle);

        return this;
    }

    public String getThumbnailDefaultUrl() {
        return this.thumbnailDefaultUrl;
    }

    public SearchResultItem setThumbnailDefaultUrl(String url) {
        this.thumbnailDefaultUrl = url;

        return this;
    }

    public SearchResultItem setThumbnailDefaultUrl(Cursor c) {
        this.thumbnailDefaultUrl = s(c, HISTORY_THUMBNAIL_DEFAULT);

        return this;
    }

    public SearchResultItem putThumbnailDefaultUrl(ContentValues v) {
        v.put(HISTORY_THUMBNAIL_DEFAULT, thumbnailDefaultUrl);

        return this;
    }

    public String getThumbnailMediumUrl() {
        return this.thumbnailMediumUrl;
    }

    public SearchResultItem setThumbnailMediumUrl(String url) {
        this.thumbnailMediumUrl = url;

        return this;
    }

    public SearchResultItem setThumbnailMediumUrl(Cursor c) {
        this.thumbnailMediumUrl = s(c, HISTORY_THUMBNAIL_MEDIUM);

        return this;
    }

    public SearchResultItem putThumbnailMediumUrl(ContentValues v) {
        v.put(HISTORY_THUMBNAIL_MEDIUM, thumbnailMediumUrl);

        return this;
    }

    public String getThumbnailHighUrl() {
        return this.thumbnailHighUrl;
    }

    public SearchResultItem setThumbnailHighUrl(String url) {
        this.thumbnailHighUrl = url;
        return this;
    }

    public SearchResultItem setThumbnailHighUrl(Cursor c) {
        this.thumbnailHighUrl = s(c, HISTORY_THUMBNAIL_HIGH);

        return this;
    }

    public SearchResultItem putThumbnailHighUrl(ContentValues v) {
        v.put(HISTORY_THUMBNAIL_HIGH, thumbnailHighUrl);

        return this;
    }

    public long getHistoryAt() {
        return this.historyAt;
    }

    public SearchResultItem setHistoryAt(long at) {
        this.historyAt = at;

        return this;
    }

    public SearchResultItem setHistoryAt(Cursor c) {
        this.historyAt = l(c, HISTORY_AT);

        return this;
    }

    public SearchResultItem putHistoryAt(ContentValues v) {
        v.put(HISTORY_AT, this.historyAt);

        return this;
    }
}
