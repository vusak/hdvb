package io.jmobile.video.browser.data;

import android.os.Parcel;

import org.json.JSONObject;

import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;

public class PlaylistItem extends TableObject {
    public static final Creator<PlaylistItem> CREATOR = new Creator<PlaylistItem>() {
        @Override
        public PlaylistItem createFromParcel(Parcel source) {
            return new PlaylistItem(source);
        }

        @Override
        public PlaylistItem[] newArray(int size) {
            return new PlaylistItem[size];
        }
    };
    private int index;
    private String kind;
    private String id;
    private String etag;
    private String channelId;
    private String title;

    public PlaylistItem() {
        super();
    }

    public PlaylistItem(JSONObject o) {
        this.id = Util.optString(o, Common.TAG_ID);
        this.etag = Util.optString(o, Common.TAG_ETAG);
        this.kind = Util.optString(o, Common.TAG_KIND);
        try {
            JSONObject ob = new JSONObject(Util.optString(o, Common.TAG_SNIPPET));
            this.channelId = Util.optString(ob, Common.TAG_CHANNEL_ID);
            this.title = Util.optString(ob, Common.TAG_TITLE);
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    public PlaylistItem(Parcel in) {
        super(in);

        this.index = in.readInt();
        this.kind = in.readString();
        this.id = in.readString();
        this.etag = in.readString();
        this.channelId = in.readString();
        this.title = in.readString();
    }


    @Override
    public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
        out.writeInt(index);
        out.writeString(kind);
        out.writeString(id);
        out.writeString(etag);
        out.writeString(channelId);
        out.writeString(title);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PlaylistItem && index == ((PlaylistItem) obj).index;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKind() {
        return this.kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtag() {
        return this.etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getChannelId() {
        return this.channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
