package io.jmobile.video.browser.network;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.jmobile.video.browser.BaseApplication;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.DBController;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.SPController;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.CategoryItem;
import io.jmobile.video.browser.data.ChannelItem;
import io.jmobile.video.browser.data.SearchResultItem;
import io.jmobile.video.browser.data.VideoCategoryItem;
import io.jmobile.video.browser.data.VideoItem;

public class Api extends Handler {

    private static RequestQueue requestQueue;
    private static Queue<Message> messageQueue = new LinkedList<Message>();

    private WeakReference<ApiListener> ref;
    private BaseApplication app;
    private SPController sp;
    private DBController db;
    private BaseApplication.ResourceWrapper r;

    public Api(BaseApplication application, ApiListener target) {
        app = application;
        ref = new WeakReference<ApiListener>(target);
        sp = application.getSPController();
        db = application.getDBController();
        r = application.getResourceWrapper();

        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(app.getApplicationContext());

    }

    public static void cancelApprequest() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public void setTarget(ApiListener target) {
        if (ref != null)
            ref.clear();

        ref = new WeakReference<ApiListener>(target);
    }

    public void getCategoryList() {
        Message m = newMessage(Common.API_CODE_GET_CATEGORY, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_TYPE, Common.TAG_CHK);
            params.put(Common.PARAM_VER, sp.getCheckCategoryUpdateVersion());
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_GET_CATEGORY, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                if (res) {
                    String version = j_result.optString(Common.PARAM_VER);
                    if (sp.getCheckCategoryUpdateVersion().equalsIgnoreCase(version))
                        res = false;
                    else {
                        sp.setCheckCategoryUpdateVersion(version);
                        db.deleteAllCategoryItem();
                        try {
                            JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);

                            for (int i = 0; i < arr.length(); i++) {
                                JSONObject json_c = arr.getJSONObject(i);
                                CategoryItem item = new CategoryItem(json_c);

                                if (item != null)
                                    db.insertOrUpdateCategoryItem(item);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                m.getData().putBoolean(Common.ARG_CATEGORY_UPDATE, res);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getChannelList(final String categoryId, final boolean isLive) {
        Message m = newMessage(Common.API_CODE_GET_CHANNEL, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_TYPE, Common.TAG_CHK);
            params.put(Common.PARAM_C_IDX, categoryId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_GET_CHANNEL, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                boolean res = j_result.optBoolean(Common.TAG_RESULT);
                ArrayList<ChannelItem> items = new ArrayList<ChannelItem>();
                if (res) {
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_LIST);
                        ArrayList<VideoCategoryItem> list = new ArrayList<>();
                        ArrayList<VideoCategoryItem> dbList = new ArrayList<>();
                        dbList.addAll(db.getVideoCategoryItems());
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);

                            if (isLive) {
                                VideoCategoryItem vItem = new VideoCategoryItem(db, json_c);

                                if (vItem != null) {
                                    list.add(vItem);
                                    db.insertOrUpdateVideoCategoryItem(vItem);
                                }
                            }
                            else {
                                ChannelItem item = new ChannelItem(json_c, categoryId);

                                if (item != null)
                                    items.add(item);
                            }
                        }

                        if (isLive) {
                            for (int i = 0; i < dbList.size(); i++) {
                                boolean delete = true;
                                for (VideoCategoryItem temp : list) {
                                    if (temp.getVideoCategoryId().equals(dbList.get(i).getVideoCategoryId())) {
                                        delete = false;
                                        break;
                                    }
                                }
                                if (delete)
                                    db.deleteVideoCategoryItem(dbList.get(i));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                m.getData().putBoolean(Common.ARG_RESULT, res);
                m.getData().putBoolean(Common.ARG_IS_LIVE, isLive);
                if (!isLive)
                    m.getData().putParcelableArrayList(Common.ARG_CHANNEL_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void sendReport(String email, String name, String message) {
        Message m = newMessage(Common.API_CODE_SEND_REPORT, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_TYPE, "add");
            params.put(Common.PARAM_M_IDX, "3");
            params.put(Common.PARAM_M_NAME, name);
            params.put(Common.PARAM_M_EMAIL, email);
            params.put(Common.PARAM_COMMENT, message);
        } catch (Exception e) {
            e.printStackTrace();
        }

        new ApiRequest(this, m, Request.Method.POST, Common.BASE_URL + Common.API_URL_SEND_REPORT, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                m.getData().putBoolean(Common.ARG_RESULT, j_result.optBoolean(Common.TAG_RESULT));
                m.getData().putString(Common.ARG_MESSAGE, j_result.optString(Common.TAG_MESSAGE));
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    // youtube.videoCategoryries.list
    public void getVideoCategoryList() {
        Message m = newMessage(Common.YOUTUBE_API_CODE_VIDEO_CATEGORIES, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_KEY, Common.YOUTUBE_API_KEY);
            params.put(Common.PARAM_PART, "snippet");
            params.put(Common.PARAM_REGION_CODE, "KR");
        } catch (Exception e) {
            e.printStackTrace();
        }
        new ApiRequest(this, m, Request.Method.GET, Common.YOUTUBE_BASE_URL + Common.YOUTUBE_URL_VIDEO_CATEGORIES, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                ArrayList<VideoCategoryItem> items = new ArrayList<VideoCategoryItem>();
                try {
                    JSONArray arr = j_result.getJSONArray(Common.TAG_ITEMS);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        VideoCategoryItem item = new VideoCategoryItem(db, json_c);

                        if (item != null) {
                            db.insertOrUpdateVideoCategoryItem(item);
                            items.add(item);
                        }
                    }
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }

                m.getData().putParcelableArrayList(Common.ARG_CATEGORY_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    public void getVideoList(String id, String pageToken) {
        Message m = newMessage(Common.YOUTUBE_API_CODE_VIDEOS, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_KEY, Common.YOUTUBE_API_KEY);
            params.put(Common.PARAM_PART, "snippet");
            params.put(Common.PARAM_CHART, "mostPopular");
            if (!pageToken.isEmpty())
                params.put(Common.PARAM_PAGE_TOKEN, pageToken);
            params.put(Common.PARAM_MAX_RESULTS, "50");
            params.put(Common.PARAM_REGION_CODE, "KR");
            params.put(Common.PARAM_VIDEO_CATEGORY_ID, id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        new ApiRequest(this, m, Request.Method.GET, Common.YOUTUBE_BASE_URL + Common.YOUTUBE_URL_VIDEOS, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                String token = "";
                ArrayList<VideoItem> items = new ArrayList<VideoItem>();
//                String code = Util.optString(j_result, "code");
//                if (code.equals("200")) {
                token = Util.optString(j_result, Common.TAG_NEXT_PAGE_TOKEN);

                try {
                    JSONArray arr = j_result.getJSONArray(Common.TAG_ITEMS);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        VideoItem item = new VideoItem(json_c);

                        if (item != null)
                            items.add(item);
                    }
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }
//                }
                m.getData().putString(Common.TAG_NEXT_PAGE_TOKEN, token);
                m.getData().putParcelableArrayList(Common.ARG_VIDEO_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    // youtube.search.list
    public void getSearchChannelVideos(final String channelId, String pageToken) {
        Message m = newMessage(Common.YOUTUBE_API_CODE_SEARCH_CHANNEL, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_KEY, Common.YOUTUBE_API_KEY);
            params.put(Common.PARAM_PART, "snippet");
            params.put(Common.PARAM_CHANNEL_ID, channelId);
            if (!TextUtils.isEmpty(pageToken))
                params.put(Common.PARAM_PAGE_TOKEN, pageToken);
            params.put(Common.PARAM_ORDER, "date");
            params.put(Common.PARAM_MAX_RESULTS, "50");
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }

        new ApiRequest(this, m, Request.Method.GET, Common.YOUTUBE_BASE_URL + Common.YOUTUBE_URL_SEARCH, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                String token = "";
                ArrayList<SearchResultItem> items = new ArrayList<SearchResultItem>();
                token = Util.optString(j_result, Common.TAG_NEXT_PAGE_TOKEN);
                try {
                    JSONArray arr = j_result.getJSONArray(Common.TAG_ITEMS);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        SearchResultItem item = new SearchResultItem(json_c);

                        if (item != null && !TextUtils.isEmpty(item.getVideoId()))
                            items.add(item);
                    }
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }

                m.getData().putString(Common.TAG_NEXT_PAGE_TOKEN, token);
                m.getData().putParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    // youtube.search.list
    public void getSearchLiveVideos(final String videoCategoryId, final int type) {
        Message m = newMessage(Common.YOUTUBE_API_CODE_SEARCH_LIVE, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_KEY, Common.YOUTUBE_API_KEY);
            params.put(Common.PARAM_PART, "snippet");
            params.put(Common.PARAM_EVENT_TYPE, "live");
            params.put(Common.PARAM_ORDER, "viewCount");
            params.put(Common.PARAM_REGION_CODE, Locale.getDefault().getCountry());
            params.put(Common.PARAM_TYPE, "video");
            params.put(Common.PARAM_VIDEO_CATEGORY_ID, videoCategoryId);
            params.put(Common.PARAM_MAX_RESULTS, type == Common.RESULT_TYPE_ITEM ? "1" : "50");
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }

        new ApiRequest(this, m, Request.Method.GET, Common.YOUTUBE_BASE_URL + Common.YOUTUBE_URL_SEARCH, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                String token = "";
                if (type == Common.RESULT_TYPE_LIST) {
                    ArrayList<SearchResultItem> items = new ArrayList<SearchResultItem>();
                    try {
                        JSONArray arr = j_result.getJSONArray(Common.TAG_ITEMS);
                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject json_c = arr.getJSONObject(i);
                            SearchResultItem item = new SearchResultItem(json_c);

                            if (item != null)
                                items.add(item);
                        }
                    } catch (Exception e) {
                        LogUtil.log(e.getMessage());
                    }

                    m.getData().putString(Common.ARG_SEARCH_LIVE_CATEGORY_ID, videoCategoryId);
                    m.getData().putParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST, items);
                } else if (type == Common.RESULT_TYPE_ITEM) {
                    try {
                        VideoCategoryItem item = db.getVideoCategoryItem(videoCategoryId);
                        if (item == null)
                            return;
                        JSONArray arr = j_result.getJSONArray(Common.TAG_ITEMS);
                        JSONObject json_c = arr.getJSONObject(0);

                        JSONObject ob = new JSONObject(Util.optString(json_c, Common.TAG_ID));
                        String videoId = Util.optString(ob, Common.TAG_VIDEO_ID);
                        ob = new JSONObject(Util.optString(json_c, Common.TAG_SNIPPET));
                        String title = Util.optString(ob, Common.TAG_TITLE);
                        String description = Util.optString(ob, Common.TAG_DESCRIPTION);
                        JSONObject tob = new JSONObject(Util.optString(ob, Common.TAG_THUMBNAILS));
                        JSONObject uob = new JSONObject(Util.optString(tob, Common.TAG_DEFAULT));
                        String urlD = Util.optString(uob, Common.TAG_URL);
                        uob = new JSONObject(Util.optString(tob, Common.TAG_MEDIUM));
                        String urlM = Util.optString(uob, Common.TAG_URL);
                        uob = new JSONObject(Util.optString(tob, Common.TAG_HIGH));
                        String urlH = Util.optString(uob, Common.TAG_URL);
                        if (json_c != null) {
                            item.setVideoCategoryLiveTitle(title);
                            item.setVideoCategoryLiveVideoId(videoId);
                            item.setVideoCategoryLiveDescription(description);
                            item.setVideoCategoryLiveUrlDefault(urlD);
                            item.setVideoCategoryLiveUrlMedium(urlM);
                            item.setVideoCategoryLiveUrlHigh(urlH);

                            db.insertOrUpdateVideoCategoryItem(item);
                        }
                    } catch (Exception e) {
                        LogUtil.log(e.getMessage());
                    }
                }
                m.getData().putString(Common.ARG_SEARCH_LIVE_CATEGORY_ID, videoCategoryId);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    // youtube.search.list
    public void getSearchResultList(String key, String pageToken) {
        Message m = newMessage(Common.YOUTUBE_API_CODE_SEARCH, null, false);

        Map<String, Object> params = new HashMap<String, Object>();
        try {
            params.put(Common.PARAM_KEY, Common.YOUTUBE_API_KEY);
            params.put(Common.PARAM_PART, "snippet");
            params.put(Common.PARAM_Q, key);
            if (!TextUtils.isEmpty(pageToken))
                params.put(Common.PARAM_PAGE_TOKEN, pageToken);
            params.put(Common.PARAM_MAX_RESULTS, "50");
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }

        new ApiRequest(this, m, Request.Method.GET, Common.YOUTUBE_BASE_URL + Common.YOUTUBE_URL_SEARCH, params, false) {
            @Override
            public void onSuccess(JSONObject j_result) {
                String token = "";
                ArrayList<SearchResultItem> items = new ArrayList<SearchResultItem>();
                token = Util.optString(j_result, Common.TAG_NEXT_PAGE_TOKEN);

                try {
                    JSONArray arr = j_result.getJSONArray(Common.TAG_ITEMS);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject json_c = arr.getJSONObject(i);
                        SearchResultItem item = new SearchResultItem(json_c);

                        if (item != null)
                            items.add(item);
                    }
                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }
//                }
                m.getData().putString(Common.TAG_NEXT_PAGE_TOKEN, token);
                m.getData().putParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST, items);
            }

            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError() {

            }
        }.ajax();
    }

    private Message newMessage(int apiCode, String progressMessage, boolean showErrorMessage) {
        Message m = Message.obtain(this, apiCode);
        Bundle b = new Bundle();
        b.putString(Common.PARAM_PROGRESS_MESSAGE, progressMessage);
        b.putBoolean(Common.PARAM_SHOW_ERROR_MESSAGE, showErrorMessage);
        m.setData(b);

        return m;
    }

    @Override
    public void handleMessage(Message msg) {
        if (ref == null || ref.get() == null)
            return;

        ref.get().handleApiMessage(msg);
    }

    public static interface ApiListener {
        public void handleApiMessage(Message m);

        public BaseActivity getApiActivity();

        public FragmentManager getApiFragmentManager();
    }

    private abstract static class ApiRequest implements Response.Listener<String>, Response.ErrorListener {
        protected Api api;
        protected Message m;
        protected boolean addAuthorization;
        protected StringRequest request;
        protected String paramString;

        public ApiRequest(Api _api, Message _m, int method, String _url, Map<String, Object> params, final boolean addAuthorizationToken) {
            this.api = _api;
            this.m = _m;
            this.addAuthorization = addAuthorizationToken;

            JSONObject j_params = null;
            if (method == Request.Method.GET) {
                if (params != null && params.size() > 0) {
                    Object[] temp = new Object[params.size()];
                    Set<String> keySet = params.keySet();
                    int i = 0;
                    for (String key : keySet) {
                        try {
                            temp[i] = key + "=" + URLEncoder.encode(String.valueOf(params.get(key)), "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            temp[i] = key + "=" + String.valueOf(params.get(key));
                        }
                        i++;
                    }

                    _url += "?" + TextUtils.join("&", temp);
                }
            } else if (params != null) {
                j_params = new JSONObject(params);
                paramString = j_params.toString();
            }

            request = new StringRequest(method, _url, this, this) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = super.getHeaders();
                    if (headers == null || headers.equals(Collections.emptyMap()))
                        headers = new Hashtable<String, String>();

                    headers.put(Common.HEADER_CACHE_CONTROL, Common.HEADER_NO_CACHE);
                    headers.put(Common.HEADER_APPLICATION_VERSION, api.app.getVersion());
                    headers.put(Common.HEADER_OS, api.app.getOS());
//                    headers.put(Common.HEADER_DEVICE_IDENTIFIER, api.app.getDeviceIdentifier());
//                    if (addAuthorizationToken)
//                        headers.put(Common.HEADER_ACCESS_TOKEN, api.sp.getAccessToken());

                    if (LogUtil.DEBUG_MODE) {
                        StringBuilder sb = new StringBuilder();
                        for (String key : headers.keySet()) {
                            sb.append(key)
                                    .append(": ")
                                    .append(headers.get(key))
                                    .append("\n");
                        }
                        LogUtil.log("API_REQUEST", "HEADER===\n" + sb.toString());
                    }
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    if (TextUtils.isEmpty(paramString))
                        return super.getParams();
                    else {
                        Map<String, String> params = new HashMap<>();
                        params.put("param", paramString);
                        return params;
                    }
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(Common.TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            if (LogUtil.DEBUG_MODE) {
                LogUtil.log("API_REQUEST", "URL : " + request.getUrl());
                try {
                    if (request.getBody() != null)
                        LogUtil.log("API_REQUEST", "BODY : " + new String(request.getBody()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        abstract public void onSuccess(String result);

        abstract public void onSuccess(JSONObject j_result);

        abstract public void onError();

        @Override
        public void onResponse(String response) {
            if (response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }

//            try {
//                response = new String(response.getBytes("8859_1"), "utf-8");
//                LogUtil.log("UTF-8 " + response);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            if (LogUtil.DEBUG_MODE && response != null)
                LogUtil.log("API_REQUEST", "RESPONSE : " + response.toString());

//            if (m.what == Common.API_CODE_GET_TREND_LIST_WORLDWIDE
//                    || m.what == Common.API_CODE_GET_TREND_LEARNMORE
//                    || m.what == Common.API_CODE_GET_REPORT_LEARNMORE) {
//                onReceiveResponse(null, response, null);
//            } else {
            JSONObject j_response = null;
            try {
                j_response = new JSONObject(response);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (j_response == null) {
                m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;
                onReceiveResponse(null, null, "NO_RESPONSE");
                return;
            }
            onReceiveResponse(j_response, null, null);
//            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            m.arg1 = Common.RESULT_CODE_ERROR_NO_RESPONSE;

            if (error.networkResponse == null)
                m.arg2 = Common.RESULT_CODE_ERROR_UNKNOWN;
            else
                m.arg2 = error.networkResponse.statusCode;

            onReceiveResponse(null, null, String.valueOf(m.arg2));
        }

        public void ajax() {
            Api.requestQueue.add(request);
            synchronized (messageQueue) {
                messageQueue.add(m);
            }
        }

        private void onReceiveResponse(JSONObject j_result, String result, String errorCode) {
            synchronized (messageQueue) {
                messageQueue.remove(m);
            }

            new ResponseTask(this, j_result, result, errorCode).run();
        }
    }

    private static class ResponseTask extends AsyncTask<Void, Void, Void> {
        protected ApiRequest request;
        protected JSONObject j_result;
        protected String result;
        protected String errorCode;

        ResponseTask(ApiRequest request, JSONObject j_result, String result, String errorCode) {
            this.request = request;
            this.j_result = j_result;
            this.result = result;
            this.errorCode = errorCode;
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (j_result != null)
                request.onSuccess(j_result);
            else if (result != null)
                request.onSuccess(result);
            else
                request.onError();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            request.api.sendMessage(request.m);
        }

        public void run() {
            executeOnExecutor(THREAD_POOL_EXECUTOR);
        }
    }
}
