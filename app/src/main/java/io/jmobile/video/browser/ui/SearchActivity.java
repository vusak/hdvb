package io.jmobile.video.browser.ui;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.pedrovgs.DraggableListener;
import com.github.pedrovgs.DraggablePanel;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.adapter.SearchResultAdapter;
import io.jmobile.video.browser.base.BaseActivity;
import io.jmobile.video.browser.base.BaseDialogFragment;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.MessageDialog;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.SearchResultItem;
import io.jmobile.video.browser.network.Api;
import io.jmobile.video.browser.ui.fragment.PlaylistFragment;

public class SearchActivity extends BaseActivity implements View.OnClickListener, YouTubePlayer.PlayerStateChangeListener,
        YouTubePlayer.OnFullscreenListener,
        PlaylistFragment.PlaylistFragmentListener {

    ImageButton backButton, searchButton, refreshButton;
    EditText keyEditText;

    FrameLayout listLayout;
    LinearLayout emptyLayout, resultEmptyLayout, historyToplayout;

    ImageButton deleteButton;

    RecyclerView lv;
    LinearLayoutManager manager;
    ArrayList<SearchResultItem> list;
    SearchResultAdapter adapter;

    Api api;

    YouTubePlayer player;
    YouTubePlayerSupportFragment youTubePlayerSupportFragment;
    DraggablePanel draggablePanel;
    PlaylistFragment playlistFragment;

    boolean isFullScreen = false;
    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LogUtil.DEBUG_MODE)
                Toast.makeText(SearchActivity.this, "SearchActivity Times up!!!", Toast.LENGTH_SHORT).show();
            if (player != null && player.isPlaying()) {
                player.pause();

            }
        }
    };

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (api == null)
            api = new Api(app, this);
        else
            api.setTarget(this);

        initView();
    }

    @Override
    public void handleApiMessage(Message m) {
        if (m.what == Common.YOUTUBE_API_CODE_SEARCH) {
            ArrayList<SearchResultItem> temp = m.getData().getParcelableArrayList(Common.ARG_SEARCH_RESULT_LIST);
            if (temp != null && temp.size() > 0) {
                list.clear();
                list.addAll(temp);
                adapter.notifyDataSetChanged();

            }
            resultEmptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
            listLayout.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (draggablePanel != null && draggablePanel.getVisibility() == View.VISIBLE && draggablePanel.isMaximized())
            draggablePanel.minimize();
        else if (player != null && isFullScreen)
            player.setFullscreen(false);
        else
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(timeReceiver, new IntentFilter(Common.INTENTFILTER_BROADCAST_TIMER));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(timeReceiver);
    }

    @Override
    public void onVideoEnded() {
        String id = playlistFragment.getNextVideoId();
        if (!TextUtils.isEmpty(id))
            player.loadVideo(id);
    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onLoading() {
        if (player != null && !player.isPlaying())
            player.play();
    }

    @Override
    public void onLoaded(String s) {
        if (draggablePanel.getVisibility() == View.VISIBLE)
            player.play();

        if (playlistFragment != null)
            playlistFragment.loaded(s);
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onFullscreen(boolean b) {
        isFullScreen = b;
    }

    // PlaylistFragmentListener
    @Override
    public void onMiniButtonClick() {
        if (!draggablePanel.isMinimized())
            draggablePanel.minimize();
    }

    @Override
    public void onChangedPlaylist(ArrayList<String> playlist) {

    }

    @Override
    public void onChangedVideo(String id) {
        if (player != null) {
            player.loadVideo(id);
        }
    }

    @Override
    public boolean isPlaying() {
        return false;
    }


    @Override
    public void onClick(View v) {

    }

    private void initView() {
        backButton = fv(R.id.button_back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        searchButton = fv(R.id.button_search);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearch(keyEditText.getText().toString());
            }
        });


        keyEditText = fv(R.id.edit_keyword);
        keyEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    startSearch(keyEditText.getText().toString());
                }
                return false;
            }
        });

        emptyLayout = fv(R.id.layout_empty_history);
        resultEmptyLayout = fv(R.id.layout_empty_result);
        listLayout = fv(R.id.layout_list);
        historyToplayout = fv(R.id.layout_history_top);
        deleteButton = fv(R.id.button_delete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fdf(MessageDialog.TAG) == null) {
                    MessageDialog dlg = MessageDialog.newInstance(MessageDialog.TAG);
                    dlg.setMessage(r.s(R.string.message_delete_all_history));
                    dlg.setNegativeLabel(r.s(R.string.cancel));
                    dlg.setPositiveLabel(r.s(R.string.delete));
                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
                        @Override
                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
                            db.deleteAllHistoryVideoItems();
                            showHistory();
                        }
                    });
                    sdf(dlg);
                }

            }
        });

        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(this, 1);
        lv.setLayoutManager(manager);
        adapter = new SearchResultAdapter(this, R.layout.item_channel_search_list, list, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
//                Intent intent = YouTubeStandalonePlayer.createVideoIntent(SearchActivity.this, Common.YOUTUBE_API_KEY, ((SearchResultItem) item).getVideoId(), 0, true, true);
//                startActivity(intent);
                String id = ((SearchResultItem) item).getVideoId();
                ArrayList<SearchResultItem> temp = new ArrayList<>();
                temp.add((SearchResultItem) item);
                for (SearchResultItem sItem : list) {
                    if (!id.equalsIgnoreCase(sItem.getVideoId()))
                        temp.add(sItem);
                }

                if (playlistFragment != null)
                    playlistFragment.refreshList(temp);

                if (player != null)
                    player.loadVideo(id);

                if (draggablePanel.getVisibility() == View.GONE)
                    draggablePanel.setVisibility(View.VISIBLE);
                else
                    draggablePanel.maximize();
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        lv.setAdapter(adapter);

        draggablePanel = fv(R.id.draggable_panel);
        youTubePlayerSupportFragment = YouTubePlayerSupportFragment.newInstance();
        youTubePlayerSupportFragment.initialize(Common.YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                player = youTubePlayer;
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                player.setPlayerStateChangeListener(SearchActivity.this);
                player.setOnFullscreenListener(SearchActivity.this);
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

        draggablePanel.setFragmentManager(getSupportFragmentManager());
        draggablePanel.setTopFragment(youTubePlayerSupportFragment);
        playlistFragment = new PlaylistFragment();
        playlistFragment.setListener(this);
        draggablePanel.setBottomFragment(playlistFragment);
        draggablePanel.initializeView();
        draggablePanel.setVisibility(View.GONE);
        hookDraggablePanelListeners();

        showHistory();
    }

    private void showHistory() {
        list.clear();

        ArrayList<SearchResultItem> dbItems = new ArrayList<>();
        dbItems.addAll(db.getHistoryVideoItems());

        String today = Util.getDay2(System.currentTimeMillis());

        for (SearchResultItem item : dbItems) {
            if (today.equalsIgnoreCase(Util.getDay2(item.getHistoryAt()))) {
                list.add(item);
            }
        }
        adapter.notifyDataSetChanged();

        emptyLayout.setVisibility(list.size() > 0 ? View.GONE : View.VISIBLE);
        listLayout.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
        deleteButton.setVisibility(list.size() > 0 ? View.VISIBLE : View.GONE);
    }

    private void startSearch(String keyword) {
        deleteButton.setVisibility(View.GONE);
        emptyLayout.setVisibility(View.GONE);
        listLayout.setVisibility(View.GONE);
        historyToplayout.setVisibility(View.GONE);
        resultEmptyLayout.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(keyword))
            api.getSearchResultList(keyword, "");
        else
            resultEmptyLayout.setVisibility(View.VISIBLE);

        Util.hideKeyBoard(keyEditText);
    }

    private void hookDraggablePanelListeners() {
        draggablePanel.setDraggableListener(new DraggableListener() {
            @Override
            public void onMaximized() {
                playVideo();
            }

            @Override
            public void onMinimized() {
                //Empty
            }

            @Override
            public void onClosedToLeft() {
                pauseVideo();
            }

            @Override
            public void onClosedToRight() {
                pauseVideo();
            }
        });
    }

    /**
     * Pause the video reproduced in the YouTubePlayer.
     */
    private void pauseVideo() {
        if (player.isPlaying()) {
            player.pause();
        }
    }

    /**
     * Resume the video reproduced in the YouTubePlayer.
     */
    private void playVideo() {
        if (!player.isPlaying()) {
            player.play();
        }
    }

}
