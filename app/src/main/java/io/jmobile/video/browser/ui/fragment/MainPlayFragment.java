package io.jmobile.video.browser.ui.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.graphics.drawable.Animatable2Compat;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.adapter.LiveVideoAdapter;
import io.jmobile.video.browser.adapter.ReAdapter;
import io.jmobile.video.browser.adapter.SearchResultAdapter;
import io.jmobile.video.browser.adapter.VideoCategoryAdapter;
import io.jmobile.video.browser.base.BaseFragment;
import io.jmobile.video.browser.base.ExpandLayout;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.data.LiveVideoItem;
import io.jmobile.video.browser.data.SearchResultItem;
import io.jmobile.video.browser.data.VideoCategoryItem;
import io.jmobile.video.browser.ui.MainActivity;

public class MainPlayFragment extends BaseFragment implements YouTubePlayer.PlayerStateChangeListener, YouTubePlayer.OnFullscreenListener {

    RecyclerView lv;
    ArrayList<VideoCategoryItem> list;
    LinearLayoutManager manager;
    VideoCategoryAdapter adapter;
    YouTubePlayer player;

    YouTubePlayerSupportFragment youTubePlayerSupportFragment;
    FrameLayout youtubeLayout;
//    Api api;

    LinearLayout textLayout;
    TextView mainText, subText, listTitleText;
    ImageButton listOpenButton, listCloseButton;
    ExpandLayout listExpandLayout;
    TextView loadingText;
    RecyclerView listLv;
    ArrayList<LiveVideoItem> liveList;
    LinearLayoutManager listManager;
    LiveVideoAdapter listAdapter;

    VideoCategoryItem curItem;
    boolean isFullScreen = false;

    LinearLayout loadingLayout;
    ImageView loadingImage;
    AnimatedVectorDrawableCompat animatedVector;
    Animation anim;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_main_play;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).setOnkeyBackPressedListener(new MainActivity.onKeyBackPressedListener() {
            @Override
            public void onBack() {
                if (player != null && isFullScreen) {
                    player.setFullscreen(false);
                }
                else {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.setOnkeyBackPressedListener(null);
                    mainActivity.onBackPressed();
                }
            }
        });
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
//        if (api == null)
//            api = new Api(app, this);
//        else
//            api.setTarget(this);
//        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (player == null)
            return;

        if (!isVisibleToUser) {
            if (player.isPlaying())
                player.pause();
            player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_SYSTEM_UI);
        } else {
            player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE
                    | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
        }

        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onVideoEnded() {
    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onLoading() {
        if (player != null && !player.isPlaying())
            player.play();
    }

    @Override
    public void onLoaded(String s) {
        if (player != null && !player.isPlaying())
            player.play();
    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onFullscreen(boolean b) {
        isFullScreen = b;
    }

    private void
    initView() {
        youtubeLayout = fv(R.id.layout_youtube);

        youTubePlayerSupportFragment = YouTubePlayerSupportFragment.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.layout_youtube, youTubePlayerSupportFragment).commit();
        youTubePlayerSupportFragment.initialize(Common.YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                player = youTubePlayer;
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                player.setPlayerStateChangeListener(MainPlayFragment.this);
                player.setOnFullscreenListener(MainPlayFragment.this);
                player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE | YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
                if (list != null && list.size() > 0) {
                    startPlayLiveStream(list.get(sp.getLiveSelectedIndex()));
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

        textLayout = fv(R.id.layout_live);
        mainText = fv(R.id.text_live_main);
        subText = fv(R.id.text_live_sub);

        loadingText = fv(R.id.text_loading);
        lv = fv(R.id.lv);
        list = new ArrayList<>();
        manager = new GridLayoutManager(getActivity(), 1);
        lv.setLayoutManager(manager);
        adapter = new VideoCategoryAdapter(getActivity(), R.layout.item_video_category, list, new VideoCategoryAdapter.VideoCategorydAdapterListener() {
            @Override
            public void onMenuButtonClicked(final int position, VideoCategoryItem item) {
//                if (fdf(LiveListDialog.TAG) == null) {
//                    LiveListDialog dlg = LiveListDialog.newInstance(LiveListDialog.TAG, getActivity(), item);
//                    dlg.setPositiveListener(new BaseDialogFragment.DialogPositiveListener() {
//                        @Override
//                        public void onDialogPositive(BaseDialogFragment dialog, String tag) {
//                            refreshList();
//
//                            VideoCategoryItem categoryItem = list.get(position);
//                            sp.setLiveSelectedIndex(position);
//                            startPlayLiveStream(categoryItem);
//                        }
//                    });
//                    sdf(dlg);
//                }
            }

            @Override
            public void OnItemClick(int position, VideoCategoryItem item) {
                if (player != null && player.isPlaying())
                    player.pause();
                VideoCategoryItem categoryItem = list.get(position);
                sp.setLiveSelectedIndex(position);
                startPlayLiveStream(categoryItem);
            }

            @Override
            public void OnItemLongClick(int position, VideoCategoryItem item) {

            }
        });
        lv.setAdapter(adapter);

        ItemTouchHelper helper = new ItemTouchHelper(createHelperCallback());
        helper.attachToRecyclerView(lv);

        listOpenButton = fv(R.id.button_list_open);
        listOpenButton.setVisibility(View.GONE);
        listOpenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (curItem != null) {
                    loadingText.setVisibility(View.VISIBLE);
                    listLv.setVisibility(View.GONE);
                    setLiveListByChannel(curItem.getVideoCategoryChannelId());
//                    api.getSearchLiveVideos(curItem.getVideoCategoryId(), Common.RESULT_TYPE_LIST);
                    String title = curItem.getVideoCategoryTitle() + r.s(R.string.message_select_live_video);
                    listTitleText.setText(title);
                }
                listOpenButton.setVisibility(View.GONE);
                listExpandLayout.expand(true);
//                }
            }
        });
        listCloseButton = fv(R.id.button_list_close);
        listCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listOpenButton.setVisibility(View.VISIBLE);
                listExpandLayout.collapse(true);
            }
        });

        listExpandLayout = fv(R.id.layout_expand_list);
        listExpandLayout.collapse(false);
        listTitleText = fv(R.id.text_list_title);
        listLv = fv(R.id.lv_list);
        listManager = new GridLayoutManager(getActivity(), 1);
        listLv.setLayoutManager(listManager);
        liveList = new ArrayList<>();
        listAdapter = new LiveVideoAdapter(getActivity(), R.layout.item_live_search_list, liveList, new ReAdapter.ReOnItemClickListener() {
            @Override
            public void OnItemClick(int position, Object item) {
                LiveVideoItem selectedItem = liveList.get(position);
                listAdapter.setSelectedIndex(position);

                if (curItem == null)
                    return;
                curItem.setVideoCategoryLiveTitle(selectedItem.getTitle());
                curItem.setVideoCategoryLiveVideoId(selectedItem.getVideoId());
                curItem.setVideoCategoryLiveDescription(selectedItem.getDescription());
                curItem.setVideoCategoryLiveUrlDefault(selectedItem.getThumbnailUrl());
                curItem.setVideoCategoryLiveUrlMedium(selectedItem.getThumbnailUrl());
                curItem.setVideoCategoryLiveUrlHigh(selectedItem.getThumbnailUrl());
                byte[] thumbnail = null;
                curItem.setVideoCategoryLiveThumbnail(thumbnail);

                db.insertOrUpdateVideoCategoryItem(curItem);
                refreshList(false);
                startPlayLiveStream(curItem);
            }

            @Override
            public void OnItemLongClick(int position, Object item) {

            }
        });
        listLv.setAdapter(listAdapter);

        loadingLayout = fv(R.id.layout_loading);
        loadingLayout.setVisibility(View.GONE);
        loadingImage = fv(R.id.image_loading);


        refreshList(true);
    }

    private void startPlayLiveStream(VideoCategoryItem item) {
        curItem = item;
        listOpenButton.setVisibility(View.VISIBLE);
        if (player != null && !TextUtils.isEmpty(item.getVideoCategoryLiveVideoId())) {
            textLayout.setVisibility(View.VISIBLE);
            mainText.setText(item.getVideoCategoryLiveTitle());
            subText.setText(item.getVideoCategoryLiveDescription());
            if (adapter != null)
                adapter.setSelectedIndex(sp.getLiveSelectedIndex());

            player.loadVideo(item.getVideoCategoryLiveVideoId());
            player.play();
        } else {
            textLayout.setVisibility(View.GONE);
            mainText.setText("");
            subText.setText("");
        }
    }

    public void startLoading() {
        if (loadingLayout == null)
            return ;

        if (Build.VERSION.SDK_INT < 21) {
            startBounceAnimDrawable();
        } else {
            startAnimDrawable();
        }
    }

    public void endLoading() {
        if (loadingLayout == null)
            return ;

        if (loadingLayout.getVisibility() == View.VISIBLE) {
            loadingLayout.setVisibility(View.GONE);
            loadingImage.clearAnimation();
            if (animatedVector != null && animatedVector.isRunning())
                animatedVector.clearAnimationCallbacks();
        }
    }

    public void refreshList(boolean start) {

        if (list == null)
            list = new ArrayList<>();
        else
            list.clear();

//        ArrayList<VideoCategoryItem> dbItems = new ArrayList<>();
//        dbItems.addAll(db.getVideoCategoryItems());
        list.addAll(db.getVideoCategoryItems());
        adapter.notifyDataSetChanged();

        if (start) {
            if (list.size() > sp.getLiveSelectedIndex() &&
                    !TextUtils.isEmpty(list.get(sp.getLiveSelectedIndex()).getVideoCategoryLiveVideoId())) {
                if (player != null && !player.isPlaying())
                    startPlayLiveStream(list.get(sp.getLiveSelectedIndex()));
            }
        }

        if (list != null && list.size() > 0) {
            for (VideoCategoryItem item : list) {
                if (item.getVideoCategoryLiveThumbnail() == null) {
                    new AddCategoryThumbnail().execute(item);
                }
            }
        }


    }

    public void pauseVideo() {
        if (player != null && player.isPlaying())
            player.pause();
    }

    private void setLiveListByChannel(String id) {
        ArrayList<LiveVideoItem> temp = new ArrayList<>();
        temp.addAll(db.getLiveVideoItemsByChannel(id));
        if (temp != null && temp.size() > 0) {
            liveList.clear();
            int index = -1;
            for (int i = 0; i < temp.size(); i++) {
                if (temp.get(i).getVideoId().equalsIgnoreCase(curItem.getVideoCategoryLiveVideoId()))
                    index = i;
                liveList.add(temp.get(i));
            }
            listAdapter.setSelectedIndex(index);
            loadingText.setVisibility(View.GONE);
            listLv.setVisibility(View.VISIBLE);

            for (LiveVideoItem live : liveList) {
                if (live.getThumbnail() == null)
                    new AddLiveThumbnail().execute(live);
            }
        }

    }

    private void moveItem(int oldPos, int newPos) {
        VideoCategoryItem item = list.get(oldPos);
        list.remove(oldPos);
        list.add(newPos, item);
        adapter.notifyItemMoved(oldPos, newPos);

        for (int i = 0; i < list.size(); i++) {
            item = list.get(i);
            item.setVideoCategoryIndex(list.size() - i);
            db.updateVideoCategoryOrder(item);
        }

        if (oldPos == sp.getLiveSelectedIndex())
            sp.setLiveSelectedIndex(newPos);
    }

    private ItemTouchHelper.Callback createHelperCallback() {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.START | ItemTouchHelper.END) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                moveItem(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return false;
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return true;
            }
        };

        return simpleCallback;
    }


    class AddCategoryThumbnail extends AsyncTask<VideoCategoryItem, Void, VideoCategoryItem> {
        @Override
        protected VideoCategoryItem doInBackground(VideoCategoryItem... videoCategoryItems) {
            VideoCategoryItem item = videoCategoryItems[0];

            Bitmap bmp = ImageUtil.getImageFromURLNonTask(item.getVideoCategoryLiveUrlDefault());
            if (bmp != null)
                item.setVideoCategoryLiveThumbnail(ImageUtil.getImageBytes(bmp));

            return item;
        }

        @Override
        protected void onPostExecute(VideoCategoryItem videoCategoryItem) {
            if (videoCategoryItem != null)
                db.insertOrUpdateVideoCategoryItem(videoCategoryItem);

            if (adapter != null)
                adapter.updateThumbnail(videoCategoryItem);
        }
    }

    class AddLiveThumbnail extends AsyncTask<LiveVideoItem, Void, LiveVideoItem> {
        @Override
        protected LiveVideoItem doInBackground(LiveVideoItem... liveVideoItems) {
            LiveVideoItem item = liveVideoItems[0];

            Bitmap bmp = ImageUtil.getImageFromURLNonTask(item.getThumbnailUrl());
            if (bmp != null)
                item.setThumbnail(ImageUtil.getImageBytes(bmp));

            return item;
        }

        @Override
        protected void onPostExecute(LiveVideoItem item) {
            if (item != null)
                db.insertOrUpdateLiveVideoItem(item);

            if (listAdapter != null)
                listAdapter.updateThumbnail(item);
        }
    }

    private void startAnimDrawable() {
        loadingLayout.setVisibility(View.VISIBLE);
        animatedVector = AnimatedVectorDrawableCompat.create(getActivity(), R.drawable.loading);
        animatedVector.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {
                startAnimDrawable();
            }
        });

        loadingImage.setImageDrawable(animatedVector);
        animatedVector.start();
    }

    private void startBounceAnimDrawable() {
        loadingLayout.setVisibility(View.VISIBLE);
        loadingImage.clearAnimation();
        anim = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce_intro);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadingImage.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        loadingImage.startAnimation(anim);
    }
}
