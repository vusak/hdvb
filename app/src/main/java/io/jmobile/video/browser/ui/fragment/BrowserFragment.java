package io.jmobile.video.browser.ui.fragment;


import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.base.BaseFragment;
import io.jmobile.video.browser.base.ExpandLayout;
import io.jmobile.video.browser.common.Common;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.common.Util;
import io.jmobile.video.browser.data.BrowserItem;
import io.jmobile.video.browser.data.FavoriteItem;


public class BrowserFragment extends BaseFragment implements GestureDetector.OnGestureListener {

    public WebView webView;
    LinearLayout wrongLayout;
    //    TextView urlEdit, newTabButton;
    Button urlEdit;
    ImageButton refreshButton, homeButton;
    ProgressBar loadingProgress;
    ExpandLayout urlExpand;

    String currentTitle, currentSite, addHistoryUrl;
    Bitmap currentIcon;

    String readingTitle, readingUrl, readingDescription, readingImage;
    ArrayList<String> readingTags;
    GestureDetector detector;
    float initialY = -1;
    String checkUrl = "";
    boolean CLEAR_HISTORY = false;

    String ERROR_PAGE_URL = "file:///android_asset/error_page.html";

    //    BrowserItem item;
    LinearLayout rootLayout;

    private BrowserFragmentListsner listener;

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        initView();

        detector = new GestureDetector(getActivity(), this);

//        if (webView != null) {
//
//            if ((webView.copyBackForwardList() == null || (webView.copyBackForwardList().getSize() <= 0) && (item != null && item.getBrowserHistory() != null))) {
//                if (webView.getOriginalUrl() != null && webView.getOriginalUrl().equalsIgnoreCase(item.getBrowserUrl()))
//                    return;
//
////            if (item.getBrowserHistory() != null) {
//                byte[] bundleBytes = item.getBrowserHistory();
//                final Parcel parcel = Parcel.obtain();
//                parcel.unmarshall(bundleBytes, 0, bundleBytes.length);
//                parcel.setDataPosition(0);
//                Bundle bundle = (Bundle) parcel.readBundle();
//                parcel.recycle();
//
//                webView.restoreState(bundle);
//
//                if (webView.getUrl() != null)
//                    urlEdit.setText(webView.getUrl());
//            }
//        }

        if (getArguments() != null) {
            String url = getArguments().getString(Common.INTENT_GO_TO_URL);
            if (!TextUtils.isEmpty(url))
                goUrl(url);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_browser;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (webView != null) {
            webView.resumeTimers();
            webView.onResume();
            webView.reload();
        }

        if (urlExpand != null)
            urlExpand.collapse(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (webView != null) {

            webView.onPause();
            webView.pauseTimers();
            webView.stopLoading();
        }


    }

    @Override
    public void onDestroy() {
        if (webView != null) {
            webView.stopLoading();
            webView.onPause();
            webView.destroy();
        }

        super.onDestroy();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (urlExpand.isExpanded() && distanceY > 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.collapse(true);
            initialY = distanceY;
            return true;
        } else if (!urlExpand.isExpanded() && distanceY < 0 && Math.abs(initialY - distanceY) > 100) {
            urlExpand.expand(true);
            initialY = distanceY;
            return true;
        }
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
    }

    public boolean onPrevButtonClicked() {
        if (webView != null && webView.canGoBack()) {

            if (!Util.isNetworkAbailable(getActivity()) && webView.getUrl().equals(ERROR_PAGE_URL))
                return false;

            webView.goBack();
            return true;
        }

        return false;
    }

    public void onNextButtonClicked() {
        if (webView != null)
            webView.goForward();
    }

    public void setURLString(String url) {
        urlEdit.setText(url);
    }

    public void setListener(BrowserFragmentListsner listener) {
        this.listener = listener;
    }

//    public BrowserItem getBrowserItem() {
//        return this.item;
//    }
//
//    public void setBrowserItem(BrowserItem item) {
//        if (item == null)
//            return;
//
//        this.item = item;
//
//    }

    public void goUrl(final String url) {
        if (urlExpand != null && !urlExpand.isExpanded())
            urlExpand.expand(false);

        if (urlEdit != null)
            urlEdit.post(new Runnable() {
                @Override
                public void run() {
                    urlEdit.setText(url);
                }
            });

        if (webView != null)
            webView.post(new Runnable() {
                @Override
                public void run() {
                    webView.onResume();
                    webView.loadUrl(url);
//                    webView.postUrl(url, null);
                }
            });

//        if (newTabButton != null)
//            newTabButton.post(new Runnable() {
//                @Override
//                public void run() {
//                    newTabButton.setText("" + sp.getBrowserTabCount());
//                }
//            });
//            newTabButton.setText("" + count);
    }

//    public void newTabButtonClicked() {
//        Bundle bundle = new Bundle();
//        webView.saveState(bundle);
//        final Parcel parcel = Parcel.obtain();
//        bundle.writeToParcel(parcel, 0);
//        byte[] bundleBytes = parcel.marshall();
//        parcel.recycle();
//
//        if (bundleBytes != null)
//            item.setBrowserHistory(bundleBytes);
//        new UpdateBrowserItem().execute(item);
//    }

    private void initView() {
        if (LogUtil.isDebugMode() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            WebView.setWebContentsDebuggingEnabled(true);

        rootLayout = fv(R.id.layout_root);

        urlExpand = fv(R.id.layout_expand_url);
        urlExpand.expand(false);

        wrongLayout = fv(R.id.layout_wrong);
        wrongLayout.setVisibility(View.GONE);
        webView = fv(R.id.webview);

//        if (!sp.isSavePasswords()) {
//            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(webView.getContext());
//            android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
//            cookieManager.setAcceptCookie(true);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                cookieManager.removeSessionCookies(null);
//                cookieManager.removeAllCookies(null);
//            } else {
//                cookieManager.removeSessionCookie();
//                cookieManager.removeAllCookie();
//            }
//            cookieSyncManager.sync();
//        }

        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSavePassword(true);
        webView.getSettings().setSaveFormData(true);
//        webView.getSettings().setDatabaseEnabled(sp.isSavePasswords());
//        webView.getSettings().setAppCacheEnabled(sp.isSavePasswords());
//        webView.getSettings().setDomStorageEnabled(sp.isSavePasswords());
//        webView.getSettings().setSavePassword(sp.isSavePasswords());
//        webView.getSettings().setSaveFormData(sp.isSavePasswords());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportMultipleWindows(true);

        webView.setWebViewClient(new MyWebViewClient());
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return detector.onTouchEvent(event);
            }
        });
        webView.requestFocus();

//        webView.addJavascriptInterface(new MyJavascriptInterface(), "Android");
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                currentTitle = title;
                currentSite = view.getOriginalUrl();
//                item.setBrowserTitle(title);
            }

            @Override
            public void onReceivedIcon(WebView view, Bitmap icon) {
                super.onReceivedIcon(view, icon);
//                item.setBrowserIcon(ImageUtil.getImageBytes(icon));
                currentIcon = icon;
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                loadingProgress.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }

            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                view.removeAllViews();

                WebView child = new WebView(view.getContext());
                child.getSettings().setJavaScriptEnabled(true);
                child.setWebChromeClient(new WebChromeClient() {
                    @Override
                    public void onCloseWindow(WebView window) {
                        window.setVisibility(View.GONE);
                        webView.removeView(window);
                    }
                });
                child.setWebViewClient(new WebViewClient());
                child.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                view.addView(child);

                WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
                transport.setWebView(child);
                resultMsg.sendToTarget();

                return true;
            }
        });

        urlEdit = fv(R.id.button_url);
        urlEdit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (webView != null)
                    webView.onPause();
                if (listener != null)
                    listener.onUrlEditClick(webView.getUrl());
                return false;
            }
        });

        refreshButton = fv(R.id.button_url_refresh);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.reload();
            }
        });

        homeButton = fv(R.id.button_home);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onHomeButtonClick();
            }
        });

        loadingProgress = fv(R.id.progress_url);
    }

    private void addHistoryList(String url) {
        if (TextUtils.isEmpty(url))
            return;

        addHistoryUrl = url;

        String temp = url.replace("http://", "");
        temp = temp.replace("https://", "");
        FavoriteItem favorite = db.getFavoriteItem(temp);
        if (favorite != null) {
            favorite.setFavoriteCount(favorite.getFavoriteCount() + 1);
        } else {
            favorite = new FavoriteItem();
            favorite.setFavoriteUrl(temp);
            favorite.setFavoriteSite(addHistoryUrl);
            favorite.setFavoriteTitle(currentTitle);
//            favorite.setFavoriteIcon(item.getHistoryIcon());
            favorite.setFavoriteAt(System.currentTimeMillis());
        }
        new AddFavoriteItem().execute(favorite);
    }


    public void setHomeData() {
        if (webView != null) {
//            webView.loadUrl("about:blank");
            webView.clearHistory();
//            item.setBrowserUrl("HOME");
//            item.setBrowserTitle("HOME");
//            item.setBrowserIcon((byte[]) null);
//            item.setBrowserThumbnail((byte[]) null);
//            item.setBrowserHistory((byte[]) null);
//            db.updateBrowser(item);
        }
    }

    public void clearCurrData() {

        readingTitle = "";
        readingUrl = "";
        readingDescription = "";
        readingImage = "";
        currentIcon = null;
        currentTitle = "";
        currentSite = "";
        addHistoryUrl = "";
    }

    public void stopWebViewLoading() {
        if (webView != null) {
            webView.onPause();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static interface BrowserFragmentListsner {
        public void onUrlEditClick(String url);

        public void onHomeButtonClick();

        public void addVideoHistoryUrl(String site, String url);

        public void onNewTabClick();
    }

    private class MyWebViewClient extends WebViewClient {

        String compareText = "";

        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
            resend.sendToTarget();
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            if (url.equals("about:blank")) {
                urlEdit.setText("");
                clearCurrData();
            }
            super.onPageCommitVisible(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
//            if (url.matches("(?i).*youtube.*")) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//                return;
//            }
            if (url.equals(ERROR_PAGE_URL)) {
                onPrevButtonClicked();
                return;
            }
            wrongLayout.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            loadingProgress.setVisibility(View.VISIBLE);
            clearCurrData();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {

            view.loadUrl("javascript:window.Android.getHeader(document.getElementsByTagName('head')[0].innerHTML);");

            compareText = url;
            loadingProgress.setVisibility(View.INVISIBLE);
            LogUtil.log("cecil onpageFinished :: " + url);

//            if (url.equals("about:blank") || Common.SITE.equals(compareText)) {
//                return;
//            }

//            Common.SITE = url;
//            sp.setBrowserUrl(url);
//            item.setBrowserUrl(url);
            urlEdit.setText(url);
            currentSite = url;
            urlExpand.collapse(false);
//            addHistoryList(currentSite);
            super.onPageFinished(view, url);
        }

        @Override
        public void onLoadResource(WebView view, final String url) {

//            if (!Common.checkAvailableDownload(url).equals("novideo")) { // url에 비디오 관련 확장자 없음
//
////                if (!sp.getCheckedDownloadUrl().equals(url)) {
////                    sp.setCheckedDownloadUrl(url);
//                if (!checkUrl.equals(url)) {
//                    checkUrl = url;
//                    Thread t = new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                URLConnection u = new URL(url).openConnection();
//                                String type = u.getHeaderField("Content-Type");
//
//                                if (url.matches("(?i).*youtube.*")) {
//
//                                } else if (type.matches("video.*")) {
//                                    if (listener != null)
//                                        listener.addVideoHistoryUrl(currentSite, url);
//                                }
//
//
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });
//
//                    t.start();
//                }
//            }
            super.onLoadResource(view, url);
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            super.doUpdateVisitedHistory(view, url, isReload);
            addHistoryList(currentSite);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            switch (errorCode) {
                case ERROR_AUTHENTICATION:
                    break;                          // 서버에서 사용자 인증 실패
                case ERROR_BAD_URL:
                    break;                           // 잘못된 URL
                case ERROR_CONNECT:
                    break;                          // 서버로 연결 실패
                case ERROR_FAILED_SSL_HANDSHAKE:
                    break;                          // SSL handshake 수행 실패
                case ERROR_FILE:
                    break;                                  // 일반 파일 오류
                case ERROR_FILE_NOT_FOUND:
                    break;               // 파일을 찾을 수 없습니다
                case ERROR_HOST_LOOKUP:
                    break;           // 서버 또는 프록시 호스트 이름 조회 실패
                case ERROR_IO:
                    break;                              // 서버에서 읽거나 서버로 쓰기 실패
                case ERROR_PROXY_AUTHENTICATION:
                    break;   // 프록시에서 사용자 인증 실패
                case ERROR_REDIRECT_LOOP:
                    break;               // 너무 많은 리디렉션
                case ERROR_TIMEOUT:
                    break;                          // 연결 시간 초과
                case ERROR_TOO_MANY_REQUESTS:
                    break;     // 페이지 로드중 너무 많은 요청 발생
                case ERROR_UNKNOWN:
                    break;                        // 일반 오류
                case ERROR_UNSUPPORTED_AUTH_SCHEME:
                    break; // 지원되지 않는 인증 체계
                case ERROR_UNSUPPORTED_SCHEME:
                    break;          // URI가 지원되지 않는 방식
            }

            webView.loadUrl(ERROR_PAGE_URL);
//            wrongLayout.setVisibility(View.VISIBLE);
//            webView.setVisibility(View.GONE);
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            if (Build.VERSION.SDK_INT >= 23) {
                switch (error.getErrorCode()) {
                    case ERROR_AUTHENTICATION:
                        break;                          // 서버에서 사용자 인증 실패
                    case ERROR_BAD_URL:
                        break;                           // 잘못된 URL
                    case ERROR_CONNECT:
                        break;                          // 서버로 연결 실패
                    case ERROR_FAILED_SSL_HANDSHAKE:
                        break;                          // SSL handshake 수행 실패
                    case ERROR_FILE:
                        break;                                  // 일반 파일 오류
                    case ERROR_FILE_NOT_FOUND:
                        break;               // 파일을 찾을 수 없습니다
                    case ERROR_HOST_LOOKUP:
                        break;           // 서버 또는 프록시 호스트 이름 조회 실패
                    case ERROR_IO:
                        break;                              // 서버에서 읽거나 서버로 쓰기 실패
                    case ERROR_PROXY_AUTHENTICATION:
                        break;   // 프록시에서 사용자 인증 실패
                    case ERROR_REDIRECT_LOOP:
                        break;               // 너무 많은 리디렉션
                    case ERROR_TIMEOUT:
                        break;                          // 연결 시간 초과
                    case ERROR_TOO_MANY_REQUESTS:
                        break;     // 페이지 로드중 너무 많은 요청 발생
                    case ERROR_UNKNOWN:
                        break;                        // 일반 오류
                    case ERROR_UNSUPPORTED_AUTH_SCHEME:
                        break; // 지원되지 않는 인증 체계
                    case ERROR_UNSUPPORTED_SCHEME:
                        break;          // URI가 지원되지 않는 방식
                }
            }
//            wrongLayout.setVisibility(View.VISIBLE);
//            webView.setVisibility(View.GONE);
            webView.loadUrl(ERROR_PAGE_URL);
            super.onReceivedError(view, request, error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
            if (Common.checkAvailableDownload(url).equals("novideo") && url.matches("http.*")) { // url에 비디오 관련 확장자 없음
                if (Uri.parse(url).getScheme().equals("market") || url.contains("https://play.google.com/store/apps/")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return true;
                } else {
//                    view.postUrl(url, null);
//                    view.loadUrl(url);
                    return false;
                }
            } else {
//                if (!sp.getCheckedDownloadUrl().equals(url)) {
//                    sp.setCheckedDownloadUrl(url);
                if (!checkUrl.equals(url)) {
                    checkUrl = url;
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                URLConnection u = new URL(url).openConnection();
                                String type = u.getHeaderField("Content-Type");
                                if (url.matches("(?i).*youtube.*")) {

                                } else if (type.matches("video.*")) {
//                                    downloadHandler.sendEmptyMessage(0);
                                    if (listener != null)
                                        listener.addVideoHistoryUrl(currentSite, url);
                                } else {
//                                    view.loadUrl(url);
//                                    return false;
                                }


                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        }
                    });

                    t.start();
                }
            }

            return true;
        }


    }

    class UpdateBrowserItem extends AsyncTask<BrowserItem, Void, BrowserItem> {
        @Override
        protected BrowserItem doInBackground(BrowserItem... params) {
            BrowserItem item = params[0];
            if (webView != null) {
                item.setBrowserThumbnail(ImageUtil.getScreenShotByteArray(getActivity(), webView));
            }

            if (currentIcon != null) {
                item.setBrowserIcon(ImageUtil.getImageBytes(currentIcon));
            }

            return item;
        }

        @Override
        protected void onPostExecute(BrowserItem browserItem) {
            db.insertOrUpdateBrowserItem(browserItem);
            if (listener != null)
                listener.onNewTabClick();
        }
    }

    class AddFavoriteItem extends AsyncTask<FavoriteItem, Void, FavoriteItem> {
        @Override
        protected FavoriteItem doInBackground(FavoriteItem... params) {
            FavoriteItem item = params[0];
            String path = item.getFavoriteSite();
            if (TextUtils.isEmpty(path))
                return null;

            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
                iconUrl = "http://" + iconUrl;
            Bitmap bitmap = null;
            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);
            if (bitmap != null)
                item.setFavoriteIcon(ImageUtil.getImageBytes(bitmap));
            else if (currentIcon != null)
                item.setFavoriteIcon(ImageUtil.getImageBytes(currentIcon));

            return item;
        }

        @Override
        protected void onPostExecute(FavoriteItem favoriteItem) {
            db.insertOrUpdateFavoriteItem(favoriteItem);
        }
    }
//
//
//    class AddHistoryItem extends AsyncTask<HistoryItem, Void, HistoryItem> {
//        @Override
//        protected HistoryItem doInBackground(HistoryItem... params) {
//            HistoryItem item = params[0];
//            String path = item.getHistoryUrl();
//            if (TextUtils.isEmpty(path))
//                return null;
//
//            String iconUrl = Uri.parse(path).getHost() + "/favicon.ico";
//            if (!iconUrl.contains("http://") && !iconUrl.contains("https://"))
//                iconUrl = "http://" + iconUrl;
//            Bitmap bitmap = null;
//            bitmap = ImageUtil.getImageFromURLNonTask(iconUrl);
//
//            if (bitmap != null)
//                item.setHistoryIcon(ImageUtil.getImageBytes(bitmap));
//            else if (currentIcon != null)
//                item.setHistoryIcon(ImageUtil.getImageBytes(currentIcon));
//
//            return item;
//        }
//
//        @Override
//        protected void onPostExecute(HistoryItem item) {
//            db.insertOrUpdateHistoryItem(item);
//
//            String temp = item.getHistoryUrl().replace("http://", "");
//            temp = temp.replace("https://", "");
//            FavoriteItem favorite = db.getFavoriteItem(temp);
//            if (favorite != null) {
//                favorite.setFavoriteCount(favorite.getFavoriteCount() + 1);
//            } else
//                favorite = new FavoriteItem();
//            favorite.setFavoriteUrl(temp);
//            favorite.setFavoriteSite(item.getHistoryUrl());
//            favorite.setFavoriteTitle(item.getHistoryTitle());
//            favorite.setFavoriteIcon(item.getHistoryIcon());
//            favorite.setFavoriteAt(item.getHistoryAt());
//            db.insertOrUpdateFavoriteItem(favorite);
//        }
//    }

}
