package io.jmobile.video.browser.adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.jmobile.video.browser.R;
import io.jmobile.video.browser.common.ImageUtil;
import io.jmobile.video.browser.common.LogUtil;
import io.jmobile.video.browser.data.VideoCategoryItem;

public class VideoCategoryAdapter extends ReSelectableAdapter<VideoCategoryItem, VideoCategoryAdapter.VideoCategoryHolder> {

    private Context context;
    private ArrayList<VideoCategoryItem> items = new ArrayList<>();
    private int index = -1;

    public VideoCategoryAdapter(Context context, int layoutId, ArrayList<VideoCategoryItem> items, VideoCategorydAdapterListener listener) {
        super(layoutId, items, context);
        this.context = context;

        this.items = items;
        this.listener = listener;
        this.setSelectMode(ReSelectableAdapter.CHOICE_SINGLE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(VideoCategoryHolder holder, int position) {

        VideoCategoryItem item = items.get(position);

        holder.rootLayout.setBackgroundResource(index == position ? R.color.gray3_100 : R.color.white);
        holder.playLayout.setVisibility(index == position ? View.VISIBLE : View.GONE);
        holder.title.setText(item.getVideoCategoryTitle());
        if (!TextUtils.isEmpty(item.getVideoCategoryLiveTitle()))
            holder.description.setText(item.getVideoCategoryLiveTitle());
        try {
            if (item.getVideoCategoryLiveThumbnail() != null)
                holder.thumbnail.setImageBitmap(ImageUtil.getImage(item.getVideoCategoryLiveThumbnail()));
            else
                holder.thumbnail.setImageResource(R.drawable.img_loading_video_120);
//            else if (!TextUtils.isEmpty(item.getVideoCategoryLiveUrlDefault()))
//                holder.thumbnail.setImageBitmap(ImageUtil.getImageFromURL(item.getVideoCategoryLiveUrlDefault()));
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
        }
    }

    @Override
    public VideoCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        VideoCategoryHolder holder = new VideoCategoryHolder(LayoutInflater.from(context).inflate(layoutId, parent, false));

        holder.title = holder.fv(R.id.text_title);
        holder.description = holder.fv(R.id.text_sub);
        holder.menu = holder.fv(R.id.button_menu);
        holder.thumbnail = holder.fv(R.id.image_thumbnail);
        holder.rootLayout = holder.fv(R.id.layout_root);
        holder.playLayout = holder.fv(R.id.layout_play);

        holder.setOnCellClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0) {
                    listener.OnItemClick(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
//                if (listener != null && items.size() > position && position >= 0)
//                    listener.OnItemLongClick(position, items.get(position));
//                return true;
                return false;
            }
        });

        holder.setOnMenuButtonClickListener(new ReAbstractViewHolder.OnItemViewClickListener() {
            @Override
            public void onItemViewClick(int position, ReAbstractViewHolder holder) {
                if (listener != null && items.size() > position && position >= 0) {
                    ((VideoCategorydAdapterListener) listener).onMenuButtonClicked(position, items.get(position));
                }
            }

            @Override
            public boolean onItemViewLongClick(int position, ReAbstractViewHolder holder) {
                return false;
            }
        });
        return holder;
    }

    public void setSelectedIndex(int index) {
        this.index = index;
        notifyDataSetChanged();
    }

    public static interface VideoCategorydAdapterListener extends ReOnItemClickListener<VideoCategoryItem> {
        public void onMenuButtonClicked(int position, VideoCategoryItem item);
    }

    public class VideoCategoryHolder extends ReAbstractViewHolder {
        TextView title, description;
        ImageButton menu;
        ImageView thumbnail;
        LinearLayout rootLayout, playLayout;
        private OnItemViewClickListener menuButtonClickListener;


        public VideoCategoryHolder(View itemView) {
            super(itemView);
        }

        public void setOnMenuButtonClickListener(OnItemViewClickListener listener) {
            menuButtonClickListener = listener;
            if (listener != null) {
                menu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        menuButtonClickListener.onItemViewClick(getPosition(), VideoCategoryHolder.this);
                    }
                });
            }
        }
    }

    public void updateThumbnail(VideoCategoryItem _item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getVideoCategoryId().equalsIgnoreCase(_item.getVideoCategoryId())) {
                notifyItemChanged(i);
                break;
            }
        }
    }
}
